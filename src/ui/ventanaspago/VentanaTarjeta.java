package ui.ventanaspago;

import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import BBDD.ActualizarRegistros;
import BBDD.CambiarEstadoAtleta;
import BBDD.EstadosInscripcion;
import DTO.PlazoDTO;
import ui.VentanasInscripcion.VentanaRegistro;
import ui.ventanasIntroductivas.VentanaInicial;

public class VentanaTarjeta extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private VentanaRegistro ventanaRegistro;
	private JLabel lblNTarjeta;
	private JTextField textField;
	private JLabel lblCaducidad;
	private JComboBox<String> comboBox;
	private JComboBox<String> comboBox_1;
	private JLabel lblMes;
	private JLabel lblAo;
	private JLabel lblCvv;
	private JTextField textField_1;
	private JButton btnAceptar;

	/**
	 * Create the frame.
	 * 
	 * @param ventanaRegistro
	 */
	public VentanaTarjeta(VentanaRegistro ventanaRegistro) {
		this.ventanaRegistro = ventanaRegistro;
		setResizable(false);
		setTitle("EII Atletismo:  Informacion transferencia");
		setIconImage(
				Toolkit.getDefaultToolkit().getImage(VentanaTransferencia.class.getResource("/Imagenes/Atlet.jpg")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 581, 416);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getLblNTarjeta());
		contentPane.add(getTextField());
		contentPane.add(getLblCaducidad());
		contentPane.add(getComboBox());
		contentPane.add(getComboBox_1());
		contentPane.add(getLblMes());
		contentPane.add(getLblAo());
		contentPane.add(getLblCvv());
		contentPane.add(getTextField_1());
		contentPane.add(getBtnAceptar());
	}

	private JLabel getLblNTarjeta() {
		if (lblNTarjeta == null) {
			lblNTarjeta = new JLabel("Nº Tarjeta:");
			lblNTarjeta.setFont(new Font("Tahoma", Font.PLAIN, 15));
			lblNTarjeta.setBounds(142, 83, 111, 47);
		}
		return lblNTarjeta;
	}

	private JTextField getTextField() {
		if (textField == null) {
			textField = new JTextField();
			textField.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent e) {
					if (textField.getText().length() >= 20)
						e.consume();
				}
			});
			textField.setBounds(262, 98, 222, 20);
			textField.setColumns(10);
		}
		return textField;
	}

	private JLabel getLblCaducidad() {
		if (lblCaducidad == null) {
			lblCaducidad = new JLabel("Caducidad:");
			lblCaducidad.setFont(new Font("Tahoma", Font.PLAIN, 15));
			lblCaducidad.setBounds(142, 154, 72, 31);
		}
		return lblCaducidad;
	}

	private JComboBox<String> getComboBox() {
		if (comboBox == null) {
			comboBox = new JComboBox();
			comboBox.setModel(new DefaultComboBoxModel(
					new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" }));
			comboBox.setBounds(263, 181, 53, 20);
		}
		return comboBox;
	}

	private JComboBox<String> getComboBox_1() {
		if (comboBox_1 == null) {
			comboBox_1 = new JComboBox<String>();
			comboBox_1.setModel(new DefaultComboBoxModel(
					new String[] { "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026" }));
			comboBox_1.setBounds(335, 181, 62, 20);
		}
		return comboBox_1;
	}

	private JLabel getLblMes() {
		if (lblMes == null) {
			lblMes = new JLabel("Mes:");
			lblMes.setBounds(263, 164, 46, 14);
		}
		return lblMes;
	}

	private JLabel getLblAo() {
		if (lblAo == null) {
			lblAo = new JLabel("Anno:");
			lblAo.setBounds(335, 164, 46, 14);
		}
		return lblAo;
	}

	private JLabel getLblCvv() {
		if (lblCvv == null) {
			lblCvv = new JLabel("CVV:");
			lblCvv.setFont(new Font("Tahoma", Font.PLAIN, 15));
			lblCvv.setBounds(142, 234, 72, 31);
		}
		return lblCvv;
	}

	private JTextField getTextField_1() {
		if (textField_1 == null) {
			textField_1 = new JTextField();
			textField_1.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent e) {
					if (textField_1.getText().length() >= 3)
						e.consume();
				}
			});
			textField_1.setBounds(262, 241, 86, 20);
			textField_1.setColumns(10);
		}
		return textField_1;
	}

	private JButton getBtnAceptar() {
		if (btnAceptar == null) {
			btnAceptar = new JButton("Aceptar");
			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			int anno = c.get(Calendar.YEAR);
			int mes = c.get(Calendar.MONTH);

			btnAceptar.addActionListener(new ActionListener() {
				double cuota = 0;

				public void actionPerformed(ActionEvent e) {
					int anno_seleccionado = Integer.parseInt((String) (getComboBox_1().getSelectedItem()));
					int mes_seleccionado = Integer.parseInt((String) getComboBox().getSelectedItem());
					if (anno == anno_seleccionado) {
						if (mes > mes_seleccionado)

							JOptionPane.showMessageDialog(rootPane, "El mes introducido es incorrecto");

					} else if (getTextField_1().getText().length() == 0 || getTextField().getText().length() == 0) {
						JOptionPane.showMessageDialog(rootPane, "Faltan datos por introducir");
					} else {
						List<PlazoDTO> plazos = ActualizarRegistros.getPlazo(ventanaRegistro.getCarrera().idCarrera);
						for (PlazoDTO plazo : plazos) {
							Date fin = new Date(plazo.fecha_fin_plazo.getTime());
							Date inicio = new Date(plazo.fecha_inicio_plazo.getTime());
							if ((c.before(fin) || c.equals(fin)) && (c.after(inicio) ||  c.equals(inicio))) {
								cuota = plazo.cuota_inscripcion;
							}
						}
						ActualizarRegistros.update(cuota, ventanaRegistro.getAtleta().DNI,
								ventanaRegistro.getCarrera().idCarrera);
						CambiarEstadoAtleta.execute(EstadosInscripcion.INSCRITO, ventanaRegistro.getAtleta().DNI,
								ventanaRegistro.getCarrera().idCarrera);

						JOptionPane.showMessageDialog(rootPane, "La operacion resulto exitosa");
						VentanaInicial v = new VentanaInicial();
						v.setVisible(true);
						
						dispose();
					}
				}
			});
			btnAceptar.setBounds(259, 341, 89, 23);
		}
		return btnAceptar;
	}
}
