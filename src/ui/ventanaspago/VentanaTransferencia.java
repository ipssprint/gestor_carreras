package ui.ventanaspago;

import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import logica.ActualizarCuota;
import ui.VentanasInscripcion.VentanaRegistro;
import ui.ventanasIntroductivas.VentanaInicial;

public class VentanaTransferencia extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JPanel contentPane;
	private JButton btnCerrar;
	private JLabel label;
	private JLabel label_1;
	private JLabel lblIban;
	private JLabel lblCuotaDeInscripcion;

	/**
	 * Create the frame.
	 * 
	 * @param ventanaRegistro
	 */
	public VentanaTransferencia(VentanaRegistro ventanaRegistro) {
		setResizable(false);
		setTitle("EII Atletismo:  Informacion transferencia");
		setIconImage(
				Toolkit.getDefaultToolkit().getImage(VentanaTransferencia.class.getResource("/Imagenes/Atlet.jpg")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 583, 441);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getBtnCerrar());
		contentPane.add(getLblIban());
		contentPane.add(getLblCuotaDeInscripcion());
		getLblIban().setText("IBAN: " + ventanaRegistro.getCarrera().iban);
		ventanaRegistro.getCarrera().cuota_inscripcion=ActualizarCuota.execute(ventanaRegistro.getCarrera());
		getLblCuotaDeInscripcion().setText("Cuota de Inscripcion: " + ventanaRegistro.getCarrera().cuota_inscripcion + " €");
		setLocationRelativeTo(ventanaRegistro);

	}

	

	private JButton getBtnCerrar() {
		if (btnCerrar == null) {
			btnCerrar = new JButton("Cerrar");
			btnCerrar.setMnemonic('c');
			btnCerrar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					VentanaInicial v = new VentanaInicial();
					v.setVisible(true);
					dispose();
				}
			});
			btnCerrar.setBounds(10, 372, 117, 29);
		}
		return btnCerrar;
	}

	private JLabel getLblIban() {
		if (lblIban == null) {
			lblIban = new JLabel("IBAN:");
			lblIban.setFont(new Font("Yu Gothic", Font.PLAIN, 20));
			lblIban.setBounds(49, 44, 518, 162);
		}
		return lblIban;
	}

	private JLabel getLblCuotaDeInscripcion() {
		if (lblCuotaDeInscripcion == null) {
			lblCuotaDeInscripcion = new JLabel("Cuota de inscripcion:");
			lblCuotaDeInscripcion.setFont(new Font("Yu Gothic", Font.PLAIN, 20));
			lblCuotaDeInscripcion.setBounds(49, 217, 406, 120);
		}
		return lblCuotaDeInscripcion;
	}
}
