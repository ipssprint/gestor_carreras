package ui.ventanaspago;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import logica.Operaciones;
import ui.VentanasInscripcion.VentanaRegistro;

public class VentanaMetodosPago extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JLabel lblMetodosDePago;
	private JButton btnConTarjeta;
	private JButton btnPorTransferencia;
	private VentanaRegistro ventanaRegistro;
	private JButton btnVolver;

	/**
	 * Create the frame.
	 */
	public VentanaMetodosPago(VentanaRegistro ventanaRegistro) {
		setTitle("EII Atletismo: Metodos de pago");
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaMetodosPago.class.getResource("/Imagenes/Atlet.jpg")));
		setResizable(false);
		this.ventanaRegistro = ventanaRegistro;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 552, 431);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getLblMetodosDePago());
		contentPane.add(getBtnConTarjeta());
		contentPane.add(getBtnPorTransferencia());
		contentPane.add(getBtnVolver());
		setLocationRelativeTo(ventanaRegistro);
	}

	private JLabel getLblMetodosDePago() {
		if (lblMetodosDePago == null) {
			lblMetodosDePago = new JLabel("METODOS DE PAGO");
			lblMetodosDePago.setBounds(203, 11, 197, 14);
			lblMetodosDePago.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblMetodosDePago;
	}

	private JButton getBtnConTarjeta() {
		if (btnConTarjeta == null) {
			btnConTarjeta = new JButton("Con tarjeta");
			btnConTarjeta.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					Operaciones.preInscribirAtleta(ventanaRegistro.getAtleta(), ventanaRegistro.getCarrera());
					VentanaTarjeta frame = new VentanaTarjeta(ventanaRegistro);
					frame.setVisible(true);
					dispose();
				}
			});
			btnConTarjeta.setBounds(67, 130, 161, 79);
		}
		return btnConTarjeta;
	}

	private JButton getBtnPorTransferencia() {
		if (btnPorTransferencia == null) {
			btnPorTransferencia = new JButton("Por transferencia");
			btnPorTransferencia.setMnemonic('t');
			btnPorTransferencia.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// CREAR VENTANA QUE MUESTRE LOS DATOS DE LA CUENTA Y LA CANTIDAD A ABONA
					Operaciones.preInscribirAtleta(ventanaRegistro.getAtleta(), ventanaRegistro.getCarrera());
					VentanaTransferencia frame = new VentanaTransferencia(ventanaRegistro);
					frame.setVisible(true);
					dispose();

				}
			});
			btnPorTransferencia.setBounds(329, 130, 183, 79);
		}
		return btnPorTransferencia;
	}

	private JButton getBtnVolver() {
		if (btnVolver == null) {
			btnVolver = new JButton("Volver");
			btnVolver.setMnemonic('v');
			btnVolver.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dispose();
					ventanaRegistro.setVisible(true);

				}
			});
			btnVolver.setBounds(6, 374, 117, 29);
		}
		return btnVolver;
	}
}
