package ui.ventanasIntroductivas;

import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import logica.ActualizarRegistroPago;
import ui.ventanaTablas.VentanaCarreras;

public abstract class Introductiva extends JFrame {

	/**
	 * Pantallas iniciales del proyecto
	 * 
	 * @author Javier Ardura
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Crea el prototipo, con su titulo, icono...
	 */
	public Introductiva() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Introductiva.class.getResource("/Imagenes/Atlet.jpg")));
		setTitle("EII Atletismo");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 900, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		setResizable(false);
		setLocationRelativeTo(null);
	}

	/**
	 * Metodo CreaBoton encargado de crear cualquier boton de la pantalla
	 * 
	 * @param texto  nombre de la funcion a realizar por el boton
	 * @param jf     pantalla (accion) a la que debe dar paso
	 * @param altura (96,196,296) Marca la posicion en el eje x del boton
	 * @param m      mnemonico del boton permitiendo reutilizar este metodo para
	 *               crear los 3 botones.
	 */
	public void CreaBotonSinOcultar(String texto, JFrame jf, int altura, char m) {
		JButton btnC = new JButton(texto);
		btnC.setBackground(Color.cyan);
		btnC.setBorder(null);
		btnC.setBounds(700, altura, 150, 50);
		btnC.setMnemonic(m);
		btnC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lanzaUnaNuevaVentanaSinOcutar(jf);
				
			}
		});
		contentPane.add(btnC);
	}

	
	public void CreaBotonRegistroPago(String texto, int altura, char m) {
		JButton btnC = new JButton(texto);
		btnC.setBackground(Color.cyan);
		btnC.setBorder(null);
		btnC.setBounds(700, altura, 150, 50);
		btnC.setMnemonic(m);
		btnC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String mensajes = ActualizarRegistroPago.execute();
				JOptionPane.showMessageDialog(rootPane, "REGISTROS ACTUALIZADOS \n" + mensajes);
			}
		});
		contentPane.add(btnC);
	}

	public void CreaBotonOcultando(String texto, JFrame jf, int altura, char m) {
		JButton btnC = new JButton(texto);
		btnC.setBackground(Color.cyan);
		btnC.setBorder(null);
		btnC.setBounds(700, altura, 150, 50);
		btnC.setMnemonic(m);
		btnC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lanzaUnaNuevaVentanaOcultando(jf);
			}
		});
		contentPane.add(btnC);
	}

	/**
	 * Pone una foto de fondo en la interfaz para mejorar la apariencia de esta
	 * 
	 * @param foto direccion de la foto a utilizar
	 */
	public void ponFotoDeFondo(String foto) {
		JLabel labelFoto = new JLabel();
		labelFoto.setIcon(creaIcono(foto));
		labelFoto.setBounds(0, 0, 900, 600);

		contentPane.add(labelFoto);
	}

	/**
	 * Crear un icono a partir de la direccion de la imagen en el proyecto, en el
	 * tama�o original
	 * 
	 * @param foto direccion de la imagen
	 * @return el icono ya creado
	 */
	private Icon creaIcono(String foto) {
		String path = foto;
		URL url = this.getClass().getResource(path);
		ImageIcon icon = new ImageIcon(url);
		return icon;
	}

	/**
	 * Crea un icono y lo coloca en un boton, adaptandola a su tama�o a partir de la
	 * ruta de la imagen
	 * 
	 * @param boton      donde poner la imagen
	 * @param rutaImagen direccion de esta
	 */
	private void setImagenAdaptada(JButton boton, String rutaImagen) {
		Image imgOriginal = new ImageIcon(getClass().getResource(rutaImagen)).getImage();
		Image imgEscalada = imgOriginal.getScaledInstance((int) (boton.getWidth()), (int) (boton.getHeight()),
				Image.SCALE_FAST);
		boton.setIcon(new ImageIcon(imgEscalada));
	}

	/**
	 * Crea una nueva ventana que se pase como parametro ocultando la anterior
	 * 
	 * @param jf ventana a crear
	 */
	private void lanzaUnaNuevaVentanaOcultando(JFrame jf) {
		jf.setVisible(true);
		this.dispose();
	}

	/**
	 * Crea una nueva ventana que se pase como parametro ocultando la anterior
	 * 
	 * @param jf ventana a crear
	 */
	private void lanzaUnaNuevaVentanaSinOcutar(JFrame jf) {
		jf.setVisible(true);
	}

	/**
	 * Crea un boton atras con su icono ya predefinido y con caracter�sticas de
	 * transparencia y que te redigira a otra pantalla
	 * 
	 * @param jf pantalla a la que te redirigira el boton
	 */
	public void CreaBotonAtras(JFrame jf) {

		JButton btnAtras = new JButton();
		btnAtras.setOpaque(true);
		btnAtras.setBorderPainted(false);
		btnAtras.setContentAreaFilled(false);
		btnAtras.setFocusPainted(false);
		btnAtras.setBorder(null);
		btnAtras.setMnemonic('A');
		btnAtras.setBounds(750, 450, 50, 50);
		setImagenAdaptada(btnAtras, "/Imagenes/atras.png");
		btnAtras.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lanzaUnaNuevaVentanaOcultando(jf);
			}
		});
		contentPane.add(btnAtras);

	}
}
