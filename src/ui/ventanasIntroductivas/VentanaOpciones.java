package ui.ventanasIntroductivas;

import javax.swing.JFrame;

import ui.ventanaResultados.VentanaResultadosAtleta;
import ui.ventanaResultados.VentanaResultadosCarrera;
import ui.ventanaTablas.VentanaCarreras;

public class VentanaOpciones extends Introductiva{
	
	/**
	 * VentanaMenu que ofrecera las distintas opciones al usuario
	 * 
	 * @author Javier Ardura
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Crea la ventana de opciones de la aplicacion, con las que podra interactuar
	 * cada agente
	 * 
	 * @param jf ventana anterior
	 */
	public VentanaOpciones(JFrame jf, int tipoUsuario) {
		super();
		// Boton opcion carreras
		if (tipoUsuario == VentanaInicial.usuario ) {
			VentanaCarreras vc = new VentanaCarreras("Inscribirse");
			vc.ocultarPanel();
			
			super.CreaBotonSinOcultar("Carreras", vc, 96, 'C');
		} 
		else if(tipoUsuario == VentanaInicial.club){
			VentanaCarreras vc = new VentanaCarreras("Inscribirse");
			vc.ocultarPanel();
			vc.activaClub();
			super.CreaBotonSinOcultar("Carreras", vc, 50, 'C');
			super.CreaBotonSinOcultar("Inscribir por lotes (Archivo)", new VentanaCarreras("Inscribirse-Lotes"), 120, 'C');
		}
			else {
		
			super.CreaBotonSinOcultar("Carreras", new VentanaCarreras("Ver inscritos"), 96, 'C');
			super.CreaBotonRegistroPago("Actualizar Registros", 396, 'A');

		}
		// Boton opcion Resultados de un atleta
		super.CreaBotonSinOcultar("Resultados Atleta", new VentanaResultadosAtleta(this), 196, 'R');
		// Boton opcion crear carrera
		super.CreaBotonSinOcultar("Resultados Carrera", new VentanaResultadosCarrera(), 296, 'E');
		// Boton atras que te lleva a la ventana anterior
		super.CreaBotonAtras(jf);
		// Imagen v2 que visualizaremos de fondo
		super.ponFotoDeFondo("/Imagenes/v2.jpeg");
	}

}
