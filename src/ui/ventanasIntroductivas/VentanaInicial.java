package ui.ventanasIntroductivas;

public class VentanaInicial extends Introductiva {

	/**
	 * Ventana Inicio de la aplicación
	 * @author Javier Ardura
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Crea la primera ventana de la aplicacion, menu de usuarios,
	 *  con las diferentes opciones que ofrece
	 */
	public final static int usuario = 0;
	public final static int club = 1;
	public final static int administrador = 2;
	public VentanaInicial()
	{
		super();
		//Boton opcion usuario
		super.CreaBotonOcultando("Usuario", new VentanaOpciones(this, usuario),96,'U');
		//Boton opcion club
		super.CreaBotonOcultando("Club", new VentanaOpciones(this, club),196,'C');
		//Boton opcion administrador
		super.CreaBotonOcultando("Administrador", new VentanaOpciones(this, administrador),296,'A');
		//Coloca la imagen v1 de fondo
		super.ponFotoDeFondo("/Imagenes/v1.jpg");
	}
}
