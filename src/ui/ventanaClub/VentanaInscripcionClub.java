
package ui.ventanaClub;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import BBDD.InsertarClub;
import DTO.CarreraDTO;
import ui.ventanasIntroductivas.VentanaInicial;

public class VentanaInscripcionClub extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnSeleccionar;
	private JTextField textField;
	private JLabel lblSeleccionaElFichero;
	private JButton btnSiguiente;
	private JButton btnCancelar;
	private CarreraDTO carrera;
	private String ruta;

	/**
	 * Create the frame.
	 */
	public VentanaInscripcionClub(CarreraDTO carrera) {
		this.carrera = carrera;
		setTitle("Eii Atletismo - Insertar Carrera");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 578, 373);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getBtnSeleccionar());
		contentPane.add(getTextField());
		contentPane.add(getLblSeleccionaElFichero());
		contentPane.add(getBtnSiguiente());
		contentPane.add(getBtnCancelar());
	}
	private JButton getBtnSeleccionar() {
		if (btnSeleccionar == null) {
			btnSeleccionar = new JButton("Seleccionar");
			btnSeleccionar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					JFileChooser c = new JFileChooser();
					FileNameExtensionFilter filter = new FileNameExtensionFilter("Archivos de texto (.txt)", "txt", "text");
					c.setFileFilter(filter);
					c.showOpenDialog(null);
					String dir = c.getSelectedFile().getAbsolutePath();
					ruta = dir;
					textField.setText(dir);
				}
			});
			btnSeleccionar.setBounds(386, 153, 117, 29);
		}
		return btnSeleccionar;
	}
	private JTextField getTextField() {
		if (textField == null) {
			textField = new JTextField();
			textField.setEditable(false);
			textField.setBounds(75, 153, 299, 26);
			textField.setColumns(10);
		}
		return textField;
	}
	private JLabel getLblSeleccionaElFichero() {
		if (lblSeleccionaElFichero == null) {
			lblSeleccionaElFichero = new JLabel("Selecciona el fichero con los atletas para insertarlos en la carrera");
			lblSeleccionaElFichero.setBounds(86, 87, 460, 16);
		}
		return lblSeleccionaElFichero;
	}
	private JButton getBtnSiguiente() {
		if (btnSiguiente == null) {
			btnSiguiente = new JButton("Siguiente");
			btnSiguiente.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					InsertarClub i = new InsertarClub();
					if(i.insertarAtletas(ruta, carrera)) {
						JOptionPane.showMessageDialog(null, "Se han añadido todos los atletas", "Exito", JOptionPane.INFORMATION_MESSAGE);
						mostrarVentana();
					}
					else {
						ShowAtletasProblemas(i);

					}
					
				}

				private void ShowAtletasProblemas(InsertarClub i) {
					VentanaAtletasProblematicos v = new VentanaAtletasProblematicos(i);
					v.setVisible(true);
					dispose();
					
				}
			});
			btnSiguiente.setBounds(443, 305, 117, 29);
		}
		return btnSiguiente;
	}
	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dispose();
				}
			});
			btnCancelar.setBounds(321, 305, 117, 29);
		}
		return btnCancelar;
	}
	private void mostrarVentana() {
		VentanaInicial v = new VentanaInicial();
		v.setVisible(true);
		dispose();
		
	}
}

