package ui.ventanaClub;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import BBDD.InsertarClub;
import DTO.AtletaDTO;
import ui.ventanasIntroductivas.VentanaInicial;

public class VentanaAtletasProblematicos extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JLabel lblNoSeHan;
	private JScrollPane scrollPane;
	private JLabel lblAtletasNoInsertados;
	private InsertarClub i;
	private JList<String> list;
	private JButton btnFinalizar;

	/**
	 * Create the frame.
	 * @param i 
	 */
	public VentanaAtletasProblematicos(InsertarClub i) {
		this.i = i;
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 626, 382);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getLblNoSeHan());
		contentPane.add(getScrollPane());
		contentPane.add(getLblAtletasNoInsertados());
		contentPane.add(getBtnFinalizar());
		llenarLista();
	}
	private JLabel getLblNoSeHan() {
		if (lblNoSeHan == null) {
			lblNoSeHan = new JLabel("No se han podido insertar todos los atletas");
			lblNoSeHan.setBounds(155, 21, 272, 16);
		}
		return lblNoSeHan;
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBounds(41, 91, 535, 237);
			scrollPane.setViewportView(getList());
		}
		return scrollPane;
	}
	private JLabel getLblAtletasNoInsertados() {
		if (lblAtletasNoInsertados == null) {
			lblAtletasNoInsertados = new JLabel("Atletas no insertados:");
			lblAtletasNoInsertados.setBounds(40, 63, 170, 16);
		}
		return lblAtletasNoInsertados;
	}
	private void llenarLista() {
		List<AtletaDTO> atletas = i.atletasConProblemas;
		DefaultListModel<String> model = new DefaultListModel<String>();
		for(AtletaDTO atleta: atletas) {
			model.addElement(atleta.DNI + " - " + atleta.nombre + " - " + atleta.apellidos + " - " + atleta.sexo + " - " + atleta.fecha_nacimiento);
		}
		list.setModel(model);

	}
	private JList<String> getList() {
		if (list == null) {
			list = new JList<String>();
		}
		return list;
	}
	private JButton getBtnFinalizar() {
		if (btnFinalizar == null) {
			btnFinalizar = new JButton("Finalizar");
			btnFinalizar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					mostrarVentana();
					
				}


			});
			btnFinalizar.setBounds(472, 325, 117, 29);
		}
		return btnFinalizar;
	}
	private void mostrarVentana() {
		dispose();
		
	}
}