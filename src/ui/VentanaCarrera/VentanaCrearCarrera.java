package ui.VentanaCarrera;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import BBDD.InsertarCarrera;
import BBDD.Listados.ListaCategorias;
import DTO.CarreraDTO;
import DTO.CategoriaDTO;
import ui.plazos.VentanaPlazosYCancelacion;
import ui.ventanaCategorias.VentanaAddCategoria;
import ui.ventanaCategorias.VentanaEditar;
import ui.ventanaTablas.VentanaCarreras;
import ui.ventanasIntroductivas.Introductiva;
import com.toedter.calendar.JDateChooser;

public class VentanaCrearCarrera extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JLabel lblNombre;
	private JTextField textFieldNombre;
	private JLabel lblFechaDeCelebracin;
	private JLabel lblLimiteDePlazas;
	private JFormattedTextField textFieldLimite;
	private JLabel lblParticipantes;
	private JLabel lblTipoDeCarrera;
	private JComboBox<String> comboBoxTipo;
	private JLabel lblDesnivel;
	private JFormattedTextField textFieldDesnivel;
	private JLabel lblMetros;
	private JLabel lblKilometraje;
	private JFormattedTextField textFieldDistancia;
	private JLabel lblKilometros;
	private JButton btnDefinirIntervalos;
	private JButton btnCancelar;
	private JScrollPane scrollPane;
	private JTable table;
	private List<CategoriaDTO> categorias;
	private VentanaCarreras ventana;
	private JButton btnAnadirCategoria;
	private JButton btnNewButton;
	private JButton btnEliminarCategoria;
	private JButton btnActualizar;
	private JLabel lblIban;
	private JTextField textFieldIBAN;
	private JRadioButton rdbtnPreinscripcion;
	private JRadioButton rdbtnInscripcion;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private CarreraDTO carrera;
	private JDateChooser dateChooser;

	/**
	 * Create the frame.
	 */
	public VentanaCrearCarrera(VentanaCarreras ventana) {
		this.ventana = ventana;
		categorias = ListaCategorias.execute();
		setIconImage(Toolkit.getDefaultToolkit().getImage(Introductiva.class.getResource("/Imagenes/Atlet.jpg")));
		setTitle("EII Atletismo - Crear carrera");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 900, 695);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setResizable(false);
		setLocationRelativeTo(null);

		contentPane.add(getLblNombre());
		contentPane.add(getTextFieldNombre());
		contentPane.add(getLblFechaDeCelebracin());
		contentPane.add(getLblLimiteDePlazas());
		contentPane.add(getTextFieldLimite());
		contentPane.add(getLblParticipantes());
		contentPane.add(getLblTipoDeCarrera());
		contentPane.add(getComboBoxTipo());
		contentPane.add(getLblDesnivel());
		contentPane.add(getTextFieldDesnivel());
		contentPane.add(getLblMetros());
		contentPane.add(getLblKilometraje());
		contentPane.add(getTextFieldDistancia());
		contentPane.add(getLblKilometros());
		contentPane.add(getBtnDefinirIntervalos());
		contentPane.add(getBtnCancelar());
		contentPane.add(getScrollPane());
		contentPane.add(getBtnAnadirCategoria());
		contentPane.add(getBtnNewButton());
		contentPane.add(getBtnEliminarCategoria());
		contentPane.add(getBtnActualizar());
		contentPane.add(getLblIban());
		contentPane.add(getTextFieldIBAN());
		contentPane.add(getRdbtnPreinscripcion());
		contentPane.add(getRdbtnInscripcion());
		contentPane.add(getDateChooser());

		comboBoxTipo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (((String) comboBoxTipo.getSelectedItem()).equals("montaña")) {
					textFieldDesnivel.setEnabled(true);
					lblDesnivel.setEnabled(true);
					lblMetros.setEnabled(true);
				} else {
					textFieldDesnivel.setEnabled(false);
					lblDesnivel.setEnabled(false);
					lblMetros.setEnabled(false);
				}
			}
		});
		rdbtnInscripcion.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				textFieldLimite.setEnabled(true);

			}
		});
		rdbtnPreinscripcion.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				textFieldLimite.setEnabled(false);

			}
		});
		textFieldDesnivel.setValue(new Integer(0));
		textFieldDistancia.setValue(new Integer(0));
		textFieldLimite.setValue(new Integer(0));
		textFieldIBAN.setText("");
		llenarTable();
		dateChooser.setMinSelectableDate(new Date());

	}

	public void ponFotoDeFondo(String foto) {
		JLabel labelFoto = new JLabel();
		labelFoto.setIcon(creaIcono(foto));
		labelFoto.setBounds(0, 0, 900, 600);

		contentPane.add(labelFoto);

	}

	/**
	 * Crear un icono a partir de la direccion de la imagen en el proyecto, en el
	 * tama�o original
	 * 
	 * @param foto direccion de la imagen
	 * @return el icono ya creado
	 */
	private Icon creaIcono(String foto) {
		String path = foto;
		URL url = this.getClass().getResource(path);
		ImageIcon icon = new ImageIcon(url);
		return icon;
	}

	private JLabel getLblNombre() {
		if (lblNombre == null) {
			lblNombre = new JLabel("Nombre:");
			lblNombre.setBounds(48, 16, 61, 16);
		}
		return lblNombre;
	}

	private JTextField getTextFieldNombre() {
		if (textFieldNombre == null) {
			textFieldNombre = new JTextField();
			textFieldNombre.setBounds(202, 11, 615, 26);
			textFieldNombre.setColumns(10);
		}
		return textFieldNombre;
	}

	private JLabel getLblFechaDeCelebracin() {
		if (lblFechaDeCelebracin == null) {
			lblFechaDeCelebracin = new JLabel("Fecha de celebración:");
			lblFechaDeCelebracin.setBounds(48, 67, 144, 16);
		}
		return lblFechaDeCelebracin;
	}

	private JLabel getLblLimiteDePlazas() {
		if (lblLimiteDePlazas == null) {
			lblLimiteDePlazas = new JLabel("Limite de Plazas:");
			lblLimiteDePlazas.setBounds(48, 390, 144, 16);
		}
		return lblLimiteDePlazas;
	}

	private JTextField getTextFieldLimite() {
		if (textFieldLimite == null) {
			textFieldLimite = new JFormattedTextField(new Integer(0));
			textFieldLimite.setBounds(202, 385, 130, 26);
			textFieldLimite.setColumns(10);
		}
		return textFieldLimite;
	}

	private JLabel getLblParticipantes() {
		if (lblParticipantes == null) {
			lblParticipantes = new JLabel("participantes");
			lblParticipantes.setBounds(344, 390, 106, 16);
		}
		return lblParticipantes;
	}

	private JLabel getLblTipoDeCarrera() {
		if (lblTipoDeCarrera == null) {
			lblTipoDeCarrera = new JLabel("Tipo de carrera:");
			lblTipoDeCarrera.setBounds(48, 126, 144, 16);
		}
		return lblTipoDeCarrera;
	}

	private JComboBox<String> getComboBoxTipo() {
		if (comboBoxTipo == null) {
			comboBoxTipo = new JComboBox<String>();
			comboBoxTipo.setModel(new DefaultComboBoxModel<String>(new String[] { "asfalto", "montaña" }));
			comboBoxTipo.setBounds(202, 122, 227, 27);
		}
		return comboBoxTipo;
	}

	private JLabel getLblDesnivel() {
		if (lblDesnivel == null) {
			lblDesnivel = new JLabel("Desnivel:");
			lblDesnivel.setBounds(49, 231, 61, 16);
		}
		return lblDesnivel;
	}

	private JTextField getTextFieldDesnivel() {
		if (textFieldDesnivel == null) {
			textFieldDesnivel = new JFormattedTextField(new Integer(0));
			textFieldDesnivel.setEnabled(false);
			textFieldDesnivel.setBounds(203, 226, 130, 26);
			textFieldDesnivel.setColumns(10);
		}
		return textFieldDesnivel;
	}

	private JLabel getLblMetros() {
		if (lblMetros == null) {
			lblMetros = new JLabel("Metros");
			lblMetros.setEnabled(false);
			lblMetros.setBounds(345, 231, 61, 16);
		}
		return lblMetros;
	}

	private JLabel getLblKilometraje() {
		if (lblKilometraje == null) {
			lblKilometraje = new JLabel("Kilometraje:");
			lblKilometraje.setBounds(52, 283, 144, 16);
		}
		return lblKilometraje;
	}

	private JTextField getTextFieldDistancia() {
		if (textFieldDistancia == null) {
			textFieldDistancia = new JFormattedTextField(new Integer(0));
			;
			textFieldDistancia.setBounds(206, 278, 130, 26);
			textFieldDistancia.setColumns(10);
		}
		return textFieldDistancia;
	}

	private JLabel getLblKilometros() {
		if (lblKilometros == null) {
			lblKilometros = new JLabel("Kilometros");
			lblKilometros.setBounds(348, 283, 81, 16);
		}
		return lblKilometros;
	}

	private JButton getBtnDefinirIntervalos() {
		if (btnDefinirIntervalos == null) {
			btnDefinirIntervalos = new JButton("Siguiente");
			VentanaCrearCarrera paraDevolver = this;
			btnDefinirIntervalos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(dateChooser.getDate() == null || textFieldIBAN.getText() == "" || textFieldDesnivel.getText() == "" || textFieldDistancia.getText() == "" || textFieldIBAN.getText() == "" || textFieldLimite.getText() == "" || textFieldNombre.getText() == "") {
						JOptionPane.showMessageDialog(null, "Debe rellenar todos los campos", "Error", JOptionPane.ERROR_MESSAGE);
					}
					else if((int)textFieldDistancia.getValue() <= 0) {
						JOptionPane.showMessageDialog(null, "La distancia no puede ser negativa ni 0", "Error", JOptionPane.ERROR_MESSAGE);
					}
					else if((int)textFieldLimite.getValue() < 0) {
						JOptionPane.showMessageDialog(null, "El numero de plazas no puede ser inferior a 0", "Error", JOptionPane.ERROR_MESSAGE);
					}
					else {
					Date celebracion = dateChooser.getDate();
					if (celebracion.before(new Date(Calendar.getInstance().getTimeInMillis()))) {
						JOptionPane.showMessageDialog(null, "La fecha de celebración es anterior a la actual",
								"Error - fecha de celebracion", JOptionPane.ERROR_MESSAGE);
					} else {

						CarreraDTO carrera = new CarreraDTO();
						carrera.nombre = textFieldNombre.getText();
						carrera.distancia = (int) textFieldDistancia.getValue();
						carrera.Fecha_competicion = dateChooser.getDate();
						carrera.tipo = ((String) comboBoxTipo.getSelectedItem());
						carrera.iban = textFieldIBAN.getText();
						if (!textFieldDesnivel.getText().equals("")) {
							carrera.desnivel = (int) textFieldDesnivel.getValue();
						}
						carrera.limite_plazas = (int) textFieldLimite.getValue();
						if (rdbtnInscripcion.isSelected()) {
							carrera.preinscripcion = false;
						} else {
							carrera.preinscripcion = true;
						}
						InsertarCarrera ia = new InsertarCarrera();
						ia.execute(carrera);
						setCarrera(carrera);
						if(rdbtnInscripcion.isSelected()) {
						int numeroPlazos = Integer.valueOf(JOptionPane.showInputDialog(null,
								"Numero de plazos y cancelaciones a definir", "Plazos y cancelaciones"));
						while(numeroPlazos <= 0) {
							numeroPlazos = Integer.valueOf(JOptionPane.showInputDialog(null,
									"Numero de plazos y cancelaciones a definir ( tiene que ser mayor que 0)", "Plazos y cancelaciones"));
						}
						VentanaPlazosYCancelacion ven = new VentanaPlazosYCancelacion(numeroPlazos, carrera.idCarrera,
								paraDevolver);
						ven.setVisible(true);
						dispose();
						}
						else {
							ventanaPreinscripcion();
						}
					}
				} 
				}
			});
			btnDefinirIntervalos.setBounds(745, 596, 139, 29);
		}
		return btnDefinirIntervalos;
	}
	private void setCarrera(CarreraDTO car) {
		this.carrera = car;
	}
	public CarreraDTO getCarrera() {
		return this.carrera;
	}
	private void ventanaPreinscripcion() {
		VentanaFechasPreinscripcion next = new VentanaFechasPreinscripcion(this);
		next.setVisible(true);
		dispose();
	}

	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					ventana.setVisible(true);
					dispose();
				}
			});
			btnCancelar.setBounds(745, 627, 139, 29);
		}
		return btnCancelar;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBounds(48, 423, 529, 233);
			scrollPane.setViewportView(getTable());
		}
		return scrollPane;
	}

	private JTable getTable() {
		if (table == null) {
			table = new JTable();
		}
		return table;
	}

	public void llenarTable() {
		DefaultTableModel modelo = new DefaultTableModel();
		String[] columnas = new String[] { "Nombre", "Edad Minima", "Edad Máxima" };
		table.setModel(modelo);
		for (String columna : columnas) {
			modelo.addColumn(columna);
		}
		for (CategoriaDTO categoria : categorias) {
			String[] fila = new String[3];
			fila[0] = categoria.nombre_categoria;
			fila[1] = String.valueOf(categoria.edad_minima);
			fila[2] = String.valueOf(categoria.edad_maxima);
			modelo.addRow(fila);
		}
	}

	private JButton getBtnAnadirCategoria() {
		if (btnAnadirCategoria == null) {
			btnAnadirCategoria = new JButton("Añadir Categoria");
			btnAnadirCategoria.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					mostrarAnadir();
				}
			});
			btnAnadirCategoria.setBounds(589, 423, 157, 29);
		}
		return btnAnadirCategoria;
	}

	public void mostrarEditar(CategoriaDTO cat) {
		VentanaEditar ve = new VentanaEditar(cat, this);
		ve.setVisible(true);
	}

	public void mostrarAnadir() {
		VentanaAddCategoria vac = new VentanaAddCategoria(this);
		vac.setVisible(true);
	}

	private JButton getBtnNewButton() {
		if (btnNewButton == null) {
			btnNewButton = new JButton("Editar categoria");
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (table.getSelectedRow() == -1) {
						JOptionPane.showMessageDialog(null, "No has seleccionado ninguna categoria", "Error",
								JOptionPane.ERROR_MESSAGE);
					} else {
						CategoriaDTO categoria = categorias.get(table.getSelectedRow());
						mostrarEditar(categoria);
					}
				}
			});
			btnNewButton.setBounds(589, 456, 157, 29);
		}
		return btnNewButton;
	}

	private JButton getBtnEliminarCategoria() {
		if (btnEliminarCategoria == null) {
			btnEliminarCategoria = new JButton("Eliminar Categoria");
			btnEliminarCategoria.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					categorias.remove(table.getSelectedRow());
					llenarTable();
				}
			});
			btnEliminarCategoria.setBounds(589, 487, 157, 29);
		}
		return btnEliminarCategoria;
	}

	private JButton getBtnActualizar() {
		if (btnActualizar == null) {
			btnActualizar = new JButton("Actualizar categorias");
			btnActualizar.setBounds(589, 514, 157, 29);
		}
		return btnActualizar;
	}

	private JLabel getLblIban() {
		if (lblIban == null) {
			lblIban = new JLabel("IBAN:");
			lblIban.setBounds(48, 340, 61, 16);
		}
		return lblIban;
	}

	private JTextField getTextFieldIBAN() {
		if (textFieldIBAN == null) {
			textFieldIBAN = new JTextField();
			textFieldIBAN.setBounds(202, 335, 585, 26);
			textFieldIBAN.setColumns(10);
		}
		return textFieldIBAN;
	}

	public void actualiza() {
		categorias = ListaCategorias.execute();
	}

	private JRadioButton getRdbtnPreinscripcion() {
		if (rdbtnPreinscripcion == null) {
			rdbtnPreinscripcion = new JRadioButton("Preinscripcion");
			buttonGroup.add(rdbtnPreinscripcion);
			rdbtnPreinscripcion.setBounds(48, 171, 141, 23);
		}
		return rdbtnPreinscripcion;
	}

	private JRadioButton getRdbtnInscripcion() {
		if (rdbtnInscripcion == null) {
			rdbtnInscripcion = new JRadioButton("Inscripcion");
			buttonGroup.add(rdbtnInscripcion);
			rdbtnInscripcion.setSelected(true);
			rdbtnInscripcion.setBounds(206, 171, 141, 23);
		}
		return rdbtnInscripcion;
	}
	private JDateChooser getDateChooser() {
		if (dateChooser == null) {
			dateChooser = new JDateChooser();
			dateChooser.setBounds(203, 62, 220, 26);
		}
		return dateChooser;
	}
}
