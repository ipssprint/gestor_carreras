package ui.VentanaCarrera;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.InputMethodEvent;
import java.awt.event.InputMethodListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.Date;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.toedter.calendar.JDateChooser;

import BBDD.InsertarCarrera;

public class VentanaFechasPreinscripcion extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JLabel lblFechaFinDe;
	private JLabel lblaPartirDe;
	private JButton btnFinalizar;
	private JButton btnAtrs;
	private VentanaCrearCarrera padre;
	private JDateChooser dateChooserFin;
	private JDateChooser dateChooserInicio;
	private JLabel lblFechaInicioPreinscripcion;
	/**
	 * Create the frame.
	 */
	public VentanaFechasPreinscripcion(VentanaCrearCarrera padre) {
		this.padre = padre;
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 451, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getLblFechaFinDe());
		contentPane.add(getLblaPartirDe());
		contentPane.add(getBtnFinalizar());
		contentPane.add(getBtnAtrs());
		contentPane.add(getDateChooserFin());
		contentPane.add(getDateChooserInicio());
		contentPane.add(getLblFechaInicioPreinscripcion());
		
		dateChooserInicio.setMinSelectableDate(Calendar.getInstance().getTime());
		dateChooserInicio.setMaxSelectableDate(padre.getCarrera().Fecha_competicion);
		dateChooserFin.setMaxSelectableDate(padre.getCarrera().Fecha_competicion);
		dateChooserFin.setMinSelectableDate(Calendar.getInstance().getTime());
		
		dateChooserInicio.addPropertyChangeListener(new PropertyChangeListener() {
			
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				dateChooserFin.setMinSelectableDate(dateChooserInicio.getDate());
				
			}
		});
	}
	private JLabel getLblFechaFinDe() {
		if (lblFechaFinDe == null) {
			lblFechaFinDe = new JLabel("Fecha fin de preinscripcion");
			lblFechaFinDe.setBounds(127, 140, 238, 16);
		}
		return lblFechaFinDe;
	}
	private JLabel getLblaPartirDe() {
		if (lblaPartirDe == null) {
			lblaPartirDe = new JLabel("(A partir de esta fecha nadie podrá inscribirse");
			lblaPartirDe.setBounds(79, 215, 326, 16);
		}
		return lblaPartirDe;
	}
	private JButton getBtnFinalizar() {
		if (btnFinalizar == null) {
			btnFinalizar = new JButton("Finalizar");
			btnFinalizar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(dateChooserFin.getDate() == null || dateChooserInicio.getDate() == null) {
						JOptionPane.showMessageDialog(null, "Debe rellenar todas las fechas", "Error", JOptionPane.ERROR_MESSAGE);
					}
					else {
					InsertarCarrera insertar = new InsertarCarrera();
					if(insertar.anadirPreinscripcion(new java.sql.Date( dateChooserInicio.getDate().getTime()), new java.sql.Date( dateChooserFin.getDate().getTime()), padre.getCarrera())) {
						JOptionPane.showMessageDialog(null, "La carrera se ha añadido correctamente", "Exito", JOptionPane.INFORMATION_MESSAGE);
						dispose();
					}
					else {
						JOptionPane.showMessageDialog(null, "Hubo un problema al añadir la carrera", "Error", JOptionPane.ERROR_MESSAGE);
						dispose();
					}
					}
				}
			});
			btnFinalizar.setBounds(230, 243, 117, 29);
		}
		return btnFinalizar;
	}
	private JButton getBtnAtrs() {
		if (btnAtrs == null) {
			btnAtrs = new JButton("Atrás");
			btnAtrs.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					padre.setVisible(true);
					dispose();
				}
			});
			btnAtrs.setBounds(90, 243, 117, 29);
		}
		return btnAtrs;
	}

	private JDateChooser getDateChooserFin() {
		if (dateChooserFin == null) {
			dateChooserFin = new JDateChooser();
			dateChooserFin.setBounds(90, 180, 265, 26);
		}
		return dateChooserFin;
	}
	private JDateChooser getDateChooserInicio() {
		if (dateChooserInicio == null) {
			dateChooserInicio = new JDateChooser();
			dateChooserInicio.setBounds(90, 85, 265, 26);
		}
		return dateChooserInicio;
	}
	private JLabel getLblFechaInicioPreinscripcion() {
		if (lblFechaInicioPreinscripcion == null) {
			lblFechaInicioPreinscripcion = new JLabel("Fecha inicio preinscripcion");
			lblFechaInicioPreinscripcion.setBounds(127, 41, 257, 16);
		}
		return lblFechaInicioPreinscripcion;
	}
}
