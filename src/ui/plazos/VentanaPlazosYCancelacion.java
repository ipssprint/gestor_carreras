package ui.plazos;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import BBDD.Listados.ListaCarrera;
import DTO.CarreraDTO;
import logica.ManejadorPlazo;
import logica.NumericHelper;
import logica.Plazo;
import logica.Plazos;
import ui.VentanaCarrera.VentanaCrearCarrera;
import util.Fecha;

public class VentanaPlazosYCancelacion extends JFrame {

	
	/**
	 * @author Javier Ardura
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JScrollPane scrollPane;
	private JPanel panelPlazo;
	private List<JPanel> paneles;
	private List<JPanel> panelesFechas;
	private JComboBox<Integer>[] panelesDias;
	private JComboBox<Integer>[] panelesMeses;	
	private JComboBox<Integer>[] panelesAnos;
	private int numeroPlazos;
	private String idCarrera;
	Plazos plazos;
	/**
	 * Create the frame.
	 */
	@SuppressWarnings("unchecked")
	public VentanaPlazosYCancelacion(int numeroPlazos, String idCarrera, VentanaCrearCarrera volver) {
		plazos = new Plazos();
		this.idCarrera = idCarrera;
		this.numeroPlazos = numeroPlazos;
		paneles = new ArrayList<>();
		panelesFechas = new ArrayList<>();
		panelesDias = new JComboBox[2*numeroPlazos];
		panelesMeses = new JComboBox[2*numeroPlazos];
		panelesAnos = new JComboBox[2*numeroPlazos];
		creaComboDias();
		creaComboMes();
		creaComboAno();
		setTitle("EII Atletismo: Establecimiento de plazos de inscripcion");
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaPlazosYCancelacion.class.getResource("/Imagenes/Atlet.jpg")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 632, 444);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		contentPane.add(getScrollPane(), BorderLayout.CENTER);
		setLocationRelativeTo(null);
		
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			creaPaneles();
			scrollPane = new JScrollPane(getPanelPlazo());
		}
		return scrollPane;
	}
	
	private JPanel getPanelPlazo() {
		if (panelPlazo == null) {
			panelPlazo = new JPanel();
			panelPlazo.setLayout(new GridLayout(numeroPlazos+1, 0, 0, 0));
			for(int i = 0; i< numeroPlazos+1; i++)
			{
				
				panelPlazo.add(paneles.get(i));
			}
			
		}
		return panelPlazo;
	}
	
	private void creaPaneles()
	{
		for(int i = 0; i< numeroPlazos; i++)
		{
			JPanel panel = new JPanel();
			panel.setLayout(new GridLayout(4, 0, 0, 0));
			JPanel panel2 = new JPanel();
			panel2.setLayout(new GridLayout(1, 3, 0, 0));
			JPanel panel3 = new JPanel();
			panel3.setLayout(new GridLayout(1, 3, 0, 0));
			panelesFechas.add(panel2);
			panelesFechas.add(panel3);
			panel.add(CreaLblPlazon());
			panel.add(panel2);
			panel.add(panel3);
			panel.add(creaPrecioInscripcion());
			paneles.add(panel);
		}
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(1, 1, 0, 0));
		panel.add(creaBoton());
		paneles.add(panel);
		
		rellenarPanelesFecha();
	}
	
	

	private JLabel CreaLblPlazon() {
		JLabel lblPlazo = new JLabel("Plazo "+ (paneles.size()+1));
			lblPlazo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		return lblPlazo;
	}
	
	
	private JTextField creaPrecioInscripcion() {
		
			JTextField txtPrecio = new JTextField();
			txtPrecio.setText("Introduzca el precio de inscripcion en el plazo Ej(25.30)");
			txtPrecio.setColumns(10);
			txtPrecio.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent arg0) {
					txtPrecio.selectAll();
				}
			});
		
		return txtPrecio;
	}
	private JButton creaBoton()
	{
		JButton boton =  new JButton();
		boton.setBackground(new Color(0xEEEEEE));
		boton.setBorder(null);
		boton.setIcon(new ImageIcon(VentanaCancelacion.class.getResource("/Imagenes/botonGuarda.png")));
		boton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			int aux =	JOptionPane.showConfirmDialog(rootPane, "¿Quiere guardar los cambios?");
				if(aux == JOptionPane.OK_OPTION)
				{	
					boolean t = guardaLosCambios();
					if(t)
					{
						int numeroPlazos = Integer.valueOf(JOptionPane.showInputDialog(null,
								"Numero de cancelaciones a definir", "Cancelaciones"));
						VentanaCancelacion ven = new VentanaCancelacion(numeroPlazos, getCarrera().idCarrera,
								null);
						ven.setVisible(true);
						dispose();
					}
					else 
						JOptionPane.showMessageDialog(rootPane, "Los plazos no son correctos");
			}
				else 
					return;
			
					
			}
		
			
	});
		return boton;
	}
	protected boolean guardaLosCambios() {
		BBDD.Plazos sql = new BBDD.Plazos();
		if(meteLosPlazos() && plazos.noSePisan())
		{
			for(Plazo p: plazos.getPlazos())
				sql.insertar(p.dameElDto());
			return true;
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	private boolean meteLosPlazos()
	{
		plazos.reinciaPlazos();
		for(int i = 0; i<paneles.size()-1; i++)
		{
			JPanel fecIni = (JPanel) paneles.get(i).getComponent(1);
			int diafi = (int) ((JComboBox<Integer>)fecIni.getComponent(0)).getSelectedItem();
			int mesfi = (int) ((JComboBox<Integer>)fecIni.getComponent(1)).getSelectedItem();
			int anofi = (int) ((JComboBox<Integer>)fecIni.getComponent(2)).getSelectedItem();
			String fi = diafi +"-"+mesfi+"-"+anofi;
			JPanel fecFin = (JPanel) paneles.get(i).getComponent(2);
			int diafifi = (int) ((JComboBox<Integer>)fecFin.getComponent(0)).getSelectedItem();
			int mesfifi = (int) ((JComboBox<Integer>)fecFin.getComponent(1)).getSelectedItem();
			int anofifi = (int) ((JComboBox<Integer>)fecFin.getComponent(2)).getSelectedItem();
			String ff = diafifi +"-"+mesfifi+"-"+anofifi;
			Double precio;
			if(NumericHelper.isNumericDouble(((JTextField)paneles.get(i).getComponent(3)).getText()))
				precio = Double.parseDouble(((JTextField)paneles.get(i).getComponent(3)).getText());
			else
				precio = -800.0;
			Plazo p = new Plazo(idCarrera, fi, ff, precio);
			if(!ManejadorPlazo.sinProblemas(p))
			{	JOptionPane.showMessageDialog(rootPane, ManejadorPlazo.problemas(p));
				return false;
				}
			else
				plazos.addToPlazos(p);
			
			
		}
		return true;
	}
	
	private JComboBox<Integer> creaCombobox(int lin, int lsup)
	{
		int j = 0;
		Integer[] items = new Integer[lsup-lin+1];
		for(int i = lin; i <= lsup; i++)
		{	items[j]= i;
			j++;
		}
		JComboBox<Integer> combo = new JComboBox<>(items);
		combo.setEditable(false);
		return combo;
	}
	
	
	
	private void rellenarPanelesFecha()
	{
		
		
		for(int i = 0; i < panelesFechas.size();i++)
		{
			Fecha f = new Fecha();
			panelesDias[i].setSelectedItem(f.getDia());
			panelesMeses[i].setSelectedItem(f.getMes());
			panelesAnos[i].setSelectedItem(f.getYear());
			panelesFechas.get(i).add(panelesDias[i]);
			panelesFechas.get(i).add(panelesMeses[i]);
			panelesFechas.get(i).add(panelesAnos[i]);
		}
	
	}
	
	private void creaComboDias()
	{
		for(int i = 0; i<numeroPlazos*2;i++)
		{
			JComboBox<Integer> combo = creaCombobox(1, 31);
			panelesDias[i] = combo;
		}
	}
	
	private void creaComboMes()
	{
		Integer[] lista = {1,2,3,4,5,6,7,8,9,10,11,12};
		for(int i = 0; i<numeroPlazos*2;i++)
		{
			int aux = i;
			JComboBox<Integer> combo = new JComboBox<>(lista);
			combo.setEditable(false);
			combo.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					int elMes = (int) panelesMeses[aux].getSelectedItem();
					int elAno = (int) panelesAnos[aux].getSelectedItem();
					Integer[] n = calculaMes(elMes,elAno);
					int slec = (int) panelesDias[aux].getSelectedItem();
					panelesDias[aux].removeAllItems();
					for(int k = 0; k < n.length; k++)
						panelesDias[aux].addItem(n[k]);
					
					if(panelesDias[aux].getItemCount()>=slec)
						panelesDias[aux].setSelectedItem(slec);
				}
				
							
				
			});
			panelesMeses[i] = combo;
		}
	}
	
	private void creaComboAno()
	{
		for(int i = 0; i<numeroPlazos*2;i++)
		{
			int aux = i;
			Fecha f = new Fecha();
			Fecha fin = new Fecha(getCarrera().Fecha_competicion);
			JComboBox<Integer> combo = creaCombobox(f.getYear(), fin.getYear());
			combo.setSelectedItem(new Fecha().getYear());
			combo.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					int elMes = (int) panelesMeses[aux].getSelectedItem();
					int elAno = (int) panelesAnos[aux].getSelectedItem();
					Integer[] n = calculaMes(elMes,elAno);
					int slec = (int) panelesDias[aux].getSelectedItem();
					panelesDias[aux].removeAllItems();
					for(int k = 0; k < n.length; k++)
						panelesDias[aux].addItem(n[k]);
					
					if(panelesDias[aux].getItemCount()>=slec)
						panelesDias[aux].setSelectedItem(slec);
					panelesMeses[aux].setSelectedItem(elMes);
				}
				
							
				
			});
			panelesAnos[i] = combo;
		}
	}

	private Integer[] calculaMes(int mes, int year) {
		int diaMaximo = new Fecha().ultimoDiaMes(mes, year);
		Integer[] lista = new Integer[diaMaximo];
		for(int i = 1; i<= diaMaximo;i++)
			lista[i-1] = i;
		return lista;
	}
	
	private CarreraDTO getCarrera()
	{
		
		CarreraDTO findCarreraById = ListaCarrera.findCarreraById(idCarrera);
		return findCarreraById;
	}

}
