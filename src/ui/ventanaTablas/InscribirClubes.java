package ui.ventanaTablas;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import BBDD.InscribirAtletaSegunTipoInscripcion;
import BBDD.PlazasDisponibles;
import BBDD.Listados.ListaCarrera;
import DTO.AtletaDTO;
import logica.Atleta;
import logica.ManejadorInscripciones;
import util.Fecha;

public class InscribirClubes extends VentanaTablas{

	private JPanel panelSur;
	private JButton btnInscribirClub;
	private String club;
	private JButton btnCancelar;
	private JPanel panelNorte;
	private JPanel panelNorte1;
	private JTextField tfield;
	private JPanel panelNorte2;
	public InscribirClubes(String idCarrera) {
		super("Inscripcion de clubes", 
				new String[] {  "DNI", "Apellidos, Nombre", "Fecha Nacimiento", "Sexo"}
		, idCarrera);
		getTableALlenar().setEnabled(true);
		getTableALlenar().setRowSorter(null);
		getContentPane().add(getPanelSur(), BorderLayout.SOUTH);
		getContentPane().add(getPanelNorte(), BorderLayout.NORTH);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void addFilasCarrera(DefaultTableModel modelo) {
		Object[] nuevaFila = new Object[4];
		
		int numero_disponibles= PlazasDisponibles.getNumeroDePlazasDisponibles(getIdCarrera());
		
		for (int i = 0; i < numero_disponibles; i++) {

			nuevaFila[0] = "";
			nuevaFila[1] = "";
			nuevaFila[2] = "";
			nuevaFila[3] = "";
			modelo.addRow(nuevaFila);

		}
		
	}

	@Override
	public void queHacer(int fila, int column) {
			
	}

	private JPanel getPanelSur() {
		if (panelSur == null) {
			panelSur = new JPanel();
			panelSur.setLayout(new GridLayout(1, 4, 0, 0));

			panelSur.add(new JPanel());
			panelSur.add(new JPanel());
			panelSur.add(getBtnInscribirClub());
			panelSur.add(getBtnCancelar());
		}
		return panelSur;
	}
	
	private JPanel getPanelNorte() {
		if (panelNorte == null) {
			panelNorte = new JPanel();
			panelNorte.setLayout(new GridLayout(2, 1, 0, 0));
			panelNorte.add(getPanelNorte1());
			panelNorte.add(getPanelNorte2());
		}
		return panelNorte;
	}
	private JPanel getPanelNorte1() {
		if (panelNorte1 == null) {
			panelNorte1 = new JPanel();
			panelNorte1.setLayout(new GridLayout(1, 2, 0, 0));
			if(InscribirAtletaSegunTipoInscripcion.tienePreinscripcion(getIdCarrera()))
			{	
				panelNorte1.add(new JLabel(
						"Atletas a preinscribir en la carrera " + 
				ListaCarrera.findCarreraById(getIdCarrera()).nombre));
			}
			else{
				
			panelNorte1.add(new JLabel(
					"Atletas a inscribir en la carrera " + 
			ListaCarrera.findCarreraById(getIdCarrera()).nombre));
			}
			panelNorte1.add(new JLabel("Ejemplo: 12345678A Sanchez Alvarez, Juan 08-08-1990 masculino"));
	
		}
		return panelNorte1;
	}
	
	private JPanel getPanelNorte2() {
		if (panelNorte2 == null) 
			panelNorte2 = new JPanel();
			panelNorte2.setLayout(new GridLayout(1, 2, 0, 0));
							
			panelNorte2.add(new JLabel("Club: "));
			
			panelNorte2.add(getTf());
	
		
		return panelNorte2;
	}
	private JButton getBtnInscribirClub() {
		if (btnInscribirClub == null) {
			btnInscribirClub = new JButton("Añadir atletas");
			btnInscribirClub.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					inscribe();
				}
			});
		}
		return btnInscribirClub;
	}
	private JTextField getTf()
	{
		if(tfield == null)
		{
			tfield = new JTextField();
		}
		return tfield;
	}
	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					int aux =	JOptionPane.showConfirmDialog(rootPane, "No se guardaran los cambios");
					if(aux == JOptionPane.OK_OPTION)
					{	
						dispose();
					}
				}
			});
		}
		return btnCancelar;
	}
	
	private void inscribe()
	{
		ArrayList<Atleta> atletas = new ArrayList<>();
		club = tfield.getText();
		for(int i = 0; i<getTableALlenar().getRowCount(); i++)
		{
			if(!(getTableALlenar().getValueAt(i, 0).toString().isEmpty()
					&& getTableALlenar().getValueAt(i, 1).toString().isEmpty()
					&& getTableALlenar().getValueAt(i, 2).toString().isEmpty()
					&& getTableALlenar().getValueAt(i, 3).toString().isEmpty()))
			
		{	Atleta a = new Atleta();
			a.DNI = getTableALlenar().getValueAt(i, 0).toString();
			String nombreAp = (String) getTableALlenar().getValueAt(i, 1).toString();
			String[] aux = nombreAp.split(", ");
			if(aux.length != 2)
			{	JOptionPane.showMessageDialog(rootPane, "Formato de apellidos y nombre incorrecto"
					,"Error",JOptionPane.ERROR_MESSAGE);
				a.nombre = "";
				a.apellidos = "";
			}
			else{
				a.nombre = aux[1];
				a.apellidos = aux[0];
			}
			a.fecha_nacimiento = getTableALlenar().getValueAt(i, 2).toString();
			a.sexo = getTableALlenar().getValueAt(i, 3).toString();
			atletas.add(a);
		}
			
		}
		if(atletas.isEmpty())
			JOptionPane.showMessageDialog(rootPane, "No hay ningún atleta para añadir"
					,"Error",JOptionPane.ERROR_MESSAGE);
		ManejadorInscripciones m = new ManejadorInscripciones();
		if(!m.sePisan(atletas) && m.todoVaBien(atletas, getIdCarrera(), club))
		{
			for(Atleta a: atletas)
			{	
				AtletaDTO ad = new AtletaDTO();
				ad.DNI = a.DNI;
				ad.nombre = a.nombre;
				ad.apellidos = a.apellidos;
				ad.sexo = a.sexo;
				ad.fecha_nacimiento = new Fecha(a.fecha_nacimiento).getFecha();
				InscribirAtletaSegunTipoInscripcion
				.execute(ad, getIdCarrera(), club);
			}
			dispose();
		}
		else if(m.sePisan(atletas))
			JOptionPane.showMessageDialog(rootPane, "Hay algun atleta repetido"
					,"Error",JOptionPane.ERROR_MESSAGE);
		else
		{	
			String pri = m.todoVaBienString(atletas, getIdCarrera(), club);
			JOptionPane.showMessageDialog(rootPane, pri
					,"Error",JOptionPane.ERROR_MESSAGE);
		}
		
	}
}
