package ui.ventanaTablas;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;

public abstract class VentanaTablas extends JFrame {

	/**
	 * Ventana Carreras que las lista y permite que te inscribas o veas los inscritos
	 * @author Javier Ardura
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JPanel panelListadoTabla;
	private JScrollPane scrollPaneTabla;
	private JTable tableALlenar;
	private DefaultTableModel modeloTableALlenar;
	private String nb;
	private String idCarrera;

	
	
	
	/**
	 * Create the frame.
	 * @wbp.parser.constructor
	 */
	public VentanaTablas(String titulo, String[] columnas) {
		setTitle("EII Atletismo: " + titulo);
		nb = columnas[columnas.length-1];
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaTablas.class.getResource("/Imagenes/Atlet.jpg")));
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 849, 428);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		contentPane.add(getPanelListado(columnas), BorderLayout.CENTER);
		setLocationRelativeTo(null);
		
		
	}
	
	public VentanaTablas(String titulo, String[] columnas, String idCarrera) {
		this.idCarrera = idCarrera;
		setTitle("EII Atletismo: " + titulo);
		nb = columnas[columnas.length-1];
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaTablas.class.getResource("/Imagenes/Atlet.jpg")));
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 849, 428);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		contentPane.add(getPanelListado(columnas), BorderLayout.CENTER);
		setLocationRelativeTo(null);
		
		
	}
	public VentanaTablas(String titulo) {
		setTitle(titulo);
//		nb = columnas[columnas.length-1];
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaTablas.class.getResource("/Imagenes/Atlet.jpg")));
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 849, 428);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
//		contentPane.add(getPanelListado(columnas), BorderLayout.CENTER);
		setLocationRelativeTo(null);
	}

	public JPanel getPanelListado(String[] columnas) {
		if (panelListadoTabla == null) {
			panelListadoTabla = new JPanel();
			panelListadoTabla.setLayout(new GridLayout(0, 1, 0, 0));
			panelListadoTabla.add(getScrollPaneTabla(columnas));
		}
		return panelListadoTabla;
	}
	private JScrollPane getScrollPaneTabla(String[] columnas) {
		if (scrollPaneTabla == null) {
			scrollPaneTabla = new JScrollPane(tableALlenar);
			scrollPaneTabla.setViewportView(getTableALlenar(columnas));
		}
		return scrollPaneTabla;
	}
	private JTable getTableALlenar(String[] columnas) {
		
		
		if (tableALlenar == null) {
			 modeloTableALlenar = new DefaultTableModel(columnas,0);
			tableALlenar = new JTable(modeloTableALlenar);
			tableALlenar.setEnabled(false);
			tableALlenar.setDefaultRenderer(Object.class, new util.Render());
			tableALlenar.addMouseListener(new MouseAdapter() {
			    public void mousePressed(MouseEvent mouseEvent) {
			    	Point point = mouseEvent.getPoint();
			        int column = tableALlenar.columnAtPoint(point);
			        int fila = tableALlenar.rowAtPoint(point);
			        if (mouseEvent.getClickCount() == 1 && (column == columnas.length-1 ||column == columnas.length-2)) {
			         
			        	if(modeloTableALlenar.getValueAt(fila, column) instanceof Component)
			        	{Component comp = (Component) modeloTableALlenar.getValueAt(fila, column);
			        	JButton botoo = null;
						if(comp instanceof JButton)
			        		 botoo = (JButton) comp;
			        	
			        	if(comp instanceof JButton&& botoo != null &&!botoo.getText().equals("NO DISPONIBLE"))
			        		queHacer(fila,column);
			        	}
			        }
			       
			        
			    }
			});
			TableColumnModel cmodel = tableALlenar.getColumnModel();
			cmodel.getColumn(0).setPreferredWidth(50);
			cmodel.getColumn(1).setPreferredWidth(30);
			tableALlenar.setBackground(Color.white);
			addFilasCarrera(this.modeloTableALlenar);
			
		}
		
		
		tableALlenar.setRowSorter(new TableRowSorter<DefaultTableModel>(modeloTableALlenar));
		return tableALlenar;
	}
	
	public abstract void addFilasCarrera(DefaultTableModel modelo);
	
	
	public JButton creaBotonConFuncion(String string) {
		JButton b = new JButton(string);
		b.setSize(tableALlenar.getRowHeight(0), tableALlenar.getRowHeight(0));
		
		b.setBorder(null);
		
		return b;
	}

	public JTextArea creaTextAreaConTexto(String texto)
	{
		JTextArea t = new JTextArea();
		t.setEditable(false);
		t.setBorder(null);
		t.setBackground(Color.WHITE);
		t.setText(texto);
		t.setLineWrap(true);
		t.setWrapStyleWord(true);
		t.setLayout(new GridLayout(1,1,1,1));
		return t;
	}
	
	public void lanzaUnaNuevaVentana(JFrame jf)
	{
		jf.setVisible(true);
	}
	
	
	public void lanzaUnaNuevaVentanaOcultando(JFrame jf)
	{
		jf.setVisible(true);
		this.dispose();
	}
	public abstract void queHacer(int fila, int column);
	
	public String getnb(){
		return this.nb;
	}
	
	public String getIdCarrera(){
		return this.idCarrera;
	}
	
	public DefaultTableModel getModeloTableALlenar() {
		return this.modeloTableALlenar;
	}
	
	public JTable getTableALlenar() {
		return this.tableALlenar;
	}
	
	
	
	
}