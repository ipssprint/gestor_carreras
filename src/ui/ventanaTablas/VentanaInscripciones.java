package ui.ventanaTablas;


import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

import BBDD.BuscaAtletaEnCarrera;
import BBDD.Listados.ListaParticipa;
import DTO.AtletaDTO;
import DTO.ParticipaDTO;
import logica.Operaciones;
import util.Fecha;

public class VentanaInscripciones extends VentanaTablas {

	/**
	 * Ventana que muestra las inscripciones a una determinada carrera
	 * @author Javier Ardura
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<AtletaDTO> aux;


	/**
	 * Create the frame.
	 */
	public VentanaInscripciones(String idCarrera) {
		super("Listado de carreras",new String[] {"DNI", "Nombre","Apellidos" ,"Categor�a", "Fecha de Inscripci�n", "Estado Inscripci�n" }, idCarrera);
		//Modificacion
		addBotonGenerarDorsales(getContentPane());	
	}

	@Override
	public void addFilasCarrera(DefaultTableModel modelo) {
		Object[] nuevaFila = new Object[6];
		ListaParticipa lp = new ListaParticipa();
		aux = (ArrayList<AtletaDTO>) lp.execute(super.getIdCarrera());

		for(AtletaDTO m : aux)
		{
			ParticipaDTO participante = BuscaAtletaEnCarrera.execute(super.getIdCarrera(), m.DNI);
			
			
			nuevaFila[0] = creaTextAreaConTexto(m.DNI);
			nuevaFila[1] =	creaTextAreaConTexto(m.nombre) ;
			nuevaFila[2] = creaTextAreaConTexto(m.apellidos);
			if(participante != null)
			{
				nuevaFila[3] = creaTextAreaConTexto(participante.nombre_categoria);
				nuevaFila[5] = creaTextAreaConTexto(participante.tipo_inscripcion);
			}
			nuevaFila[4] = creaTextAreaConTexto(new Fecha(lp.getFechaInscripcionbyparticipant(m.DNI, super.getIdCarrera())).printFecha());
			
			modelo.addRow(nuevaFila);
		}
		
	}

	@Override
	public void queHacer(int fila,int column) {		
	}
	
	public void addBotonGenerarDorsales(Container container) {
		//Boton a añadir
		JButton boton = new JButton("Generar dorsales");
		boton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Operaciones op = new Operaciones();
				op.generarDorsales();
			}
		});
		
		//Panel en el que se añade
		JPanel panelBoton = new JPanel();
		panelBoton.setLayout(new BorderLayout(0, 0));
		panelBoton.add(boton, BorderLayout.EAST);
		
		//Añadir panel al contentPane pasado como parametro
		container.add(panelBoton, BorderLayout.SOUTH);
	}



}
