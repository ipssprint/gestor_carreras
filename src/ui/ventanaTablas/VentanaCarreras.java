package ui.ventanaTablas;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

import BBDD.InscribirAtletaSegunTipoInscripcion;
import BBDD.PlazasDisponibles;
import BBDD.Plazos;
import BBDD.Listados.ListaCarrera;
import DTO.CarreraDTO;
import DTO.PreinscripcionDTO;
import logica.AnularInscripcion;
import ui.VentanaCarrera.VentanaCrearCarrera;
import ui.VentanasInscripcion.VentanaRegistro;
import ui.ventanaClub.VentanaInscripcionClub;
import util.Fecha;

public class VentanaCarreras extends VentanaTablas {

	/**
	 * Ventana Carreras que las lista y permite que te inscribas o veas los
	 * inscritos
	 * 
	 * @author Javier Ardura
	 */
	private static final long serialVersionUID = 1L;

	private ArrayList<CarreraDTO> aux;
	private String nb;
	private JButton btnAadirCarrera, btnCancelarInscripcion;
	private JPanel panel;
	private boolean esClub;

	/**
	 * Create the frame.
	 */
	public VentanaCarreras(String nb) {
		super("Listado de carreras", new String[] { "Nombre", "Tipo", "Cuota de inscripcion", "Distancia",
				"Fecha fin inscripcion", "Fecha del evento", "Desnivel", "Plazas disponibles" ,"Anulacion", nb });
		this.nb = nb;
		getContentPane().add(getPanel(), BorderLayout.SOUTH);
		esClub = false;

	}

	private void lanzaVentanaRegistro(CarreraDTO c) {
		lanzaUnaNuevaVentanaOcultando(new VentanaRegistro(this, c));
	}

	private void lanzaVentanaIncribirClub(CarreraDTO c) {
		lanzaUnaNuevaVentana(new InscribirClubes(c.idCarrera));
	}
	
	@Override
	public void addFilasCarrera(DefaultTableModel model) {
		Object[] nuevaFila = new Object[10];
		aux = (ArrayList<CarreraDTO>) new ListaCarrera().executeActuales();
		addResto();

		for (CarreraDTO m : aux) {

			nuevaFila[0] = creaTextAreaConTexto(m.nombre);
			nuevaFila[1] = creaTextAreaConTexto(m.tipo);
			Double prec = new Plazos().precioFecha(new Fecha().printFechaSQLvalueOf(),m.idCarrera);
			if(prec != null)
			nuevaFila[2] = creaTextAreaConTexto(prec + " €");
			else
				nuevaFila[2] = creaTextAreaConTexto("No hay inscripcion disponible" + " €");
			nuevaFila[3] = creaTextAreaConTexto(m.distancia + " km");
			if(m.Fecha_fin_inscripcion != null)
				nuevaFila[4] = creaTextAreaConTexto(new Fecha(m.Fecha_fin_inscripcion).printFecha());
			else{
				PreinscripcionDTO predto = InscribirAtletaSegunTipoInscripcion.damelaPreinscripcion(m.idCarrera);
				nuevaFila[4] = creaTextAreaConTexto(new Fecha(predto.fechaFin).printFecha());
				}
			
			nuevaFila[5] = creaTextAreaConTexto(new Fecha(m.Fecha_competicion).printFecha());
			if(!InscribirAtletaSegunTipoInscripcion.tienePreinscripcion(m.idCarrera))
				nuevaFila[7] = creaTextAreaConTexto(PlazasDisponibles.getNumeroDePlazasDisponibles(m.idCarrera)+"");
			else
				nuevaFila[7] = creaTextAreaConTexto("");
			nuevaFila[8] = getBtnCancelarInscripcion(m.idCarrera);
			nuevaFila[6] = creaTextAreaConTexto(m.desnivel+"");
			
			if(prec != null || InscribirAtletaSegunTipoInscripcion.tienePreinscripcionAhora(m.idCarrera))
			{	
				nuevaFila[9] = creaBotonConFuncion(super.getnb());
				if(InscribirAtletaSegunTipoInscripcion.tienePreinscripcionAhora(m.idCarrera)&& !getnb().equals("Ver inscritos"))
					((JButton) nuevaFila[9]).setText("Preinscribirse");
				
			}
			else
			{	if(!getnb().equals("Ver inscritos")){
				JButton bt7 = new JButton("NO DISPONIBLE");
				bt7.setEnabled(false);
				nuevaFila[9] = bt7;
			}
			
			}
			model.addRow(nuevaFila);
			}

			

		
	}

	@Override
	public void queHacer(int fila, int column) {
		
		if (column == 6) {
			String dni = JOptionPane.showInputDialog(this, "Introduzca el dni: ");
			double diferencia=AnularInscripcion.execute(dni, aux.get(fila).idCarrera);
			if (diferencia > -1)
				JOptionPane.showMessageDialog(rootPane, "La inscripcion se anulo correctamente" +" se le devolvera su cuota "+diferencia+" €");
			else {
				JOptionPane.showMessageDialog(rootPane, "No ha sido posible anular la inscripcion");
			}

		} else if (nb.equals("Inscribirse") && esClub == false)
			lanzaVentanaRegistro(aux.get(fila));
		else if(nb.equals("Inscribirse") && esClub)
			lanzaVentanaIncribirClub(aux.get(fila));
		else if(nb.equals("Inscribirse-Lotes")) {
			lanzaVentanaRegistroClub(aux.get(fila));
		}
		else
			super.lanzaUnaNuevaVentana(new VentanaInscripciones(aux.get(fila).idCarrera));

	}

	private void lanzaVentanaRegistroClub(CarreraDTO carreraDTO) {
		lanzaUnaNuevaVentanaOcultando(new VentanaInscripcionClub(carreraDTO));
		
	}

	private JButton getBtnCancelarInscripcion(String idCarrera) {
		if (btnCancelarInscripcion == null) {
			btnCancelarInscripcion = new JButton("Anular Inscripcion");
		}

		return btnCancelarInscripcion;
	}

	private JButton getBtnAadirCarrera() {
		if (btnAadirCarrera == null) {
			btnAadirCarrera = new JButton("Añadir carrera");
			btnAadirCarrera.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					siguienteVentana();
				}
			});
		}
		return btnAadirCarrera;
	}

	private void siguienteVentana() {
		VentanaCrearCarrera ventana = new VentanaCrearCarrera(this);
		ventana.setVisible(true);
		dispose();
	}

	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setLayout(new BorderLayout(0, 0));
			panel.add(getBtnAadirCarrera(), BorderLayout.EAST);
		}
		return panel;
	}
	
	public void ocultarPanel()
	{
		getContentPane().remove(panel);
	}

	public void activaClub() {
		esClub = true;
		
	}
	
	
	private void addResto()
	{
		ArrayList<PreinscripcionDTO> ff = 
				InscribirAtletaSegunTipoInscripcion.listaConPreinscripcionActiva();
		for(PreinscripcionDTO pd : ff){
			if(!estaYa(pd.idCarrera))
				aux.add(ListaCarrera.findCarreraById(pd.idCarrera));
			}
	}

	private boolean estaYa(String idCarrera) {
		for(int i = 0; i < aux.size(); i++)
			if(idCarrera == aux.get(i).idCarrera)
				return true;
		return false;
	}
	public void actualizar()
	{
		for(int i = 0; i< getModeloTableALlenar().getRowCount();i++)
			getModeloTableALlenar().removeRow(i);
		
		addFilasCarrera(getModeloTableALlenar());
	}
	
}