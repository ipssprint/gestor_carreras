package ui.ventanaResultados;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class DialogoNoHayParciales extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JLabel lblEsteParticipanteNo;
	private JLabel lblParcialesEnEsta;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			DialogoNoHayParciales dialog = new DialogoNoHayParciales();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public DialogoNoHayParciales() {
		setResizable(false);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		contentPanel.add(getLblEsteParticipanteNo());
		contentPanel.add(getLblParcialesEnEsta());
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						dispose();;
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		setLocationRelativeTo(null);
	}
	private JLabel getLblEsteParticipanteNo() {
		if (lblEsteParticipanteNo == null) {
			lblEsteParticipanteNo = new JLabel("Este participante no tiene");
			lblEsteParticipanteNo.setFont(new Font("Tahoma", Font.PLAIN, 34));
			lblEsteParticipanteNo.setBounds(15, 43, 414, 37);
		}
		return lblEsteParticipanteNo;
	}
	private JLabel getLblParcialesEnEsta() {
		if (lblParcialesEnEsta == null) {
			lblParcialesEnEsta = new JLabel("parciales en esta carrera");
			lblParcialesEnEsta.setFont(new Font("Tahoma", Font.PLAIN, 34));
			lblParcialesEnEsta.setBounds(15, 90, 414, 37);
		}
		return lblParcialesEnEsta;
	}
}
