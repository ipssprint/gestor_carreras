package ui.ventanaResultados;

import java.awt.BorderLayout;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import DTO.ControlDTO;
import ui.ventanaTablas.VentanaTablas;

public class VentanaResultadosTiemposParciales extends VentanaTablas{

	private List<ControlDTO> parciales;
	private String dorsal;
	private String nombre;
	private String apellidos;
	
	public VentanaResultadosTiemposParciales(List<ControlDTO> parciales, String dorsal, String nombre, String apellidos) {
		super("Tiempos parciales de x carrera");
		this.parciales = parciales;
		this.dorsal = dorsal;
		this.nombre = nombre;
		this.apellidos = apellidos;
			
		String[] nombreColumnas = getStringParciales();
		getContentPane().add(getPanelListado(nombreColumnas), BorderLayout.CENTER);
		
		int filasABorrar = getModeloTableALlenar().getRowCount();
		for (int i=0; i < filasABorrar; i++)
			getModeloTableALlenar().removeRow(0);
		addFilasCarrera(getModeloTableALlenar());
		setBounds(100, 120, 849, 100);
		setLocationRelativeTo(null);
			
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void addFilasCarrera(DefaultTableModel modelo) {
		Object[] nuevaFila = new Object[this.parciales.size()+3];
		nuevaFila[0] = dorsal;
		nuevaFila[1] = nombre;
		nuevaFila[2] = apellidos;
		for (int i=3; i < nuevaFila.length; i++) {
			if (parciales.get(i-3).tiempo == null)
				nuevaFila[i] = creaTextAreaConTexto("---");
			else
				nuevaFila[i] = creaTextAreaConTexto("" + parciales.get(i-3).tiempo);
		}
		
		modelo.addRow(nuevaFila);	
	}

	@Override
	public void queHacer(int fila, int column) {
		// TODO Auto-generated method stub
		
	}
	
	private String[] getStringParciales(){
		String[] stringParciales = new String[this.parciales.size()+3];
		stringParciales[0] = "Dorsal";
		stringParciales[1] = "Nombre";
		stringParciales[2] = "Apellidos";
		for (int i=3; i < stringParciales.length; i++)
			stringParciales[i] = "Parcial " + (i-2);
		
		return stringParciales;
	}

}
