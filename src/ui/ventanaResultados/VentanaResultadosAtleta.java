package ui.ventanaResultados;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import DTO.ControlDTO;
import DTO.ParticipaDTO;
import DTO.ResultadoDeAtletaEnCarrerasDTO;
import logica.Operaciones;
import ui.ventanasIntroductivas.VentanaOpciones;

public class VentanaResultadosAtleta extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JPanel panel;
	private JTextField txtIntroduzcaElDni;
	private JButton btnBuscar;
	private JScrollPane scrollPane;
	private JTable table;
	private Object[][] datos;

	private JButton btnAtrs;
	private VentanaOpciones vO;

	/**
	 * Create the frame.
	 * 
	 * @param ventanaOpciones
	 */
	public VentanaResultadosAtleta(VentanaOpciones ventanaOpciones) {
		setIconImage(
				Toolkit.getDefaultToolkit().getImage(VentanaResultadosAtleta.class.getResource("/Imagenes/Atlet.jpg")));
		setTitle("EII Atletismo: Resultados atleta");
		vO = ventanaOpciones;
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1117, 697);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		contentPane.add(getPanel(), BorderLayout.NORTH);
		contentPane.add(getScrollPane(), BorderLayout.CENTER);
		setLocationRelativeTo(null);
	}

	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			panel.add(getBtnAtrs());
			panel.add(getTxtIntroduzcaElDni());
			panel.add(getBtnBuscar());
		}
		return panel;
	}

	private JTextField getTxtIntroduzcaElDni() {
		if (txtIntroduzcaElDni == null) {
			txtIntroduzcaElDni = new JTextField();
			txtIntroduzcaElDni.setFont(new Font("Tahoma", Font.PLAIN, 15));
			txtIntroduzcaElDni.setToolTipText("INTRODUZCA EL DNI");
			txtIntroduzcaElDni.setText("INTRODUZCA EL DNI");
			txtIntroduzcaElDni.setColumns(10);
		}
		return txtIntroduzcaElDni;
	}

	private JButton getBtnBuscar() {
		if (btnBuscar == null) {
			btnBuscar = new JButton("BUSCAR");
			btnBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					ResultadoDeAtletaEnCarrerasDTO r = cargarDatosEnTabla(getTxtIntroduzcaElDni().getText());
					datos = new Object[r.inscripciones.size()][10 + r.maximosControles];
					String[] columnNames = asignarNuevosNombresPuntosDeControl(r.maximosControles);
					for (int j = 0; j < r.participaciones.size(); j++) {
						ParticipaDTO s = r.participaciones.get(j);
						if (r.controles.get(s.idCarrera) != null) {

							List<ControlDTO> cs = r.controles.get(r.participaciones.get(j).idCarrera);
							int i = 0;
							for (ControlDTO c : cs) {
								if(c.tiempo!=null)
								datos[j][10 + i] = c.tiempo;
								else datos[j][10 + i] = "--";
								i++;
							}

						}
						if (r.participaciones.get(j).dorsal != 0)
							datos[j][0] = r.participaciones.get(j).dorsal;
						else
							datos[j][0] = "NO HAY INFORMACION DISPONIBLE";
						if (r.atleta.DNI != null)
							datos[j][1] = r.atleta.DNI;
						else
							datos[j][1] = "NO HAY INFORMACION DISPONIBLE";
						if (r.atleta.nombre != null)
							datos[j][2] = r.atleta.nombre;
						else
							datos[j][2] = "NO HAY INFORMACION DISPONIBLE";
						if (r.atleta.sexo != null)
							datos[j][3] = r.atleta.sexo;
						else
							datos[j][3] = "NO HAY INFORMACION DISPONIBLE";
						if (r.inscripciones.get(r.participaciones.get(j).idCarrera).nombre != null)
							datos[j][4] = r.inscripciones.get(r.participaciones.get(j).idCarrera).nombre;
						else
							datos[j][4] = "NO HAY INFORMACION DISPONIBLE";
						if (r.participaciones.get(j).tipo_inscripcion != null)
							datos[j][5] = r.participaciones.get(j).tipo_inscripcion;
						else
							datos[j][5] = "NO HAY INFORMACION DISPONIBLE";
						if (r.participaciones.get(j).tiempo != null) {
							datos[j][6] = r.participaciones.get(j).tiempo;
							if (r.inscripciones.get(r.participaciones.get(j).idCarrera).distancia > 0)
								datos[j][9] = r.inscripciones.get(r.participaciones.get(j).idCarrera).distancia
										/ r.participaciones.get(j).tiempo.getMinutes();

						} else
							datos[j][6] = "NO HAY INFORMACION DISPONIBLE";
						if (r.participaciones.get(j).posicion != 0)
							datos[j][7] = r.participaciones.get(j).posicion;
						else
							datos[j][7] = "NO HAY INFORMACION DISPONIBLE";

						if (r.participaciones.get(j).nombre_categoria != null)
							datos[j][8] = r.participaciones.get(j).nombre_categoria;
						else
							datos[j][8] = "NO HAY INFORMACION DISPONIBLE";

					}
					DefaultTableModel dtm = new DefaultTableModel(datos, columnNames);
					getTable().setModel(dtm);
					getTable().repaint();
				}

			});
			btnBuscar.setMnemonic('B');
		}
		return btnBuscar;
	}

	private String[] asignarNuevosNombresPuntosDeControl(int maximosControles) {
		String[] column = { "DORSAL", "DNI", "NOMBRE", "SEXO", "NOMBRE CARRERA", "ESTADO DE INSCRIPCION", "TIEMPO",
				"POSICION", "CATEGORIA", "KM/MIN" };
		if (maximosControles > 0) {

			String[] column2 = new String[10 + maximosControles];
			column2[0] = "DORSAL";
			column2[1] = "DNI";
			column2[2] = "NOMBRE";
			column2[3] = "SEXO";
			column2[4] = "NOMBRE CARRERA";
			column2[5] = "ESTADO DE INSCRIPCION";
			column2[6] = "TIEMPO";
			column2[7] = "POSICION";
			column2[8] = "CATEGORIA";
			column2[9] = "KM/MIN";

			for (int i = 0; i < maximosControles; i++)
				column2[10 + i] = "Control: " + i;

			return column2;

		}
		return column;

	}

	private ResultadoDeAtletaEnCarrerasDTO cargarDatosEnTabla(String dni) {
		ResultadoDeAtletaEnCarrerasDTO r = Operaciones.calcularResultadosDeAtleta(dni);
		return r;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTable());
			table.setFillsViewportHeight(true);

		}
		return scrollPane;
	}

	private JTable getTable() {
		if (table == null) {
			table = new JTable();
		}
		return table;
	}

	private JButton getBtnAtrs() {
		if (btnAtrs == null) {
			btnAtrs = new JButton("Atras");
			btnAtrs.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					vO.setVisible(true);
					dispose();
				}
			});
			btnAtrs.setMnemonic('A');
		}
		return btnAtrs;
	}
}
