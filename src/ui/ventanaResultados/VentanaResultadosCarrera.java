package ui.ventanaResultados;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;

import BBDD.Listados.ListaCarrera;
import BBDD.Listados.ListaCategorias;
import DTO.ControlDTO;
import logica.Operaciones;
import logica.ResultadoIndividual;
import ui.ventanaTablas.VentanaTablas;

public class VentanaResultadosCarrera extends VentanaTablas {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String nombreCarrera;
	private String sexo;
	private String categoria;
	private List<ResultadoIndividual> resultados;
	
	/**
	 * Create the frame.
	 */
	public VentanaResultadosCarrera() {
		super("Clasificaci�n de x carrera", new String[] {"Dorsal", "Nombre", "Apellidos" ,"Categoria", 
                "Sexo", "Posicion", "TiempoFinal", "TiempoMedio", "TiemposParciales"});
		addPanelFiltro(getContentPane());
	}

	@Override
	public void addFilasCarrera(DefaultTableModel modelo) {
		Object[] nuevaFila = new Object[9];
		this.resultados = new ArrayList<>();
		if (this.categoria != null && this.categoria.equals("Todas"))
			this.resultados = Operaciones.generarResultados(nombreCarrera, sexo);
		else
			this.resultados = Operaciones.generarResultados(this.nombreCarrera, this.sexo, this.categoria);
		
		for(ResultadoIndividual r : this.resultados)
		{
		    nuevaFila[0] = creaTextAreaConTexto(r.getDorsalString());
		    nuevaFila[1] = creaTextAreaConTexto(r.nombre);
            nuevaFila[2] = creaTextAreaConTexto(r.apellidos);
			nuevaFila[3] = creaTextAreaConTexto(r.getCategoria());
			nuevaFila[4] = creaTextAreaConTexto(r.sexo);
			nuevaFila[5] = creaTextAreaConTexto(r.getPosicionString());		
			nuevaFila[6] = creaTextAreaConTexto(r.getTiempoFinalString());
			nuevaFila[7] = creaTextAreaConTexto(r.getTiempoMedioString());
			nuevaFila[8] = creaBotonConFuncion("Ver Parciales");
			modelo.addRow(nuevaFila);
		}
		
	}

	@Override
	public void queHacer(int fila,int column) {
		VentanaResultadosTiemposParciales ventana = null;
		DialogoNoHayParciales dialogo = null;
		List<ControlDTO> parciales = null;
		if (column == 8) {
			parciales = Operaciones.obtenerParciales(this.nombreCarrera, resultados.get(fila).dni);
			if (parciales.size() > 0) {
			    String dorsal = ((JTextArea) getTableALlenar().getValueAt(fila, 0)).getText();
			    String nombre = ((JTextArea) getTableALlenar().getValueAt(fila, 1)).getText();
			    String apellidos = ((JTextArea) getTableALlenar().getValueAt(fila, 2)).getText(); 
				ventana = new VentanaResultadosTiemposParciales(parciales, dorsal, nombre, apellidos);
				ventana.setVisible(true);;
			}
			else {
				dialogo = new DialogoNoHayParciales();
				dialogo.setVisible(true);
			}
		}
	}
	
	public void addPanelFiltro(Container container){
		//Filtro para la carrera
		JLabel etiquetaCarrera = new JLabel("Carrera: ");
		etiquetaCarrera.setHorizontalAlignment(SwingConstants.RIGHT);
		JComboBox<String> comboBoxCarrera = new JComboBox<>();
		List<String> nombresCarreras = ListaCarrera.executeNombres();
		for (String n: nombresCarreras)
			comboBoxCarrera.addItem(n);
		
		
		//Filtro para la categoria
		JLabel etiquetaCategoria = new JLabel("Categoria: ");
		etiquetaCategoria.setHorizontalAlignment(SwingConstants.RIGHT);
		JComboBox<String> comboBoxCategoria = new JComboBox<>();
		List<String> nombresCategorias = ListaCategorias.executeNombres();
		comboBoxCategoria.addItem("Masc Todas");
		comboBoxCategoria.addItem("Fem Todas");
		for (String n: nombresCategorias) {
			comboBoxCategoria.addItem("Masc " + n);
			comboBoxCategoria.addItem("Fem " + n);
		}
		
		
		
		//Botón de busqueda
		JButton boton = new JButton("Buscar");
		boton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String[] categoria = ((String) comboBoxCategoria.getSelectedItem()).split(" ");
                setNombreCarrera((String) comboBoxCarrera.getSelectedItem());
                if (categoria[0].equals("Masc"))
                    setSexo("masculino");
                if (categoria[0].equals("Fem"))
                    setSexo("femenino");  
                setCategoria(categoria[1]);
                
				//Leo los tiempos parciales
				try {
				    Operaciones.leerFichero(nombreCarrera);
				} catch(IOException e) {
				    System.out.println("Fallo al cargar los controles");
				}
				
				//Borra las filas antiguas
				int filasABorrar = getModeloTableALlenar().getRowCount();
				for (int i=0; i < filasABorrar; i++)
					getModeloTableALlenar().removeRow(0);
				
				//Crea una nueva tabla para que que encaje con los parciales
				addFilasCarrera(getModeloTableALlenar());
				
				
			}
		});
		
		//Panel en el que se añade
		JPanel panelSuperior = new JPanel();
		panelSuperior.setLayout(new GridLayout());
		panelSuperior.add(etiquetaCarrera);
		panelSuperior.add(comboBoxCarrera);
		panelSuperior.add(etiquetaCategoria);
		panelSuperior.add(comboBoxCategoria);
		panelSuperior.add(boton);
		
				
		//Añadir panel al contentPane pasado como parametro
		container.add(panelSuperior, BorderLayout.NORTH);
	}	
	
//	private void addNuevaTabla(Container container) {
//	    //Obtengo los parciales a añadir en la nueva tabla
//	    for (int i=0; i < resultados.size(); i++)
//	        parciales = Operaciones.obtenerParciales(this.nombreCarrera, resultados.get(i).dni);
//	    
//	    //Creo el array con los nombres de la s
//	    String[] columnas = getStringColumnas();
//	    
//	    //Creo la tabla
//	    this.modeloTableALlenar = new DefaultTableModel(columnas,0);
//	    this.tableALlenar = new JTable(modeloTableALlenar);
//	    tableALlenar.setEnabled(false);
//	    tableALlenar.setDefaultRenderer(Object.class, new util.Render());
//	    TableColumnModel cmodel = tableALlenar.getColumnModel();
//	    cmodel.getColumn(0).setPreferredWidth(50);
//	    cmodel.getColumn(1).setPreferredWidth(30);
//	    tableALlenar.setBackground(Color.white);
//	    
//	    //Relleno la tabla
//	    addFilasCarrera(modeloTableALlenar);
//          
//
//        
//	    
//	}
//	
//	private String[] getStringColumnas(){
//	    String[] string = new String[] {"Categoria", "Sexo", "Posicion" ,"Dorsal", 
//                "Nombre", "Apellidos", "TiempoFinal", "TiempoMedio", "Tiempos Parciales"};
//        
//	    String[] defaultString = new String[9 + this.parciales.size()];
//	    defaultString[0] = "Categoria";
//	    defaultString[0] = "Categoria";
//	    defaultString[0] = "Categoria";
//	    defaultString[0] = "Categoria";
//	    defaultString[0] = "Categoria";
//	    defaultString[0] = "Categoria";
//	    defaultString[0] = "Categoria";
//	    
//        for (int i=9; i < defaultString.length; i++)
//            defaultString[i] = "Parcial " + (i+1);
//        
//        return stringParciales;
//    }

	
	private void setNombreCarrera(String nombreCarrera) {
		this.nombreCarrera = nombreCarrera;
	}
	
	private void setSexo(String sexo) {
		this.sexo = sexo;
	}
	
	private void setCategoria(String categoria) {
		this.categoria = categoria;
	}



}
