package ui.VentanasInscripcion;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import BBDD.InscribirAtletaSegunTipoInscripcion;
import BBDD.InscribirAtletas;
import DTO.AtletaDTO;
import DTO.CarreraDTO;
import logica.Atleta;
import logica.Categoria;
import logica.ManejadorInscripciones;
import ui.ventanaTablas.VentanaCarreras;
import ui.ventanasIntroductivas.Introductiva;
import ui.ventanaspago.VentanaMetodosPago;
import util.Fecha;

public class VentanaRegistro extends Introductiva {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JTextField textFieldNombre;

	private JTextField textFieldApellidos;

	private JTextField textFieldDNI;

	@SuppressWarnings("rawtypes")
	private JComboBox comboBoxDia;

	@SuppressWarnings("rawtypes")
	private JComboBox comboBoxMes;

	private JComboBox<String> comboBoxAno;

	private final ButtonGroup buttonGroup = new ButtonGroup();

	private CarreraDTO carrera;

	private AtletaDTO atleta;

	private List<Categoria> categorias;
	private JRadioButton rdbtnMasculino;
	private JRadioButton rdbtnFemenino;
	private String[] meses;
	private JButton btnYaEstoyRegistrado;


	/**
	 * 
	 * Create the frame.
	 * 
	 */

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public VentanaRegistro(VentanaCarreras ventana, CarreraDTO car) {
		meses =  new String[] {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Octubre","Noviembre","Diciembre"};
		
		categorias = new ArrayList<Categoria>();

		categorias.add(new Categoria("MASCULINA", 16, 100, "masculino"));

		categorias.add(new Categoria("FEMENINA", 16, 100, "femenino"));

		this.carrera = car;

		setBounds(100, 100, 900, 600);

		JButton btnSiguiente = new JButton("Siguiente");

		btnSiguiente.addActionListener(new ActionListener() {

			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {

				if(InscribirAtletaSegunTipoInscripcion.tienePreinscripcion(carrera.idCarrera))
				{
					tratarPreinscripcion();
				}
				
				else{
				@SuppressWarnings("unused")
				InscribirAtletas ins = new InscribirAtletas();
				if(textFieldNombre.getText().length() == 0 || textFieldApellidos.getText().length() == 0 || textFieldDNI.getText().length() != 9) {
					JOptionPane.showMessageDialog(null, "Debe rellenar todos los campos correctamente", "Campos invalidos", JOptionPane.ERROR_MESSAGE);
				}
				else {

					
				atleta = new AtletaDTO();

				atleta.nombre = textFieldNombre.getText();

				atleta.apellidos = textFieldApellidos.getText();

				atleta.DNI = textFieldDNI.getText();

				atleta.fecha_nacimiento = new Date(Integer.valueOf((String)comboBoxAno.getSelectedItem()) - 1900, comboBoxMes.getSelectedIndex() +1,
						comboBoxDia.getSelectedIndex() + 1);
				if (rdbtnFemenino.isSelected()) {
					atleta.sexo = "femenino";
				} else {
					atleta.sexo = "masculino";
				}
				InscribirAtletas ia = new InscribirAtletas();

				if(ia.execute(atleta, car)) {
					nuevaVentana();
				}
				else {
					JOptionPane.showMessageDialog(null, "Usuario ya inscrito en la carrera", "Usuario repetido",JOptionPane.INFORMATION_MESSAGE);
				}
			
				}
			}
			}

		});

		btnSiguiente.setBounds(777, 522, 117, 29);

		getContentPane().add(btnSiguiente);

		JButton btnAtras = new JButton("Atras");

		btnAtras.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				ventana.setVisible(true);

				dispose();

			}

		});

		btnAtras.setBounds(777, 491, 117, 29);

		getContentPane().add(btnAtras);

		JLabel lblNombre = new JLabel("Nombre:");

		lblNombre.setBounds(147, 110, 61, 16);

		getContentPane().add(lblNombre);

		JLabel lblApellidos = new JLabel("Apellidos:");

		lblApellidos.setBounds(147, 162, 84, 16);

		getContentPane().add(lblApellidos);

		JLabel lblDni = new JLabel("DNI:");

		lblDni.setBounds(147, 214, 61, 16);

		getContentPane().add(lblDni);

		JLabel lblFechaNacimiento = new JLabel("Fecha Nacimiento:");

		lblFechaNacimiento.setBounds(147, 268, 117, 16);

		getContentPane().add(lblFechaNacimiento);

		JLabel lblSexo = new JLabel("Sexo:");

		lblSexo.setBounds(147, 326, 61, 16);

		getContentPane().add(lblSexo);

		textFieldNombre = new JTextField();

		textFieldNombre.setBounds(273, 105, 486, 26);

		getContentPane().add(textFieldNombre);

		textFieldNombre.setColumns(10);

		textFieldApellidos = new JTextField();

		textFieldApellidos.setBounds(273, 157, 486, 26);

		getContentPane().add(textFieldApellidos);

		textFieldApellidos.setColumns(10);

		textFieldDNI = new JTextField();

		textFieldDNI.setBounds(273, 209, 486, 26);

		getContentPane().add(textFieldDNI);

		textFieldDNI.setColumns(10);

		comboBoxDia = new JComboBox();


		comboBoxDia.setBounds(276, 264, 75, 27);

		getContentPane().add(comboBoxDia);

		comboBoxMes = new JComboBox();


		comboBoxMes.setBounds(384, 264, 221, 27);

		getContentPane().add(comboBoxMes);

		comboBoxAno = new JComboBox();

		comboBoxAno.setBounds(639, 264, 120, 27);


		getContentPane().add(comboBoxAno);
		llenarFechero(comboBoxDia,comboBoxMes,comboBoxAno);
		
		rdbtnMasculino = new JRadioButton("Masculino");
		rdbtnMasculino.setSelected(true);

		buttonGroup.add(rdbtnMasculino);

		rdbtnMasculino.setBounds(285, 322, 141, 23);

		getContentPane().add(rdbtnMasculino);

		rdbtnFemenino = new JRadioButton("Femenino");

		buttonGroup.add(rdbtnFemenino);

		rdbtnFemenino.setBounds(464, 322, 141, 23);

		getContentPane().add(rdbtnFemenino);
		
		btnYaEstoyRegistrado = new JButton("Ya estoy registrado");
		btnYaEstoyRegistrado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostrarVentanaDNI();
			}
		});
		btnYaEstoyRegistrado.setBounds(0, 0, 148, 29);
		getContentPane().add(btnYaEstoyRegistrado);

		// Coloca la imagen v1 de fondo

		super.ponFotoDeFondo("/Imagenes/v3.jpg");

		setLocationRelativeTo(null);
		comboBoxAno.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				eventoFecha(comboBoxDia, comboBoxMes, comboBoxAno);	
				llenarMes(comboBoxDia, comboBoxMes, comboBoxAno);
			}
		});
		comboBoxMes.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				eventoFecha(comboBoxDia, comboBoxMes, comboBoxAno);	
				//llenarMes(comboBoxDia, comboBoxMes, comboBoxAno);
			}
		});

	}

	public CarreraDTO getCarrera() {

		return carrera;

	}

	public void setCarrera(CarreraDTO carrera) {

		this.carrera = carrera;

	}

	public AtletaDTO getAtleta() {

		return atleta;

	}

	public void setAtleta(AtletaDTO atleta) {

		this.atleta = atleta;

	}

	public void nuevaVentana() {

		VentanaMetodosPago pago = new VentanaMetodosPago(this);

		pago.setVisible(true);

		dispose();

	}

	private int getUltimoDiaMes(int mes, int ano) {
		Calendar calFin = Calendar.getInstance();		
		calFin.set(ano, mes, 1);
		return calFin.getActualMaximum(Calendar.DAY_OF_MONTH);
	}
	
	
	private void eventoFecha(JComboBox<String> dia, JComboBox<String> mes, JComboBox<String> ano) {
		
		Calendar miCalendario = Calendar.getInstance();
		
		int diaHoy = miCalendario.get(Calendar.DAY_OF_MONTH);
		
		int mess = miCalendario.get(Calendar.MONTH);
		
		int anoo = miCalendario.get(Calendar.YEAR);
		
		String[] aux = new String[10];
		
		if(mess == mes.getSelectedIndex() && Integer.valueOf((String) ano.getSelectedItem()) == anoo) {
			int ultimoDia = getUltimoDiaMes(mess, anoo);
			aux= new String[(ultimoDia - diaHoy) + 1];
			System.out.println(diaHoy + " " + mess + " " + anoo);
			for(int i= diaHoy; i<= ultimoDia; i++) {
				aux[i-diaHoy] = String.valueOf(i);
			}
			dia.setModel(new DefaultComboBoxModel<>(aux));
		}
		else {
			int ultimoDia = getUltimoDiaMes(mes.getSelectedIndex(), Integer.valueOf((String) ano.getSelectedItem()));
			aux= new String[ultimoDia];
			for(int i= 1; i<= ultimoDia; i++) {
				aux[i-1] = String.valueOf(i);
			}
			dia.setModel(new DefaultComboBoxModel<>(aux));
		}	
	}
	private void llenarMes(JComboBox<String> dia, JComboBox<String> mes, JComboBox<String> ano) {
		Calendar miCalendario = Calendar.getInstance();
		int mess =miCalendario.get(Calendar.MONTH);
		int anoo =miCalendario.get(Calendar.YEAR);
		String[] aux = new String[10];
		if(Integer.valueOf((String) ano.getSelectedItem()) != anoo) {
		aux = new String[11];
		for (int i = 0; i<11; i++) {
			aux[i] = meses[i];
		}
		mes.setModel(new DefaultComboBoxModel<>(aux));
		}
		else {
			aux = new String[12-(mess)];
			for (int i = 0; i<(12-mess); i++) {
				aux[i] = meses[(mess-1) + i];
			}
			mes.setModel(new DefaultComboBoxModel<>(aux));
		}
	}
	private void llenarFechero(JComboBox<String> dia, JComboBox<String> mes, JComboBox<String> ano) {
		Calendar miCalendario = Calendar.getInstance();
		int diaHoy = miCalendario.get(Calendar.DAY_OF_MONTH);
		int mess =miCalendario.get(Calendar.MONTH);
		int anoo =miCalendario.get(Calendar.YEAR);
		String[] aux = new String[anoo-16-1945];
		for(int i=1945 ; i< anoo -16; i++) {
			aux[i-1945] = String.valueOf(i);
		}
		ano.setModel(new DefaultComboBoxModel<>(aux));
		mes.setModel(new DefaultComboBoxModel<>(meses));
		int ultimoDia = getUltimoDiaMes(mess, anoo);
		aux= new String[(ultimoDia - diaHoy) + 1];
		System.out.println(diaHoy + " " + mess + " " + anoo);
		for(int i= diaHoy; i<= ultimoDia; i++) {
			aux[i-diaHoy] = String.valueOf(i);
		}
		dia.setModel(new DefaultComboBoxModel<>(aux));
	}
	public void mostrarVentanaDNI() {
		VentanaRegistroDni vdni = new VentanaRegistroDni(this);
		vdni.setVisible(true);
	}
	
	private void tratarPreinscripcion()
	{
		Atleta a = new Atleta();
		a.nombre = textFieldNombre.getText();
		a.apellidos = textFieldApellidos.getText();
		a.DNI = textFieldDNI.getText();
		a.fecha_nacimiento = comboBoxDia.getSelectedItem() + "-"+(comboBoxMes.getSelectedIndex()+1)+"-"+comboBoxAno.getSelectedItem();
		
		if(rdbtnFemenino.isSelected())
			a.sexo = "masculino";
		else
			a.sexo = "femenino";
		ArrayList<Atleta> al = new ArrayList<>();
		al.add(a);
		ManejadorInscripciones mi = new ManejadorInscripciones();
		if(mi.todoVaBien(al, carrera.idCarrera, "INDEPENDIENTE"))
		{
			AtletaDTO adto = new AtletaDTO();
			adto.nombre = a.nombre;
			adto.apellidos = a.apellidos;
			adto.sexo = a.sexo;
			adto.fecha_nacimiento = new Fecha(a.fecha_nacimiento).getFecha();
			adto.DNI = a.DNI;
			InscribirAtletaSegunTipoInscripcion.execute(adto, carrera.idCarrera, "INDEPENDIENTE");
			dispose();
		}
		else
		{
			JOptionPane.showMessageDialog(rootPane, 
					mi.todoVaBienString(al, carrera.idCarrera, "INDEPENDIENTE"));
		}
		
	}

}
