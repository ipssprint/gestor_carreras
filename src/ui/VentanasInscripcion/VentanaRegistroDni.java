package ui.VentanasInscripcion;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import BBDD.AtletaporDni;
import BBDD.InscribirAtletas;
import DTO.AtletaDTO;
import ui.ventanaspago.VentanaMetodosPago;

public class VentanaRegistroDni extends JFrame {

	private JPanel contentPane;
	private JButton Buscar;
	private JLabel lblIntroduceTuDni;
	private JTextField textField;
	private VentanaRegistro vr;

	/**
	 * Create the frame.
	 */
	public VentanaRegistroDni(VentanaRegistro vr) {
		this.vr = vr;
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		contentPane.add(getBuscar(), BorderLayout.SOUTH);
		contentPane.add(getLblIntroduceTuDni(), BorderLayout.NORTH);
		contentPane.add(getTextField(), BorderLayout.CENTER);
	}
	
	private JButton getBuscar() {
		if (Buscar == null) {
			Buscar = new JButton("Siguiente");
			Buscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					AtletaDTO atleta = AtletaporDni.execute(textField.getText());

					if(atleta == null) {
						JOptionPane.showMessageDialog(null, "No existe el DNI, debes registrarte primero", "Error", JOptionPane.ERROR_MESSAGE);
					}
					else {
						System.out.println(atleta.nombre);
						InscribirAtletas ia = new InscribirAtletas();
						if(ia.execute(atleta, vr.getCarrera())) {
							vr.setAtleta(atleta);
							nuevaVentana();
							dispose();
							vr.dispose();
							nuevaVentana();
						}
						else {
							JOptionPane.showMessageDialog(null, "Usuario ya inscrito en la carrera", "Usuario repetido",JOptionPane.INFORMATION_MESSAGE);
						}
						
						}
					}
					
				}
			);
	}
		return Buscar;
	}
	private JLabel getLblIntroduceTuDni() {
		if (lblIntroduceTuDni == null) {
			lblIntroduceTuDni = new JLabel("Introduce tu DNI:");
			lblIntroduceTuDni.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblIntroduceTuDni;
	}
	private JTextField getTextField() {
		if (textField == null) {
			textField = new JTextField();
			textField.setColumns(10);
		}
		return textField;
	}
	public void nuevaVentana() {

		VentanaMetodosPago pago = new VentanaMetodosPago(vr);

		pago.setVisible(true);

		dispose();

	}
}
