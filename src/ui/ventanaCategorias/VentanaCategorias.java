package ui.ventanaCategorias;

import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import BBDD.Categoria;
import DTO.CategoriaDTO;
import logica.AddCategoria;
import logica.NumericHelper;
import ui.VentanaCarrera.VentanaCrearCarrera;

public abstract class VentanaCategorias extends JDialog {

	/**
	 * @author Javier Ardura
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JLabel lblNombreCategoria;
	private JTextField textFieldNombreCategoria;
	private JLabel lblEdadMinima;
	private JTextField textFieldEdadMinima;
	private JLabel lblEdadMaxima;
	private JTextField textFieldEdadMaxima;
	private JButton btnGuardar;
	private JButton btnAtras;
	private JButton btnBorrarCategoria;
	private CategoriaDTO categoriadto;
	private CategoriaDTO categoriadtocopia;
	private Categoria cat;
	private VentanaCrearCarrera vc;


	/**
	 * Create the frame.
	 */
	public VentanaCategorias(String nombre, CategoriaDTO categoria, VentanaCrearCarrera vc) {
		this.categoriadto = categoria;
		this.vc = vc;
		if(categoria != null)
		{
			this.categoriadtocopia = new CategoriaDTO();
			this.categoriadtocopia.nombre_categoria = categoria.nombre_categoria;
		this.categoriadtocopia.edad_maxima = categoria.edad_maxima;
		this.categoriadtocopia.edad_minima = categoria.edad_minima;}
		cat = new Categoria();
		setTitle("EII Atletismo: "+ nombre);
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaCategorias.class.getResource("/Imagenes/Atlet.jpg")));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 685, 398);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getLblNombreCategoria());
		contentPane.add(getTextFieldNombreCategoria());
		contentPane.add(getLblEdadMinima());
		contentPane.add(getTextFieldEdadMinima());
		contentPane.add(getLblEdadMaxima());
		contentPane.add(getTextFieldEdadMaxima());
		contentPane.add(getBtnGuardar());
		contentPane.add(getBtnAtras());
		setLocationRelativeTo(null);
		setResizable(false);
		
	}
	private JLabel getLblNombreCategoria() {
		if (lblNombreCategoria == null) {
			lblNombreCategoria = new JLabel("Nombre de la categoria:");
			lblNombreCategoria.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblNombreCategoria.setBounds(37, 25, 184, 17);
			lblNombreCategoria.setDisplayedMnemonic('N');
			lblNombreCategoria.setLabelFor(getTextFieldNombreCategoria());
		}
		return lblNombreCategoria;
	}
	private JTextField getTextFieldNombreCategoria() {
		if (textFieldNombreCategoria == null) {
			textFieldNombreCategoria = new JTextField();
			textFieldNombreCategoria.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent arg0) {
					textFieldNombreCategoria.selectAll();
				}
			});
			textFieldNombreCategoria.setBounds(67, 53, 329, 20);
			textFieldNombreCategoria.setColumns(10);
		}
		return textFieldNombreCategoria;
	}
	private JLabel getLblEdadMinima() {
		if (lblEdadMinima == null) {
			lblEdadMinima = new JLabel("Edad minima de la categoria:");
			lblEdadMinima.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblEdadMinima.setBounds(37, 112, 184, 17);
			lblEdadMinima.setDisplayedMnemonic('E');
			lblEdadMinima.setLabelFor(getTextFieldEdadMinima());
		}
		return lblEdadMinima;
	}
	private JTextField getTextFieldEdadMinima() {
		if (textFieldEdadMinima == null) {
			textFieldEdadMinima = new JTextField();
			textFieldEdadMinima.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent e) {
					textFieldEdadMinima.selectAll();
				}
			});
			textFieldEdadMinima.setColumns(10);
			textFieldEdadMinima.setBounds(67, 154, 86, 20);
		}
		return textFieldEdadMinima;
	}
	private JLabel getLblEdadMaxima() {
		if (lblEdadMaxima == null) {
			lblEdadMaxima = new JLabel("Edad maxima de la categoria:");
			lblEdadMaxima.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblEdadMaxima.setDisplayedMnemonic('M');
			lblEdadMaxima.setBounds(37, 208, 184, 17);
			lblEdadMaxima.setLabelFor(getTextFieldEdadMaxima());
		}
		return lblEdadMaxima;
	}
	private JTextField getTextFieldEdadMaxima() {
		if (textFieldEdadMaxima == null) {
			textFieldEdadMaxima = new JTextField();
			textFieldEdadMaxima.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent e) {
					textFieldEdadMaxima.selectAll();
				}
			});
			textFieldEdadMaxima.setColumns(10);
			textFieldEdadMaxima.setBounds(67, 236, 86, 20);
		}
		return textFieldEdadMaxima;
	}
	private JButton getBtnGuardar() {
		if (btnGuardar == null) {
			btnGuardar = new JButton("Guardar");
			btnGuardar.setMnemonic('G');
			btnGuardar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String nombrecat = textFieldNombreCategoria.getText();
					String eminS = textFieldEdadMinima.getText();
					String emaxS = textFieldEdadMaxima.getText();
					int emin = -1;
					int emax = -1;
					if(nombrecat.isEmpty())
						textFieldNombreCategoria.setBorder(BorderFactory.createLineBorder(Color.red));
					if(!nombrecat.isEmpty())
						textFieldNombreCategoria.setBorder(BorderFactory.createLineBorder(Color.black));
					if(!NumericHelper.isNumeric(eminS)&& !eminS.isEmpty())
						textFieldEdadMinima.setBorder(BorderFactory.createLineBorder(Color.red));
					if(NumericHelper.isNumeric(eminS)|| eminS.isEmpty())
						textFieldEdadMinima.setBorder(BorderFactory.createLineBorder(Color.black));
					if( !NumericHelper.isNumeric(emaxS) && !emaxS.isEmpty())
						textFieldEdadMaxima.setBorder(BorderFactory.createLineBorder(Color.red));
					if(NumericHelper.isNumeric(emaxS)|| emaxS.isEmpty())
						textFieldEdadMaxima.setBorder(BorderFactory.createLineBorder(Color.black));
					
					if(NumericHelper.isNumeric(emaxS)) 
					{
						
						emax = Integer.parseInt(emaxS);
					}
					if( NumericHelper.isNumeric(eminS))
					{
						emin = Integer.parseInt(eminS);
					}
					if(emaxS.isEmpty()) 
					{
						
						emax = 999;
					}
					if( eminS.isEmpty())
					{
						emin = 18;
					}
					
					
					AddCategoria ac = new AddCategoria(nombrecat, emin, emax);
					if(ac.sePuedeCrear())
					{	
						categoriadto.nombre_categoria = textFieldNombreCategoria.getText();
						categoriadto.edad_minima = emin;
						categoriadto.edad_maxima = emax;
						int aux = JOptionPane.showConfirmDialog(rootPane, "¿Esta seguro que quiere guardar los cambios?");
					
						if(aux == JOptionPane.OK_OPTION)
						{ 
							validaciones(categoriadto, categoriadtocopia, cat);
						}
						else 
							return;
					}
					else
					{
						creaDialogoConTexto("Revise los campos, hay algun dato incorrecto");
					}
					
					
						
					
				}
			});
			btnGuardar.setBounds(393, 307, 89, 23);
		}
		return btnGuardar;
	}
	private JButton getBtnAtras() {
		if (btnAtras == null) {
			btnAtras = new JButton("Atras");
			btnAtras.setMnemonic('A');
			btnAtras.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					int aux = JOptionPane.showConfirmDialog(rootPane, "¿Quiere volver atrás sin guardar cambios?");
					if(aux == JOptionPane.OK_OPTION)
						dispose();
				}
			});
			btnAtras.setBounds(503, 307, 89, 23);
		}
		return btnAtras;
	}
	
	public void addBotonBorrar()
	{
		contentPane.add(getBtnBorrarCategoria());
	}
	private JButton getBtnBorrarCategoria() {
		if (btnBorrarCategoria == null) {
			btnBorrarCategoria = new JButton("Borrar categoria");
			btnBorrarCategoria.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					int a = JOptionPane.showConfirmDialog(rootPane, "¿Esta seguro que quiere eliminar la categoria?");
					if(a == JOptionPane.OK_OPTION)
					{	Categoria c = new Categoria();
					getVc().actualiza();
					getVc().llenarTable();
					dispose();
					if(!textFieldNombreCategoria.getText().isEmpty())
						c.borrar(textFieldNombreCategoria.getText());
						if(new Categoria().tieneAsociados(textFieldNombreCategoria.getText()))
								JOptionPane.showMessageDialog(rootPane, "No se puede eliminar una categoria con atletas");
					}
					
				}
			});
			btnBorrarCategoria.setMnemonic('B');
			btnBorrarCategoria.setToolTipText("");
			btnBorrarCategoria.setBounds(251, 307, 132, 23);
		}
		return btnBorrarCategoria;
	}
	
	public void preEscrito()
	{
		textFieldNombreCategoria.setText(this.categoriadto.nombre_categoria);
		textFieldNombreCategoria.setEnabled(false);
		textFieldEdadMinima.setText(this.categoriadto.edad_minima+"");
		textFieldEdadMaxima.setText(this.categoriadto.edad_maxima+"");
		if(categoriadto.edad_maxima==999)
			textFieldEdadMaxima.setText("");
		if(categoriadto.edad_minima==18)
			textFieldEdadMinima.setText("");
	}
	public abstract void validaciones(CategoriaDTO categoriadto, CategoriaDTO categoriadtocopia, Categoria cat);
	
	public void creaDialogoConTexto(String texto)
	{
		JOptionPane.showMessageDialog(rootPane, texto, "Error",JOptionPane.ERROR_MESSAGE);
	}
	
	public void inicializaElDto()
	{
		this.categoriadto = new CategoriaDTO();
	}
	
	public VentanaCrearCarrera getVc()
	{
		return this.vc;
	}
}
