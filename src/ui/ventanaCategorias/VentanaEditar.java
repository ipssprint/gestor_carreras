package ui.ventanaCategorias;

import javax.swing.JOptionPane;

import BBDD.Categoria;
import DTO.CategoriaDTO;
import ui.VentanaCarrera.VentanaCrearCarrera;

public class VentanaEditar extends VentanaCategorias {

	
	
	/**
	 * @author Javier Ardura
	 */
	private static final long serialVersionUID = 1L;

	public VentanaEditar(CategoriaDTO categoria, VentanaCrearCarrera vc) {
		super("editar categoria.", categoria, vc);
		super.addBotonBorrar();
		super.preEscrito();
	}

	@Override
	public void validaciones(CategoriaDTO categoriadto, CategoriaDTO categoriadtocopia, Categoria cat)
	{
		if(categoriadto.edad_minima <18 || categoriadto.edad_maxima < categoriadto.edad_minima || categoriadto.nombre_categoria.isEmpty())
		{	
			JOptionPane.showMessageDialog(rootPane, "Hay algun campo incorrecto","Error",JOptionPane.ERROR_MESSAGE);
			return;	
		}
		if(categoriadto.nombre_categoria.equals(categoriadtocopia.nombre_categoria))
		{
			if(categoriadto.edad_maxima == categoriadtocopia.edad_maxima && categoriadto.edad_minima == categoriadtocopia.edad_minima)
			{	super.getVc().llenarTable(); dispose();}
			else if(categoriadto.edad_maxima == categoriadtocopia.edad_maxima && categoriadto.edad_minima != categoriadtocopia.edad_minima)
			{
				if(cat.comprobarChoqueEdad(categoriadto.edad_minima, categoriadto.nombre_categoria))
				{	cat.modificar(categoriadto);
				super.getVc().actualiza();
				super.getVc().llenarTable();
					dispose();}
				else
					creaDialogoConTexto("No se puede hacer el cambio, esta edad esta en otra categoria");
			}	
			else if(categoriadto.edad_maxima != categoriadtocopia.edad_maxima && categoriadto.edad_minima == categoriadtocopia.edad_minima)
			{
				if(cat.comprobarChoqueEdad(categoriadto.edad_maxima, categoriadto.nombre_categoria))
				{	cat.modificar(categoriadto);
				

				super.getVc().actualiza();
				super.getVc().llenarTable();
					dispose();
				}
				else
					creaDialogoConTexto("No se puede hacer el cambio, esta edad esta en otra categoria");
			}
			else
			{
				if(cat.comprobarChoqueEdad(categoriadto.edad_maxima, categoriadto.nombre_categoria)&& cat.comprobarChoqueEdad(categoriadto.edad_minima, categoriadto.nombre_categoria))
				{	cat.modificar(categoriadto);
				super.getVc().actualiza();
				super.getVc().llenarTable();
				dispose();}
				else
				creaDialogoConTexto("No se puede hacer el cambio, esta edad, o edades esta en otra categoria");
			}
		}
		else{
		if(categoriadto.edad_maxima == categoriadtocopia.edad_maxima && categoriadto.edad_minima == categoriadtocopia.edad_minima)
		{
			if(!categoriadto.nombre_categoria.equals(categoriadtocopia.nombre_categoria))
			{
				if(cat.noExiste(categoriadto.nombre_categoria))
				{
					cat.insertar(categoriadto);
					//cat.acepta(categoriadto.nombre_categoria, categoriadtocopia.nombre_categoria);
					cat.editarParticipantes(categoriadto.nombre_categoria, categoriadtocopia.nombre_categoria);
					cat.borra_acepta(categoriadtocopia.nombre_categoria);
					super.getVc().actualiza();
					super.getVc().llenarTable();
					dispose();
				}
				else 
					creaDialogoConTexto("Este nombre de categoria ya existe");
			}
		}
		if(categoriadto.edad_maxima == categoriadtocopia.edad_maxima && categoriadto.edad_minima != categoriadtocopia.edad_minima )
		{
			if(cat.noExiste(categoriadto.nombre_categoria)&& cat.comprobarChoqueEdad(categoriadto.edad_minima, categoriadto.nombre_categoria))
			{
				cat.insertar(categoriadto);
				cat.acepta(categoriadto.nombre_categoria, categoriadtocopia.nombre_categoria);
				cat.editarParticipantes(categoriadto.nombre_categoria, categoriadtocopia.nombre_categoria);
				cat.borra_acepta(categoriadtocopia.nombre_categoria);
				super.getVc().actualiza();
				super.getVc().llenarTable();
				dispose();
			}
			
			else
				creaDialogoConTexto("No se puede hacer el cambio, esta edad, o nombre estan en otra categoria");
		}
		if(categoriadto.edad_maxima != categoriadtocopia.edad_maxima && categoriadto.edad_minima == categoriadtocopia.edad_minima )
		{
			if(cat.noExiste(categoriadto.nombre_categoria)&& cat.comprobarChoqueEdad(categoriadto.edad_maxima, categoriadto.nombre_categoria))
			{
				cat.insertar(categoriadto);
				cat.acepta(categoriadto.nombre_categoria, categoriadtocopia.nombre_categoria);
				cat.editarParticipantes(categoriadto.nombre_categoria, categoriadtocopia.nombre_categoria);
				cat.borra_acepta(categoriadtocopia.nombre_categoria);
				super.getVc().actualiza();
				super.getVc().llenarTable();
				dispose();
			}
			
			else
				creaDialogoConTexto("No se puede hacer el cambio, esta edad, o nombre estan en otra categoria");
		}
		}
	}

}
