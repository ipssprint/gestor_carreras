package ui.ventanaCategorias;

import javax.swing.JOptionPane;

import BBDD.Categoria;
import DTO.CategoriaDTO;
import ui.VentanaCarrera.VentanaCrearCarrera;

public class VentanaAddCategoria extends VentanaCategorias {

	
	public VentanaAddCategoria(VentanaCrearCarrera vc) {
		super("añadir nueva categoria", null,vc);
		super.inicializaElDto();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void validaciones(CategoriaDTO categoriadto, CategoriaDTO categoriadtocopia, Categoria cat) {

		if(categoriadto.edad_minima <18 || categoriadto.edad_maxima < categoriadto.edad_minima || categoriadto.nombre_categoria.isEmpty())
		{	
			JOptionPane.showMessageDialog(rootPane, "Hay algun campo incorrecto","Error",JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		if(cat.noExiste(categoriadto.nombre_categoria) && cat.comprobarChoqueEdad(categoriadto.edad_minima)&& cat.comprobarChoqueEdad(categoriadto.edad_maxima))
		{	cat.insertar(categoriadto);
		super.getVc().actualiza();
		super.getVc().llenarTable();
			dispose();}
			
		else
			creaDialogoConTexto("No se puede añadir, ya existe este nombre o las edades en otra categoria");
	}
	
	

}
