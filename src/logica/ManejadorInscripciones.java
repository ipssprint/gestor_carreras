package logica;

import java.util.ArrayList;

import BBDD.BuscaAtletaEnCarrera;
import BBDD.Categoria;
import BBDD.Listados.ListaAtletas;
import DTO.AtletaDTO;
import DTO.ParticipaDTO;
import util.Fecha;

public class ManejadorInscripciones {
	public static boolean sinProblemas(Atleta a, String idCarrera, String club)
	{	
		if(a.DNI.isEmpty() || a.nombre.isEmpty() || a.apellidos.isEmpty() 
				|| a.fecha_nacimiento.isEmpty() || a.sexo.isEmpty() || club.isEmpty())
			return false;
		else if(a.DNI.toCharArray().length > 9)
			return false;
		else if(a.nombre.toCharArray().length > 255 || a.apellidos.toCharArray().length > 255)
			return false;
		else if(!a.sexo.equals("masculino") && !a.sexo.equals("femenino"))
			return false;
		else if(!validoLaFecha(a.fecha_nacimiento))
		{	return false;
		
		}
		else if(esMenorDeEdad(a.fecha_nacimiento))
			return false;
		else if(!tieneCategoria(a.fecha_nacimiento, idCarrera))
			return false;
		else if(yaEstaEnLaCarrera(idCarrera, a, club))
			return false;
		return true;
	}
	
	private static boolean tieneCategoria(String fecha_nacimiento, String idC) {
		Fecha f = new Fecha(fecha_nacimiento);
		Categoria c = new Categoria();
		return c.getCategoriaEdadAceptada(f.getEdadDeEstaFecha(), idC);
		
	}
	
	private static boolean yaEstaEnLaCarrera(String idCarrera, Atleta a, String club)
	{
		ParticipaDTO p = BuscaAtletaEnCarrera.execute(idCarrera, a.DNI, club);
		ParticipaDTO pd = BuscaAtletaEnCarrera.execute(idCarrera, a.DNI);

		
		if(pd != null && pd.club.equals("INDEPENDIENTE") && !club.equals("INDEPENDIENTE")&& pd.importe_pagado == 0)
			return false;
		else if(p!= null || (pd != null && pd.importe_pagado > 0))
			return true;
		return false;
	}

	private static boolean esMenorDeEdad(String fecha_nacimiento) {
		Fecha f = new Fecha();
		Fecha hoy = new Fecha(f.getDia(), f.getMes(), f.getYear());
		Fecha hme = new Fecha(f.getDia(), f.getMes(), f.getYear()-18);
		long dieciOchoAns = hoy.getFecha().getTime() - hme.getFecha().getTime();
		long edadActual = hoy.getFecha().getTime() - new Fecha(fecha_nacimiento).getFecha().getTime();
		if(edadActual < dieciOchoAns)
			return true;
		else 
			return false;
	}


	public static String problemas(Atleta a, String idCarrera, String club)
	{
		if(a.DNI.isEmpty() || a.fecha_nacimiento.isEmpty() 
				|| a.sexo.isEmpty() || club.isEmpty())
			return "Uno de los campos esta vacio";
		else if( a.nombre.isEmpty() || a.apellidos.isEmpty())
			return "Nombre y/o apellidos incorrectos";
		else if(a.DNI.toCharArray().length > 9)
			return "DNI invalido "+ a.DNI;
		else if(a.nombre.toCharArray().length > 255 || a.apellidos.toCharArray().length > 255)
			return "Nombre y/o apellidos muy largos para el atleta " + a.DNI;
		else if(!a.sexo.equals("masculino") && !a.sexo.equals("femenino"))
			return "El sexo debe ser masculino o femenino";
		else if(!validoLaFecha(a.fecha_nacimiento))
			return "Formato de fecha incorrecto o fecha incorrecta para " + a.nombre + " "+a.apellidos;
		else if(esMenorDeEdad(a.fecha_nacimiento))
			return "Hay un atleta menor de edad "+ a.nombre + " "+a.apellidos;
		else if(!tieneCategoria(a.fecha_nacimiento, idCarrera))
			return "No hay categoria en esta carrera para "+ a.nombre + " "+a.apellidos;
		else if(yaEstaEnLaCarrera(idCarrera, a, club))
			return "Ya se ha inscrito en esta carrera "+ a.nombre + " "+a.apellidos;
		
		return "";
	}

	private static boolean validoLaFecha(String fecha) {
		String aux[] = fecha.split("-");
		if(aux.length !=3)
			return false;
		else{
			if(!NumericHelper.isNumeric(aux[0]) || !NumericHelper.isNumeric(aux[1]) || !NumericHelper.isNumeric(aux[2]))
				return false;
			else
			{
				if(Integer.parseInt(aux[2])<1900 || Integer.parseInt(aux[2])>2018)
					return false;
				if(Integer.parseInt(aux[1])<1 ||Integer.parseInt(aux[1])>12 )
					return false;
				if(Integer.parseInt(aux[0])<1 ||
						new Fecha().ultimoDiaMes(Integer.parseInt(aux[1]), Integer.parseInt(aux[2])) < Integer.parseInt(aux[0]))
					return false;
				
			}
		}
		return true;
	}
	
	public boolean sePisan(ArrayList<Atleta> a)
	{
		for(int i = 0; i < a.size()-1; i++)
		{
			for(int j = i+1; j < a.size(); j++)
			{
				if(a.get(i).DNI.equals(a.get(j).DNI))
					return true;
			}
		}
		return false;
	}
	
	public boolean todoVaBien(ArrayList<Atleta> a, String idCarrera, String club)
	{
		for(Atleta at : a)
		{
			if(!consistente(at) || !sinProblemas(at, idCarrera, club) )
				return false;
		}
		return true;
	}
	
	private boolean consistente(Atleta at) {
		AtletaDTO aux = ListaAtletas.getAtleta(at.DNI);
		if(aux == null || (aux.DNI.equals(at.DNI) && aux.nombre.equals(at.nombre)
				&& aux.apellidos.equals(at.apellidos) && aux.sexo.equals(at.sexo)
			&& aux.fecha_nacimiento.getTime() == 
			new Fecha(aux.fecha_nacimiento).getFecha().getTime()))
			return true;
		return false;
	}

	public String todoVaBienString(ArrayList<Atleta> a, String idCarrera, String club)
	{
		for(Atleta at : a)
		{
			if(!consistente(at))
				return "Ya hay un atleta con este DNI pero otros datos";
			if(!sinProblemas(at, idCarrera, club))
				return problemas(at, idCarrera, club);
			
		}
		return "";
	}

}
