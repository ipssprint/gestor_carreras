package logica;

import DTO.PlazoDTO;
import util.Fecha;

public class Plazo {

	String idCarrera;
	String fechaInicio;
	String fechaFin;
	double precio_inscripcion;
	public Plazo(String idCarrera, String fechaInicio, String fechaFin, double precio_inscripcion) {
	
		this.idCarrera = idCarrera;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.precio_inscripcion = precio_inscripcion;
	}
	
	
	
	
	public PlazoDTO dameElDto()
	{	Fecha fecha1 = new Fecha(fechaInicio);
		Fecha fecha2 = new Fecha(fechaFin);
		if(ManejadorPlazo.sinProblemas(this))
		{
			PlazoDTO tr = new PlazoDTO();
			tr.idCarrera = this.idCarrera;
			tr.fecha_inicio_plazo = fecha1.getFecha();
			tr.fecha_fin_plazo = fecha2.getFecha();
			tr.cuota_inscripcion = this.precio_inscripcion;
			return tr;
		}
		return null;
	}
}
