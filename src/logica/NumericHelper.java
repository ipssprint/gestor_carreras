package logica;
/**
 * @author Javier Ardura 
 **/
public class NumericHelper {

	/** Dice si una cadena es un numero
	 * @param s
	 * @return
	 */
	public static boolean isNumeric(String s)
	{
		try{
			Integer.parseInt(s);
			return true;
		}
		catch (NumberFormatException e) {
			return false;
		}
	}
	
	public static boolean isNumericDouble(String s)
	{
		try{
			Double.parseDouble(s);
			return true;
		}
		catch (NumberFormatException e) {
			return false;
		}
	}
}
