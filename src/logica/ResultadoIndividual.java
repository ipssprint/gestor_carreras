package logica;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

public class ResultadoIndividual {

	public String dni;
	public String categoria;
	public String sexo;
	public int posicion;
	public int dorsal;
	public String nombre;
	public String apellidos;
	public Time tiempoFinal;
	public Time tiempoMedio;
	public List<Time> tiemposParciales = new ArrayList<>();
	
	public ResultadoIndividual(String dni, String categoria, String sexo, int dorsal, String nombre, 
			String apellidos, Time tiempoFinal) {
		this.dni = dni;
		this.categoria = categoria;
		this.sexo = sexo;
		this.dorsal = dorsal;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.tiempoFinal = tiempoFinal;
	}
	
	public String getPosicionString(){
		String s = "" + this.posicion;
		return s;
	}



	@SuppressWarnings("deprecation")
	public String getTiempoFinalString(){
	    if (tiemposParciales.size() == 0)
	        return "DNS";
		if (tiemposParciales.size() > 0 && tiempoFinal == null)
			return "DNF";
		String s = "" + tiempoFinal.getHours() + "." + tiempoFinal.getMinutes() + "." + tiempoFinal.getSeconds();
		return s;
	}

	public String getDorsalString() {
		String s = "" + this.dorsal;
		return s;
	}
	
	public String getCategoria(){
		if (categoria == null)
			return "Absoluto (SC)";
		return this.categoria;
	}

    @SuppressWarnings("deprecation")
    public String getTiempoMedioString() {
        if (tiempoMedio == null)
            return "---";
        String s = "" + tiempoMedio.getHours() + "." + tiempoMedio.getMinutes() + "." + tiempoMedio.getSeconds();
        return s; 
    }
	
}
