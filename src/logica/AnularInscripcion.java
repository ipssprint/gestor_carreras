package logica;

import java.util.List;

import BBDD.ActualizarRegistros;
import BBDD.BuscaAtletaEnCarrera;
import BBDD.CambiarEstadoAtleta;
import BBDD.EstadosInscripcion;
import DTO.ParticipaDTO;
import DTO.PlazoCancelacionDTO;

public class AnularInscripcion {

	public static double execute(String dni, String idCarrera) {
		// Existe la inscripcion del atleta
		double result = -1;
		ParticipaDTO p = BuscaAtletaEnCarrera.execute(idCarrera, dni);
		if (p != null) {
			if (p.tipo_inscripcion.equals(EstadosInscripcion.INSCRITO.toString())) {
				List<PlazoCancelacionDTO> plazosDev = ActualizarRegistros.getPlazoDev(idCarrera);
				for (PlazoCancelacionDTO plazo : plazosDev)
					if (plazo.fecha_inicio_plazo.before(p.fecha_inscripcion)
							&& plazo.fecha_fin_plazo.after(p.fecha_inscripcion))
						result = p.importe_pagado*plazo.porcentaje_devolucion;
			} else {
				result = 0;
			}
			CambiarEstadoAtleta.execute(EstadosInscripcion.ANULADO, dni, idCarrera);
			return result;
		}
		return result;
	}

}
