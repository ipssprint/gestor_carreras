package logica;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Time;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import BBDD.CambiarDorsal;
import BBDD.CambiarEstadoAtleta;
import BBDD.EstadosInscripcion;
import BBDD.InscribirAtletas;
import BBDD.InscripcionesDeUnaCarrera;
import BBDD.InsertarControl;
import BBDD.Listados.ListaAtletas;
import BBDD.Listados.ListaCarrera;
import BBDD.Listados.ListaControles;
import BBDD.Listados.ListaParticipa;
import BBDD.Listados.ListaResultados;
import DTO.AtletaDTO;
import DTO.CarreraDTO;
import DTO.ControlDTO;
import DTO.ParticipaDTO;
import DTO.ResultadoDeAtletaEnCarrerasDTO;

public class Operaciones {

	/**
	 * Metodo que genera dorsales en la tabla Participa a partir del dorsal 20.
	 * 
	 * @param participantes
	 * @param todos         los participantes
	 */
	public void generarDorsales() {
		List<ParticipaDTO> participantes = ListaParticipa.execute();

		for (int i = 0; i < participantes.size(); i++) {
			CambiarDorsal.execute(i + 20, participantes.get(i).DNI, participantes.get(i).idCarrera);
			;
		}
	}

	public static List<ControlDTO> obtenerParciales(String nombreCarrera, String dni) {
		return ListaControles.execute(nombreCarrera, dni);
	}

	/**
	 * M�todo que genera los resultados ordenados por posicion y filtrados por sexo
	 * para usarla en la VentanaResultados.
	 * 
	 * LA LISTA DE RESULTADOS TIENE EL DNI AUNQUE LUEGO NO LO USE EN LA TABLA DE
	 * RESULTADOS
	 * 
	 * @param sexo
	 */
	@SuppressWarnings("deprecation")
	public static List<ResultadoIndividual> generarResultados(String nombreCarrera, String sexo, String categoria) {

		// Devuelve los resutlados de una carrera
		List<ResultadoIndividual> resultados = ListaResultados.execute(nombreCarrera, sexo, categoria);

		// Calcula el tiempo medio
		CarreraDTO carrera = ListaCarrera.findCarreraByName(nombreCarrera);
		for (ResultadoIndividual r : resultados)
			r.tiempoMedio = new Time((r.tiempoFinal.getSeconds() * 1000) / carrera.distancia);

		// Ordena los resultados segun el tiempo
		resultados.sort((r1, r2) -> {
			Time t1 = ((ResultadoIndividual) r1).tiempoFinal;
			Time t2 = ((ResultadoIndividual) r2).tiempoFinal;
			;
			if (t1 == null && t2 != null)
				return 1;
			else if (t2 == null && t1 != null)
				return -1;
			else if (t1 == null && t2 == null)
				return 0;
			else
				return t1.compareTo(t2);
		});

		// Asigna las posiciones
		for (int i = 0; i < resultados.size(); i++)
			resultados.get(i).posicion = i + 1;

		// Añade los tiempos parciales
		for (ResultadoIndividual r : resultados) {
			List<ControlDTO> controles = ListaControles.execute(nombreCarrera, r.dni);
			for (ControlDTO c : controles) {
				r.tiemposParciales.add(c.tiempo);
			}
		}

		return resultados;
	}

	public static List<ResultadoIndividual> generarResultados(String nombreCarrera, String sexo) {
		// Devuelve los resutlados de una carrera
		List<ResultadoIndividual> resultados = ListaResultados.execute(nombreCarrera, sexo);
		;

		// Ordena los resultados segun el tiempo
		resultados.sort((r1, r2) -> {
			Time t1 = ((ResultadoIndividual) r1).tiempoFinal;
			Time t2 = ((ResultadoIndividual) r2).tiempoFinal;
			;
			if (t1 == null && t2 != null)
				return 1;
			else if (t2 == null && t1 != null)
				return -1;
			else if (t1 == null && t2 == null)
				return 0;
			else
				return t1.compareTo(t2);
		});

		// Asigna las posiciones
		for (int i = 0; i < resultados.size(); i++)
			resultados.get(i).posicion = i + 1;

		// Añade los tiempos parciales
		for (ResultadoIndividual r : resultados) {
			List<ControlDTO> controles = ListaControles.execute(nombreCarrera, r.dni);
			for (ControlDTO c : controles) {
				r.tiemposParciales.add(c.tiempo);
			}
		}

		return resultados;
	}

	public static void leerFichero(String nombreCarrera) throws IOException {
		// Obtengo el idCarrera
		CarreraDTO carrera = ListaCarrera.findCarreraByName(nombreCarrera);

		String ruta = "resultados/" + nombreCarrera + ".txt";
		String cadena;
		int dorsal = 0;

		FileReader fr = new FileReader(ruta);
		BufferedReader b = new BufferedReader(fr);
		
		int numeroControles = Integer.parseInt(b.readLine());
		while (b.ready()) {
			cadena = b.readLine();
			String[] linea = cadena.split(" ");
			
			String[] primeraLinea= linea[1].split("-");
			int contadorControles = Integer.parseInt(primeraLinea[0]);

			dorsal = Integer.parseInt(linea[0]);
			ParticipaDTO participante = ListaParticipa.findAtletaByDorsalAndCarrera(dorsal, carrera.idCarrera);

			if (participante != null && !comprobarParticipante(participante.DNI, carrera.idCarrera)) {
				
			    for (int i = 1; i < linea.length; i++) {
			        String[] linea2 = linea[i].split("-");
					
			        if (Integer.parseInt(linea2[0]) >= contadorControles) {
					    
					    // Creo el ControlDTO
                        ControlDTO control = new ControlDTO();
                        control.DNI = participante.DNI;
                        control.idCarrera = carrera.idCarrera;
					    
                        if (contadorControles != Integer.parseInt(linea2[0])){
					        control.idPuntoControl = contadorControles;
					        control.tiempo = null;
					        i--;
					    }
                        else {
                            control.idPuntoControl = Integer.parseInt(linea2[0]);
                            control.tiempo = Time.valueOf(linea2[1]);
                        }
                        
                        InsertarControl.execute(control);
    					if (contadorControles == numeroControles && control.tiempo != null)
    						InscribirAtletas.modificarTiempoFinal(control.tiempo, participante.DNI, carrera.idCarrera);
    					
    					contadorControles++;
    					} 
					}
			    
    			    for (int i = contadorControles; i <= numeroControles; i++) {
    			        // Creo el ControlDTO
                        ControlDTO control = new ControlDTO();
                        control.DNI = participante.DNI;
                        control.idCarrera = carrera.idCarrera;
                        control.idPuntoControl = i;
                        control.tiempo = null;
                        InsertarControl.execute(control);
    			    }
				}
			}
		fr.close();
        b.close();
		}
	

	private static boolean comprobarParticipante(String dni, String idCarrera) {
		if (ListaControles.existeDniEnCarrera(dni, idCarrera))
			return true;
		return false;
	}

	private static boolean comprobarExisteControl(String dni, String idCarrera, long idPuntoControl) {
		if (ListaControles.existePuntoControl(dni, idCarrera, idPuntoControl))
			return true;
		return false;
	}

	/**
	 * Encuentra las inscripciones que tiene un atleta a partir de su dni
	 * 
	 * @param dni
	 * @param participaciones
	 * @return
	 */
	private static List<ParticipaDTO> encuentraInscripcionesdeAtleta(String dni, List<ParticipaDTO> participaciones) {

		for (int i = 0; i < participaciones.size(); i++) {
			if (!participaciones.get(i).DNI.toLowerCase().equals(dni.toLowerCase())) {
				participaciones.remove(i);
				i--;
			}
		}
		if (participaciones.size() == 0)
			throw new RuntimeException("ATLETA NO INSCRITO");
		return participaciones;
	}

	/**
	 * Mete en participaDTO Las posiciones de los tiempos
	 * 
	 * @param inscripciones
	 * @param participaciones
	 */
	private static List<ParticipaDTO> calcularPosciones(List<ParticipaDTO> participaciones) {
		List<ParticipaDTO> resultados = null;
		for (ParticipaDTO r : participaciones) {
			resultados = InscripcionesDeUnaCarrera.execute(r.idCarrera, r.nombre_categoria);
			if (resultados != null)
				for (int i = 0; i < resultados.size(); i++) {
					resultados.get(i).posicion = i + 1;
				}
			for (ParticipaDTO s : resultados) {
				if (r.DNI.equals(s.DNI) & r.idCarrera.equals(s.idCarrera)) {
					r.posicion = s.posicion;
				}

			}
		}
		return participaciones;
	}

	/**
	 * Encuentra el atleta que queremos a partir de su dni
	 * 
	 * @param dni
	 * @param atletas
	 * @return
	 */
	private static AtletaDTO findAtletaEnLista(String dni, List<AtletaDTO> atletas) {
		for (AtletaDTO atleta : atletas)
			if (atleta.DNI.toLowerCase().equals(dni.toLowerCase()))
				return atleta;
		return null;

	}

	/**
	 * Devuelve las inscripciones de un atleta y las carreras que participa
	 * 
	 * @param dni
	 * @return
	 */
	public static ResultadoDeAtletaEnCarrerasDTO calcularResultadosDeAtleta(String dni) {

		ResultadoDeAtletaEnCarrerasDTO r = new ResultadoDeAtletaEnCarrerasDTO();
		List<AtletaDTO> atletas = ListaAtletas.execute();
		r.atleta = findAtletaEnLista(dni, atletas);
		if (r.atleta == null)
			throw new RuntimeException("ATLETA NO ENCONTRADO");

		r.participaciones = ListaParticipa.execute();

		r.participaciones = calcularPosciones(r.participaciones);
		r.participaciones = encuentraInscripcionesdeAtleta(dni, r.participaciones);
		r.inscripciones = encuentraCarreras(r.participaciones);

		r.controles = encuentraControles(r);

		r.participaciones = ListaParticipa.execute();

		r.participaciones = calcularPosciones(r.participaciones);
		r.participaciones = encuentraInscripcionesdeAtleta(dni, r.participaciones);
		r.inscripciones = encuentraCarreras(r.participaciones);
		return r;
	}

	private static Map<String, List<ControlDTO>> encuentraControles(ResultadoDeAtletaEnCarrerasDTO r) {
		Map<String, List<ControlDTO>> con = new HashMap<String, List<ControlDTO>>();
		for (int j = 0; j < r.participaciones.size(); j++) {
			CarreraDTO carreraDTO = r.inscripciones.get(r.participaciones.get(j).idCarrera);
			String nombreCarrera = r.inscripciones.get(carreraDTO.idCarrera).nombre;
			try {
				leerFichero(nombreCarrera);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		for (int j = 0; j < r.participaciones.size(); j++) {
			List<ControlDTO> contr = ListaControles
					.execute(r.inscripciones.get(r.participaciones.get(j).idCarrera).nombre, r.atleta.DNI);
			if (contr.size() > r.maximosControles)
				r.maximosControles = contr.size();
			con.put(r.inscripciones.get(r.participaciones.get(j).idCarrera).idCarrera, contr);
		}

		return con;
	}

	/**
	 * Devuelve los dtos con las carreras que se encuentran en la inscripcion
	 * 
	 * @param inscripciones
	 * @return
	 */
	private static Map<String, CarreraDTO> encuentraCarreras(List<ParticipaDTO> inscripciones) {
		List<CarreraDTO> carreras = ListaCarrera.execute();
		Map<String, CarreraDTO> carrerasInscritas = new HashMap<String, CarreraDTO>();
		for (CarreraDTO carrera : carreras)
			for (ParticipaDTO participa : inscripciones)
				if (participa.idCarrera.equals(carrera.idCarrera)) {
					carrerasInscritas.put(participa.idCarrera, carrera);
				}
		if (carrerasInscritas.size() == 0)
			throw new RuntimeException("NO EXISTEN LAS CARRERAS INSCRITAS");
		return carrerasInscritas;

	}

	/**
	 * Se comunica con la base de datos y marca como pendiente de pago al atleta en
	 * participa
	 * 
	 * @param atleta
	 * @param carrera
	 */
	public static void preInscribirAtleta(AtletaDTO atleta, CarreraDTO carrera) {
		CambiarEstadoAtleta.execute(EstadosInscripcion.PENDIENTE, atleta.DNI, carrera.idCarrera);

	}

	/**
	 * Marca al atleta como inscrito en la base de datos
	 * 
	 * @param atleta
	 * @param carrera
	 */
	public static void inscribirAtleta(AtletaDTO atleta, CarreraDTO carrera) {
		CambiarEstadoAtleta.execute(EstadosInscripcion.INSCRITO, atleta.DNI, carrera.idCarrera);

	}

}