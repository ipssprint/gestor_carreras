package logica;

import java.util.Date;

import BBDD.Plazos;
import BBDD.Listados.ListaCarrera;
import DTO.CarreraDTO;
import DTO.PlazoDTO;
import util.Fecha;

public class ManejadorPlazoCancelacion {
	public static boolean sinProblemas(PlazoCancelacion p)
	{
		if(p.idCarrera == null)
			return false;
		else if(!validoLaFecha(p.fechaInicio))
			return false;
		else if(!validoLaFecha(p.fechaFin))
			return false;
		else if(p.porcentajeCanc <0 || p.porcentajeCanc > 1)
			return false;
		else if(validoLaFecha(p.fechaFin)&&validoLaFecha(p.fechaInicio))
		{
			Fecha fecha1 = new Fecha(p.fechaInicio);
			Fecha fecha2 = new Fecha(p.fechaFin);
			if(fecha1.getFecha().getTime()>fecha2.getFecha().getTime())
				return false;
			if(validaActual(fecha1) || validaActual(fecha2))
				return false;
			if(esMayorQueLaCarrera(p))
				return false;
			if(esMenorQueLasInscrip(p))
				return false;
		}
		
		
		return true;
	}
	
	private static boolean validaActual(Fecha fechaFin) {
		Fecha a = new Fecha(1, 1, 2018);
		Fecha b = new Fecha(2, 1, 2018);	
		Long unDia = (b.getFecha().getTime() - a.getFecha().getTime());
		Fecha hoy = new Fecha(new Date(new Date().getTime()-unDia));
		if(hoy.getFecha().getTime() > fechaFin.getFecha().getTime())
			return true;
		return false;
	}

	public static String problemas(PlazoCancelacion p)
	{
		if(p.idCarrera == null)
			return "IDCARRERA invalido";
		else if(!validoLaFecha(p.fechaInicio))
			return "Fecha inicio no valida";
		else if(!validoLaFecha(p.fechaFin))
			return "Fecha fin no valida";
		else if(p.porcentajeCanc <0 || p.porcentajeCanc > 1)
			return "Porcentaje no valido";
		else if(validoLaFecha(p.fechaFin)&&validoLaFecha(p.fechaInicio))
		{
			Fecha fecha1 = new Fecha(p.fechaInicio);
			Fecha fecha2 = new Fecha(p.fechaFin);
			if(fecha1.getFecha().getTime()>fecha2.getFecha().getTime())
				return "La fecha de fin es mayor que la de inicio";
			if(validaActual(fecha1) || validaActual(fecha2))
				return "Una de las fechas o ambas han pasado";
			if(esMayorQueLaCarrera(p))
				return "En este plazo ya se habria corrido la carrera";
			if(esMenorQueLasInscrip(p))
				return "En este plazo no habria inscripciones";
		}
		
		
		return "";
	}

	private static boolean validoLaFecha(String fecha) {
		String aux[] = fecha.split("-");
		if(aux.length !=3)
			return false;
		else{
			if(!NumericHelper.isNumeric(aux[0]) || !NumericHelper.isNumeric(aux[1]) || !NumericHelper.isNumeric(aux[2]))
				return false;
			else
			{
				if(Integer.parseInt(aux[2])<0 )
					return false;
				if(Integer.parseInt(aux[1])<1 ||Integer.parseInt(aux[1])>12 )
					return false;
				if(Integer.parseInt(aux[0])<1 ||
						new Fecha().ultimoDiaMes(Integer.parseInt(aux[1]), Integer.parseInt(aux[2])) < Integer.parseInt(aux[0]))
					return false;
				
			}
		}
		return true;
	}
	
	private static boolean esMayorQueLaCarrera(PlazoCancelacion pd)
	{
		CarreraDTO c = ListaCarrera.findCarreraById(pd.idCarrera);
		if(new Fecha(pd.fechaFin).getFecha().getTime()>c.Fecha_competicion.getTime())
			return true;
		return false;
	}
	
	private static boolean esMenorQueLasInscrip(PlazoCancelacion pd)
	{
		PlazoDTO p = new Plazos().plazosCarrera(pd.idCarrera).get(0);
		if(p.fecha_inicio_plazo.getTime() > new Fecha(pd.fechaInicio).getFecha().getTime() )
			return true;
		return false;
	}
}
