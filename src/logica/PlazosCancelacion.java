package logica;

import java.util.ArrayList;
import java.util.List;

import util.Fecha;

public class PlazosCancelacion {

	private List<PlazoCancelacion> plazos;
	
	public PlazosCancelacion() {
		plazos = new ArrayList<>();
	}
	
	public void addToPlazos(PlazoCancelacion p)
	{
		if(sePuedeAdd(p))
			plazos.add(p);
	}
	
	private boolean sePuedeAdd(PlazoCancelacion p) {
		return ManejadorPlazoCancelacion.sinProblemas(p);
	}
	public boolean noSePisan()
	{
		for(int i = 0; i< plazos.size()-1; i++)
		{	
			Fecha a = new Fecha(1, 1, 2018);
			Fecha b = new Fecha(2, 1, 2018);	
			Long unDia = b.getFecha().getTime() - a.getFecha().getTime();
			if(plazos.get(i).dameElDto().fecha_fin_plazo.getTime() >= plazos.get(i+1).dameElDto().fecha_inicio_plazo.getTime())
				return false;
			else if(plazos.get(i+1).dameElDto().fecha_inicio_plazo.getTime()- plazos.get(i).dameElDto().fecha_fin_plazo.getTime() > unDia)
				return false;
		}
		return true;
	}
	public List<PlazoCancelacion> getPlazos()
	{
		return plazos;
	}
	
	public void reinciaPlazos()
	{
		plazos = new ArrayList<>();
	}
}
