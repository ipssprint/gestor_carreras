package logica;

import DTO.PlazoCancelacionDTO;
import util.Fecha;

public class PlazoCancelacion {

	String idCarrera;
	String fechaInicio;
	String fechaFin;
	double porcentajeCanc;
	public PlazoCancelacion(String idCarrera, String fechaInicio, String fechaFin, double porcentajeCanc) {
	
		this.idCarrera = idCarrera;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.porcentajeCanc = porcentajeCanc;
	}
	
	
	
	
	public PlazoCancelacionDTO dameElDto()
	{	Fecha fecha1 = new Fecha(fechaInicio);
		Fecha fecha2 = new Fecha(fechaFin);
		if(ManejadorPlazoCancelacion.sinProblemas(this))
		{
			PlazoCancelacionDTO tr = new PlazoCancelacionDTO();
			tr.idCarrera = this.idCarrera;
			tr.fecha_inicio_plazo = fecha1.getFecha();
			tr.fecha_fin_plazo = fecha2.getFecha();
			tr.porcentaje_devolucion = this.porcentajeCanc;
			return tr;
		}
		return null;
	}
}
