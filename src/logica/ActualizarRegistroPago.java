package logica;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import BBDD.ActualizarRegistros;
import BBDD.BuscaAtletaEnCarrera;
import BBDD.CambiarEstadoAtleta;
import BBDD.EstadosInscripcion;
import DTO.ExtractoDto;
import DTO.ParticipaDTO;
import DTO.PlazoDTO;

public class ActualizarRegistroPago {

	public static String execute() {
		String cadena;
		FileReader f;
		List<ExtractoDto> extractos = new ArrayList<ExtractoDto>();
		ExtractoDto e;
		String cd = "";
		try {
			f = new FileReader("extractoBancario");
			BufferedReader b = new BufferedReader(f);
			while ((cadena = b.readLine()) != null) {
				e = new ExtractoDto();
				String[] cadenas = cadena.split(";");
				e.dni = cadenas[0];
				e.idCarrera = cadenas[1];
				e.fecha = new SimpleDateFormat("dd/MM/yyyy").parse(cadenas[2]);
				e.cantidad = Double.parseDouble(cadenas[3]);
				extractos.add(e);

			}
			b.close();
			List<String> s = ActualizarRegistroPago.execute(extractos);
			cd = "";
			for (String r : s)
				cd = cd + r;

		} catch (IOException | ParseException ex) {
			System.out.println("ERROR LEYENDO EXTRACTO BANCARIO");
		}
		return cd;

	}

	public static List<String> execute(List<ExtractoDto> extractos) {
		List<ExtractoDto> extractosValidos = new ArrayList<ExtractoDto>();
		List<ExtractoDto> extractosAnulados = new ArrayList<ExtractoDto>();

		boolean cantidadB = false, fechaB = false;
		List<String> msg = new ArrayList<String>();
		for (ExtractoDto extracto : extractos) {

			ParticipaDTO p = BuscaAtletaEnCarrera.execute(extracto.idCarrera, extracto.dni);
			if (p != null) {
				if (p.tipo_inscripcion.equals(EstadosInscripcion.PENDIENTE.toString())) {
					List<PlazoDTO> plazos = ActualizarRegistros.getPlazo(extracto.idCarrera);
					for (PlazoDTO plazo : plazos) {
						if (extracto.cantidad >= plazo.cuota_inscripcion) {
							cantidadB = true;

						} else {
							cantidadB = false;
						}
						Date fin = new Date(plazo.fecha_fin_plazo.getTime());
						Date inicio = new Date(plazo.fecha_inicio_plazo.getTime());
						if (extracto.fecha.before(fin) && extracto.fecha.after(inicio)) {
							fechaB = true;
							int dias = (int) ((extracto.fecha.getTime() - p.fecha_inscripcion.getTime()) / 86400000);
							if (dias > 2)
								fechaB = false;
						}
						if (cantidadB && fechaB) {
							if (extracto.cantidad > plazo.cuota_inscripcion) {
								double diferencia = extracto.cantidad - plazo.cuota_inscripcion;
								msg.add("Hay que devolver " + diferencia + "a " + extracto.dni + "\n");
							}
							extractosValidos.add(extracto);
						} else if (!cantidadB || !fechaB) {
							msg.add("Hay que devolver " + extracto.cantidad + "a " + extracto.dni + " € \n");
							if (!extractosValidos.contains(extracto))
								extractosAnulados.add(extracto);
						}
					}
				}
			}
		}
		for (ExtractoDto ex : extractosValidos) {

			CambiarEstadoAtleta.execute(EstadosInscripcion.INSCRITO, ex.dni, ex.idCarrera);

			ActualizarRegistros.update(ex.cantidad, ex.dni, ex.idCarrera);
		}
		for (ExtractoDto ex : extractosAnulados) {
			ActualizarRegistros.update(ex.cantidad, ex.dni, ex.idCarrera);
			CambiarEstadoAtleta.execute(EstadosInscripcion.ANULADO, ex.dni, ex.idCarrera);
		}
		return msg;

	}

}
