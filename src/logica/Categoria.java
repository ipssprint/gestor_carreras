package logica;

public class Categoria {
	
	private String Nombre;
	private int edadInicio;
	private int edadFin;
	private String sexo;
	
	public Categoria(String nombre, int edadInicio, int edadFin, String sexo) {
		super();
		Nombre = nombre;
		this.edadInicio = edadInicio;
		this.edadFin = edadFin;
		this.sexo = sexo;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public int getEdadInicio() {
		return edadInicio;
	}
	public void setEdadInicio(int edadInicio) {
		this.edadInicio = edadInicio;
	}
	public int getEdadFin() {
		return edadFin;
	}
	public void setEdadFin(int edadFin) {
		this.edadFin = edadFin;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	

}
