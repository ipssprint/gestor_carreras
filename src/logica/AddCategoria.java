package logica;

import DTO.CategoriaDTO;

public class AddCategoria {

	private String nombre;
	private int emin;
	private int emax;

	public AddCategoria(String nombre, int emin, int emax) {
		this.nombre = nombre;
		this.emin = emin;
		this.emax = emax;
	}
	
	public boolean sePuedeCrear()
	{
		if(nombre == null || nombre.isEmpty())
			return false;
		if(emin < 0 || emax <0)
			return false;
		if(emin > emax)
			return false;
		return true;
	}
	
	public CategoriaDTO crea()
	{
		if(sePuedeCrear())
		{
			CategoriaDTO c = new CategoriaDTO();
			c.nombre_categoria = nombre;
			c.edad_maxima = emax;
			c.edad_minima = emin;
			return c;
		}
		return null;
	}
}
