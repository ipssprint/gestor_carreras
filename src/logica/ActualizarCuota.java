package logica;

import java.util.Date;
import java.util.List;

import BBDD.ActualizarRegistros;
import DTO.CarreraDTO;
import DTO.PlazoDTO;

public class ActualizarCuota {

	public static double execute(CarreraDTO carrera) {
		List<PlazoDTO> p = ActualizarRegistros.getPlazo(carrera.idCarrera);
		Date d = new Date();

		for (PlazoDTO pla : p) {
			Date fin = new Date(pla.fecha_fin_plazo.getTime());
			Date inicio = new Date(pla.fecha_inicio_plazo.getTime());
			if (fin.after(d) && inicio.before(d))
				return pla.cuota_inscripcion;
		}
		return 0;
	}

}
