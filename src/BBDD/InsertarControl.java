package BBDD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import DTO.ControlDTO;
import JDBC.Jdbc;

public class InsertarControl {

    private static final String SQL_INSERTAR_CONTROL = "Insert into CONTROL (DNI,IDCARRERA,IDPUNTOCONTROL,TIEMPO) values (?, ?, ?, ?);";
    
    public static boolean execute(ControlDTO control) {
        boolean out = false;
        Connection c = null;
        PreparedStatement pst = null;
        ResultSet rs = null;

        try {
            c = Jdbc.getConnection();
            pst = c.prepareStatement(SQL_INSERTAR_CONTROL);
            pst.setString(1, control.DNI);
            pst.setString(2, control.idCarrera);
            pst.setLong(3, control.idPuntoControl);
            pst.setTime(4, control.tiempo);
            
            pst.executeUpdate();
            out =  true;
            c.commit();
            // hacer aserti de si esta en la tabla inscripcion y demás
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            Jdbc.close(rs, pst, c);
        }
        return out;
    }

}
