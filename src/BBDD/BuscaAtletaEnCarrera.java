package BBDD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import DTO.ParticipaDTO;
import JDBC.Jdbc;

public class BuscaAtletaEnCarrera {

	private static String SQL = "select * from Participa where idcarrera = ? and dni = ?";
	private static String SQLclub = "select * from Participa where idcarrera = ? "
			+ "and dni = ? and club_perteneciente = ?";
	public static ParticipaDTO execute(String idCarrera, String dni) {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(SQL );
			pst.setString(1, idCarrera);
			pst.setString(2, dni);
			rs = pst.executeQuery();
			while (rs.next()) {
				ParticipaDTO m = new ParticipaDTO();
				m.DNI = rs.getString(1);
				m.idCarrera = rs.getString(2);
				m.fecha_inscripcion = rs.getDate(3);
				m.tipo_inscripcion = rs.getString(4);
				m.dorsal = rs.getInt(5);
				m.tiempo = rs.getTime(6);
				m.nombre_categoria = rs.getString(7);
				m.importe_pagado = rs.getDouble(8);
				m.club = rs.getString(9);
				return m;
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return null;
	}
	
	public static ParticipaDTO execute(String idCarrera, String dni, String club) {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(SQLclub);
			pst.setString(1, idCarrera);
			pst.setString(2, dni);
			pst.setString(3, club);
			rs = pst.executeQuery();
			while (rs.next()) {
				ParticipaDTO m = new ParticipaDTO();
				m.DNI = rs.getString(1);
				m.idCarrera = rs.getString(2);
				m.fecha_inscripcion = rs.getDate(3);
				m.tipo_inscripcion = rs.getString(4);
				m.dorsal = rs.getInt(5);
				m.tiempo = rs.getTime(6);
				m.nombre_categoria = rs.getString(7);
				m.importe_pagado = rs.getDouble(8);
				m.club = rs.getString(9);
				return m;
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return null;
	}
}
