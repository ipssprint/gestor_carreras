package BBDD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import JDBC.Jdbc;

public class CambiarEstadoAtleta {
	private static final String SQL = "update Participa set tipo_inscripcion = ? where dni = ? and idcarrera = ?";

	/**
	 * llama a la base de datos y actualiza el estado de inscripcion del atleta
	 * 
	 * @param estadoInscripcion
	 * @param dni
	 * @param idcarrera
	 * @return
	 */
	public static void execute(EstadosInscripcion estadoInscripcion, String dni, String idcarrera) {
		Connection c = null;
		PreparedStatement pst = null;

		try {
			c = Jdbc.getConnection();
			pst = c.prepareStatement(SQL);
			pst.setString(1, estadoInscripcion.toString());
			pst.setString(2, dni);
			pst.setString(3, idcarrera);
			pst.execute();
			c.commit();
			// hacer aserti de si esta en la tabla inscripcion y demás
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {

			Jdbc.close(null, pst, c);
		}

	}

}
