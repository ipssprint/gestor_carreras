package BBDD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import DTO.ParticipaDTO;
import JDBC.Jdbc;

public class InscripcionesDeUnaCarrera {
	private static final String SQL = "select * from participa where idCarrera = ? "
			+ " and  nombre_categoria=? and tipo_inscripcion='INSCRITO' order by tiempo ";

	public static List<ParticipaDTO> execute(String idcarrera, String nombre_categoria) {
		Connection c = null;
		List<ParticipaDTO> lm;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			c = Jdbc.getConnection();
			ps = c.prepareStatement(SQL);

			ps.setString(1, idcarrera);
			ps.setString(2, nombre_categoria);
			rs = ps.executeQuery();
			lm = new ArrayList<ParticipaDTO>();
			while (rs.next()) {
				ParticipaDTO m = new ParticipaDTO();
				m.DNI = rs.getString(1);
				m.idCarrera = rs.getString(2);
				m.fecha_inscripcion = rs.getDate(3);
				m.tipo_inscripcion = rs.getString(4);
				if (rs.getString(5) != null)
					m.dorsal = rs.getInt(5);
				if (rs.getString(6) != null)
					m.tiempo = rs.getTime(6);
				m.nombre_categoria = rs.getString(7);
				lm.add(m);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, ps, c);
		}
		return lm;
	}
}
