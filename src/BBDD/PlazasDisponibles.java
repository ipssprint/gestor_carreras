package BBDD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import JDBC.Jdbc;

public class PlazasDisponibles {

	private static String SQL1 = "select c.LIMITE_PLAZAS from CARRERA c where c.idCarrera = ? ";
	private static String SQL2 = "select count(p.DNI) from PARTICIPA p where p.IDCARRERA = ?";

	public static int getLimitePlazas(String idCarrera) {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		int tr = -50;
		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(SQL1);
			pst.setString(1, idCarrera);

			rs = pst.executeQuery();
			
			while(rs.next())
			{	
				tr= rs.getInt(1);
				break;
			}

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return tr;
	}

	
	public static int getNumeroIncritos(String idCarrera) {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		int tr = -50;
		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(SQL2);
			pst.setString(1, idCarrera);

			rs = pst.executeQuery();
			
			while(rs.next())
			{	
				tr= rs.getInt(1);
				break;
			}

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return tr;
	}

	public static int getNumeroDePlazasDisponibles(String idCarrera)
	{
		return getLimitePlazas(idCarrera) - getNumeroIncritos(idCarrera);
	}
}
