package BBDD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import DTO.PlazoCancelacionDTO;
import DTO.PlazoDTO;
import JDBC.Jdbc;

public class ActualizarRegistros {
	
	private static String SQL_DEVOLUCION = "select * from PlazoDevolucion where idcarrera=? ";
	private static String SQL = "select * from Plazo where idcarrera=? ";

	public static List<PlazoCancelacionDTO> getPlazoDev(String idCarrera) {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<PlazoCancelacionDTO> lm = new ArrayList<PlazoCancelacionDTO>();
		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(SQL_DEVOLUCION);

			pst.setString(1, idCarrera);

			rs = pst.executeQuery();

			while (rs.next()) {
				PlazoCancelacionDTO m = new PlazoCancelacionDTO();
				m.idCarrera = rs.getString(1);
				m.porcentaje_devolucion = rs.getDouble(2);
				m.fecha_inicio_plazo = rs.getDate(3);
				m.fecha_fin_plazo = rs.getDate(4);
				lm.add(m);

			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return lm;
	}

	public static List<PlazoDTO> getPlazo(String idCarrera) {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<PlazoDTO> lm = new ArrayList<PlazoDTO>();
		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(SQL);

			pst.setString(1, idCarrera);

			rs = pst.executeQuery();

			while (rs.next()) {
				PlazoDTO m = new PlazoDTO();
				m.idCarrera = rs.getString(1);
				m.cuota_inscripcion = rs.getDouble(2);
				m.fecha_inicio_plazo = rs.getDate(3);
				m.fecha_fin_plazo = rs.getDate(4);

				lm.add(m);

			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return lm;
	}

	private static final String SQL_UPDATE = "update Participa set importe_pagado = ? where dni = ? and idcarrera = ?";

	public static void update(Double importePagado, String dni, String idcarrera) {
		Connection c = null;
		PreparedStatement pst = null;

		try {
			c = Jdbc.getConnection();
			pst = c.prepareStatement(SQL_UPDATE);
			pst.setDouble(1, importePagado);
			pst.setString(2, dni);
			pst.setString(3, idcarrera);
			pst.execute();
			c.commit();
			// hacer aserti de si esta en la tabla inscripcion y demás
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {

			Jdbc.close(null, pst, c);
		}

	}

}
