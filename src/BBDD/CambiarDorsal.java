package BBDD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import JDBC.Jdbc;

public class CambiarDorsal {

	private static final String SQL = "update Participa set dorsal = ? where dni = ? and idcarrera = ? and tipo_inscripcion = 'INSCRITO'";

	/**
	 * Crea el dorsal de cada atleta
	 * 
	 * @param dorsal
	 * @param dni
	 * @param idcarrera
	 * @return
	 */
	public static void execute(int dorsal, String dni, String idcarrera) {
		Connection c = null;;
		PreparedStatement ps = null;
		try {
			c = Jdbc.getConnection();
			ps = c.prepareStatement(SQL);
			
			ps.setInt(1, dorsal);
			ps.setString(2, dni);
			ps.setString(3, idcarrera);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(c);;
		}
	}
}
