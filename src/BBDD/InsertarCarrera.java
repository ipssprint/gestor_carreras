
package BBDD;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import DTO.CarreraDTO;
import DTO.CategoriaDTO;
import JDBC.Jdbc;

public class InsertarCarrera {
	/**
	 * @author Yago Garc?a
	 *
	 */
	private static final String SQL_INSERTAR_CARRERA = "Insert into CARRERA (NOMBRE,TIPO,DISTANCIA,FECHA_COMPETICION,IDCARRERA,IBAN, LIMITE_PLAZAS,DESNIVEL) values (?, ?, ?, ?, ?, ?, ? ,?);";
	private static final String SQL_PREINSCRIPCION = "Insert into Preinscripcion (IDCARRERA, FECHA_INICIO_PLAZO, FECHA_FIN_PLAZO) values (?,?,?)";
	private static final String SQL_CATEGORIAS = "Select * from categoria";
	private static final String acepta = "insert into aceptadas values (?,?)";
	public boolean execute(CarreraDTO carrera) {
		boolean out = false;
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			c = Jdbc.getConnection();
			pst = c.prepareStatement(SQL_INSERTAR_CARRERA);
			pst.setString(1, carrera.nombre);
			pst.setString(2, carrera.tipo);
			pst.setDouble(3, carrera.distancia);
			pst.setDate(4, new java.sql.Date(carrera.Fecha_competicion.getTime()));
			pst.setString(5, String.valueOf(getID()));
			pst.setString(6, carrera.iban);
			if(carrera.limite_plazas > 0)
				pst.setInt(7, carrera.limite_plazas);
			else
				pst.setInt(7, 500);
			pst.setInt(8, carrera.desnivel);
			carrera.idCarrera = String.valueOf(getID());
			
			pst.executeUpdate();
			out =  true;
			c.commit();
			acepta(cats(), carrera.idCarrera);
			// hacer aserti de si esta en la tabla inscripcion y demás
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return out;
	}
	private int getID() {
		boolean out = false;
		Connection c = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			c = Jdbc.getConnection();
			st = c.prepareStatement("select count (IDCARRERA) from CARRERA");
			rs = st.executeQuery();
			
			if(!rs.next()) {
				return 0;
			}
			return rs.getInt(1) +1;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, st, c);
		}
	}
	public boolean anadirPreinscripcion(Date dateInicio, Date dateFin, CarreraDTO carrera) {
		boolean out = false;
		Connection c = null;
		PreparedStatement st = null;
		try {
			c = Jdbc.getConnection();
			st = c.prepareStatement(SQL_PREINSCRIPCION);
			st.setString(1, carrera.idCarrera);
			st.setDate(2, dateInicio);
			st.setDate(3, dateFin);
			st.executeUpdate();
			out = true;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(st);
			Jdbc.close(c);
		}
		return out;
	}
	
	private void acepta(ArrayList<CategoriaDTO> cat, String id) {
		for(CategoriaDTO k : cat)
		{Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
			try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(acepta);
			pst.setString(1, id);
			pst.setString(2, k.nombre_categoria);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		}
	}
		
		private ArrayList<CategoriaDTO> cats()
		{
			ArrayList<CategoriaDTO> ad = new ArrayList<>();
			Connection c = null;
			PreparedStatement pst = null;
			ResultSet rs = null;
			
				try {
				c = Jdbc.getConnection();
				pst = c.prepareStatement(SQL_CATEGORIAS);
				rs =  pst.executeQuery();
				while(rs.next())
				{
					CategoriaDTO vv = new CategoriaDTO();
					vv.nombre_categoria = rs.getString(1);
					vv.edad_minima = rs.getInt(2);
					vv.edad_maxima = rs.getInt(3);
					ad.add(vv);
				}
			} catch (SQLException e) {
				throw new RuntimeException(e);
			} finally {
				Jdbc.close(rs, pst, c);
			}
				return ad;
		}
		
	
	
}