package BBDD.Listados;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import DTO.AtletaDTO;
import JDBC.Jdbc;

/**
 * @author Javier Ardura Hace la consulta con la cual se listan los corredores
 *         de la bdd
 *
 */
public class ListaAtletas {

	private static String SQL = "select * from Atleta";
	private static String SQL_ID = "select * from Atleta where dni=?";

	public static List<AtletaDTO> execute() {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<AtletaDTO> lm = null;

		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(SQL);

			rs = pst.executeQuery();
			lm = new ArrayList<AtletaDTO>();
			while (rs.next()) {
				AtletaDTO m = new AtletaDTO();
				m.DNI = rs.getString(1);
				m.nombre = rs.getString(2);
				m.apellidos = rs.getString(3);
				m.sexo = rs.getString(4);
				m.fecha_nacimiento = rs.getDate(5);
				lm.add(m);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return lm;
	}

	public static AtletaDTO getAtleta(String dni) {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		AtletaDTO m = null;
		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(SQL_ID);
			pst.setString(1, dni);
			rs = pst.executeQuery();

			while (rs.next()) {
				m = new AtletaDTO();
				m.DNI = rs.getString(1);
				m.nombre = rs.getString(2);
				m.apellidos = rs.getString(3);
				m.sexo = rs.getString(4);
				m.fecha_nacimiento = rs.getDate(5);

			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return m;

	}
}
