package BBDD.Listados;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import DTO.CategoriaDTO;
import JDBC.Jdbc;

public class ListaCategorias {
	private static String SQL = "select * from Categoria";
	private static String SQL_PREDETERMINADAS = "select * from Categoria where idCarrera = 0;";

	private static String ListaEdades = "select * from Carrera where edad_minima >= ? and edad_maxima <= ?";
	
	private static String listaNombres = "select Categoria.nombre_categoria from Categoria";

	public static List<CategoriaDTO> execute() {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<CategoriaDTO> lm = null;

		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(SQL);

			rs = pst.executeQuery();
			lm = new ArrayList<CategoriaDTO>();
			while (rs.next()) {
				CategoriaDTO m = new CategoriaDTO();
				m.nombre_categoria = rs.getString(1);
				m.edad_minima = rs.getInt(2);
				m.edad_maxima = rs.getInt(3);
				lm.add(m);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return lm;
	}
	
	public static List<CategoriaDTO> executePredeterminada() {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<CategoriaDTO> lm = null;

		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(SQL_PREDETERMINADAS);

			rs = pst.executeQuery();
			lm = new ArrayList<CategoriaDTO>();
			while (rs.next()) {
				CategoriaDTO m = new CategoriaDTO();
				m.nombre_categoria = rs.getString(1);
				m.edad_minima = rs.getInt(2);
				m.edad_maxima = rs.getInt(3);
				lm.add(m);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return lm;
	}

	public List<CategoriaDTO> executeEdades(int edad) {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<CategoriaDTO> lm = null;

		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(ListaEdades);
			pst.setInt(1, edad);
			pst.setInt(2, edad);
			rs = pst.executeQuery();
			lm = new ArrayList<CategoriaDTO>();
			while (rs.next()) {
				CategoriaDTO m = new CategoriaDTO();
				m.nombre_categoria = rs.getString(1);
				m.edad_minima = rs.getInt(2);
				m.edad_maxima = rs.getInt(3);
				lm.add(m);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return lm;
	}
	
	public static List<String> executeNombres() {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<String> res = null;

		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(listaNombres);

			rs = pst.executeQuery();
			res = new ArrayList<String>();
			while (rs.next()) {
				String s = rs.getString(1);
				res.add(s);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return res;
	}
}
