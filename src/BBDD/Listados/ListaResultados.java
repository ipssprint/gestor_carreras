package BBDD.Listados;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import JDBC.Jdbc;
import logica.ResultadoIndividual;

public class ListaResultados {

	private static final String SQL = "select Atleta.dni, Participa.nombre_categoria, Atleta.sexo, Participa.dorsal,"
			+ " Atleta.nombre, Atleta.apellido, Participa.tiempo "
			+ "from Participa, Atleta, Carrera "
			+ "where Carrera.nombre = ? "
			+ "and Atleta.sexo = ? "
			+ "and Participa.nombre_categoria = ? "
			+ "and Participa.idcarrera = Carrera.idCarrera "
			+ "and Participa.dni = Atleta.dni ";
	
	private static final String SQLSinCategoria = "select Atleta.dni, Participa.nombre_categoria, Atleta.sexo, Participa.dorsal,"
			+ " Atleta.nombre, Atleta.apellido, Participa.tiempo "
			+ "from Participa, Atleta, Carrera "
			+ "where Carrera.nombre = ? "
			+ "and Atleta.sexo = ? "
			+ "and Participa.idcarrera = Carrera.idCarrera "
			+ "and Participa.dni = Atleta.dni ";;

	/**
	 * Devuelve los resultados seg�n el sexo que indiques
	 * 
	 * @param dorsal
	 * @param dni
	 * @param idcarrera
	 * @return
	 */
	public static List<ResultadoIndividual> execute(String nombreCarrera, String sexo, String categoria)
	{
		Connection c = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ResultadoIndividual> sol = null;

		try {
			c = Jdbc.getConnection();
			ps = c.prepareStatement(SQL);
			ps.setString(1, nombreCarrera);
			ps.setString(2, sexo);
			ps.setString(3, categoria);
			rs = ps.executeQuery();
			sol = new ArrayList<ResultadoIndividual>();
			
			while(rs.next()) {
				ResultadoIndividual r = new ResultadoIndividual(
						rs.getString(1),	//Dni
						rs.getString(2), 	//Categoria
						rs.getString(3), 	//Sexo
						rs.getInt(4),		//Dorsal
						rs.getString(5),	//Nombre
						rs.getString(6),	//Apellidos
						rs.getTime(7));		//TiempoFinal
				sol.add(r);
			}
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		finally {
			Jdbc.close(rs, ps, c);
		}
		return sol;
	}

	public static List<ResultadoIndividual> execute(String nombreCarrera, String sexo) {
		Connection c = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ResultadoIndividual> sol = null;

		try {
			c = Jdbc.getConnection();
			ps = c.prepareStatement(SQLSinCategoria);
			ps.setString(1, nombreCarrera);
			ps.setString(2, sexo);
			rs = ps.executeQuery();
			sol = new ArrayList<ResultadoIndividual>();
			
			while(rs.next()) {
				ResultadoIndividual r = new ResultadoIndividual(
						rs.getString(1),	//Dni
						rs.getString(2), 	//Categoria
						rs.getString(3), 	//Sexo
						rs.getInt(4),		//Dorsal
						rs.getString(5),	//Nombre
						rs.getString(6),	//Apellidos
						rs.getTime(7));		//TiempoFinal
				sol.add(r);
			}
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		finally {
			Jdbc.close(rs, ps, c);
		}
		return sol;
	}
}


