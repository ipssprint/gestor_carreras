package BBDD.Listados;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import DTO.AtletaDTO;
import DTO.ParticipaDTO;
import JDBC.Jdbc;

/**
 * @author Javier Ardura Hace la consulta con la cual se listan los
 *         participantes de la bdd
 *
 */
public class ListaParticipa {

	private static String SQL = "select * from atleta where dni in (select dni from participa where idCarrera = ?)";
	private static String SQL2 = "select * from Participa";
	private static String SQL3 = "select fecha_inscripcion from participa where dni = ? and idCarrera = ?";
	private static String SQL_FIND_BY_DORSAL_CARRERA = "select * from participa where dorsal = ? and idCarrera = ?";
	
	public static List<ParticipaDTO> execute() {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<ParticipaDTO> lm = null;

		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(SQL2);
			rs = pst.executeQuery();
			lm = new ArrayList<ParticipaDTO>();
			while (rs.next()) {
				ParticipaDTO m = new ParticipaDTO();
				m.DNI = rs.getString(1);
				m.idCarrera = rs.getString(2);
				m.fecha_inscripcion = rs.getDate(3);
				m.tipo_inscripcion = rs.getString(4);
				if (rs.getString(5) != null)
					m.dorsal = rs.getInt(5);
				if (rs.getString(6) != null)
					m.tiempo = rs.getTime(6);
				m.nombre_categoria = rs.getString(7);
				lm.add(m);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return lm;
	}

	public List<AtletaDTO> execute(String idCarrera) {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<AtletaDTO> lm = null;

		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(SQL);
			pst.setString(1, idCarrera);
			rs = pst.executeQuery();
			lm = new ArrayList<AtletaDTO>();
			while (rs.next()) {
				AtletaDTO m = new AtletaDTO();
				m.DNI = rs.getString(1);
				m.nombre = rs.getString(2);
				m.apellidos = rs.getString(3);
				m.sexo = rs.getString(4);
				m.fecha_nacimiento = rs.getDate(5);
				
				lm.add(m);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return lm;
	}

	public Date getFechaInscripcionbyparticipant(String dni, String idCarrera) {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Date tr = null;

		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(SQL3);
			pst.setString(1, dni);
			pst.setString(2, "" + idCarrera);
			rs = pst.executeQuery();
			while (rs.next()) {
				tr = rs.getDate(1);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return tr;
	}

	
	public static ParticipaDTO findAtletaByDorsalAndCarrera(int dorsal, String idCarrera) {
        Connection c = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        ParticipaDTO m = null;
        try {
            c = Jdbc.getConnection();

            pst = c.prepareStatement(SQL_FIND_BY_DORSAL_CARRERA);
            pst.setInt(1, dorsal);
            pst.setString(2, idCarrera);
            rs = pst.executeQuery();

            while (rs.next()) {
                m = new ParticipaDTO();
                m.DNI = rs.getString(1);
                m.idCarrera = rs.getString(2);
                m.fecha_inscripcion = rs.getDate(3);
                m.tipo_inscripcion = rs.getString(4);
                m.dorsal = rs.getInt(5);
                m.tiempo = rs.getTime(6);
                m.nombre_categoria = rs.getString(7);
                m.importe_pagado = rs.getDouble(8);
                m.club = rs.getString(9);

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            Jdbc.close(rs, pst, c);
        }
        return m;
    }
}
