package BBDD.Listados;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import DTO.ControlDTO;
import JDBC.Jdbc;

public class ListaControles {

	private static final String SQL = "select Control.DNI, Control.idCarrera, Control.idPuntoControl, Control.tiempo "
			+ "from Control, Carrera, Participa "
			+ "where Carrera.nombre = ? "
			+ "and Control.DNI = ? "
			+ "and Control.Dni = Participa.Dni "
			+ "and Control.idcarrera = Participa.idCarrera "
			+ "and Participa.idCarrera = Control.idcarrera";
	
	private static final String SQL_EXISTS_DNI_CARRERA = "select * from control where dni = ? and idCarrera = ?";
	private static final String SQL_EXISTS_CONTROL = "select * from control where dni = ? and idCarrera = ? and idPuntoControl = ?";

	
	/**
	 * Devuelve los tiempos segun el dni y carrera que le pases
	 * 
	 * @param dorsal
	 * @param dni
	 * @param idcarrera
	 * @return
	 */
	public static List<ControlDTO> execute(String nombreCarrera, String dni)
	{
		Connection c = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ControlDTO> sol = null;

		try {
			c = Jdbc.getConnection();
			ps = c.prepareStatement(SQL);
			ps.setString(1, nombreCarrera);
			ps.setString(2, dni);
			rs = ps.executeQuery();
			sol = new ArrayList<ControlDTO>();
			
			while(rs.next()) {
				ControlDTO con = new ControlDTO();
					con.DNI = rs.getString(1);
					con.idCarrera = rs.getString(2);
					con.idPuntoControl = rs.getLong(3);
					con.tiempo = rs.getTime(4);
				sol.add(con);
			}
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		finally {
			Jdbc.close(rs, ps, c);
		}
		return sol;
	}
    
    public static boolean existeDniEnCarrera(String dni, String idCarrera) {
        Connection c = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        boolean res = false;
        
        try {
            c = Jdbc.getConnection();

            pst = c.prepareStatement(SQL_EXISTS_DNI_CARRERA);
            pst.setString(1, dni);
            pst.setString(2, idCarrera);
            rs = pst.executeQuery();

            if (rs.next()) {
                res = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            Jdbc.close(rs, pst, c);
        }
        return res;
    }

    public static boolean existePuntoControl(String dni, String idCarrera, long idPuntoControl) {
        Connection c = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        boolean res = false;
        
        try {
            c = Jdbc.getConnection();

            pst = c.prepareStatement(SQL_EXISTS_CONTROL);
            pst.setString(1, dni);
            pst.setString(2, idCarrera);
            pst.setLong(3, idPuntoControl);
            rs = pst.executeQuery();

            if (rs.next()) {
                res = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            Jdbc.close(rs, pst, c);
        }
        return res;
    }
}
