package BBDD.Listados;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import DTO.CarreraDTO;
import JDBC.Jdbc;
import util.Fecha;

/**
 * @author Javier Ardura Hace la consulta con la cual se listan las carreras de
 *         la bdd
 *
 */
public class ListaCarrera {

	private static String SQL = "select * from Carrera";

	private static String ListaOrdenadoNOPasadas = "select * from Carrera where fecha_fin_inscripcion >= TO_DATE(?, 'DD/MM/YYYY') order by fecha_fin_inscripcion";

	private static String listaNombres = "select Carrera.nombre from Carrera where fecha_fin_inscripcion <= TO_DATE(?, 'DD/MM/YYYY')";
	private static String SQL_carreraPorId = "select * from Carrera where idcarrera = ?";
	
	private static String SQL_carreraPorNombre = "select * from Carrera where nombre = ?";
	
	public static List<CarreraDTO> execute() {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<CarreraDTO> lm = null;

		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(SQL);

			rs = pst.executeQuery();
			lm = new ArrayList<CarreraDTO>();
			while (rs.next()) {
				CarreraDTO m = new CarreraDTO();
				m.idCarrera = rs.getString(1);
				m.nombre = rs.getString(2);
				m.tipo = rs.getString(3);
				m.distancia = rs.getInt(4);
				m.Fecha_fin_inscripcion = rs.getDate(5);
				m.Fecha_competicion = rs.getDate(6);
				m.iban=rs.getString(7);
				m.limite_plazas = rs.getInt(8);
				m.desnivel = rs.getInt(9);
				lm.add(m);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return lm;
	}

	public List<CarreraDTO> executeActuales() {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<CarreraDTO> lm = null;

		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(ListaOrdenadoNOPasadas);
			Fecha f = new Fecha();
			pst.setString(1, f.printFechaSQL());

			rs = pst.executeQuery();
			lm = new ArrayList<CarreraDTO>();
			while (rs.next()) {
				CarreraDTO m = new CarreraDTO();
				m.idCarrera = rs.getString(1);
				m.nombre = rs.getString(2);
				m.tipo = rs.getString(3);
				m.distancia = rs.getInt(4);
				m.Fecha_fin_inscripcion = rs.getDate(5);
				m.Fecha_competicion = rs.getDate(6);
				m.iban=rs.getString(7);
				m.limite_plazas = rs.getInt(8);
				m.desnivel = rs.getInt(9);
				lm.add(m);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return lm;
	}
	
	public static List<String> executeNombres() {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<String> res = null;

		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(listaNombres);
			Fecha f = new Fecha();
			pst.setString(1, f.printFechaSQL());
			
			rs = pst.executeQuery();
			res = new ArrayList<String>();
			
			while (rs.next()) {
				String s = rs.getString(1);
				res.add(s);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return res;
	}
	public static CarreraDTO findCarreraById(String id) {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(SQL_carreraPorId);
			pst.setString(1,id);

			rs = pst.executeQuery();
			
			while (rs.next()) {
				CarreraDTO m = new CarreraDTO();
				m.idCarrera = rs.getString(1);
				m.nombre = rs.getString(2);
				m.tipo = rs.getString(3);
				m.distancia = rs.getInt(4);
				m.Fecha_fin_inscripcion = rs.getDate(5);
				m.Fecha_competicion = rs.getDate(6);
				m.iban=rs.getString(7);
				m.limite_plazas = rs.getInt(8);
				m.desnivel = rs.getInt(9);
				return m;
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return null;
	}
	public static CarreraDTO findCarreraByName(String name) {
        Connection c = null;
        PreparedStatement pst = null;
        ResultSet rs = null;

        try {
            c = Jdbc.getConnection();

            pst = c.prepareStatement(SQL_carreraPorNombre);
            pst.setString(1,name);

            rs = pst.executeQuery();
            
            while (rs.next()) {
                CarreraDTO m = new CarreraDTO();
                m.idCarrera = rs.getString(1);
                m.nombre = rs.getString(2);
                m.tipo = rs.getString(3);
                m.distancia = rs.getInt(4);
                m.Fecha_fin_inscripcion = rs.getDate(5);
                m.Fecha_competicion = rs.getDate(6);
                m.iban=rs.getString(7);
				m.limite_plazas = rs.getInt(8);
				m.desnivel = rs.getInt(9);
                return m;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            Jdbc.close(rs, pst, c);
        }
        return null;
    }
}
