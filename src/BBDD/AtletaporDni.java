package BBDD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import DTO.AtletaDTO;
import JDBC.Jdbc;

public class AtletaporDni {
	private static String SQL = "select * from Atleta where dni = ?";

	public static AtletaDTO execute(String dni) {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(SQL );
			pst.setString(1, dni);
			rs = pst.executeQuery();
			if(rs.next()) {
				AtletaDTO m = new AtletaDTO();
				m.DNI = rs.getString(1);
				m.nombre = rs.getString(2);
				m.apellidos = rs.getString(3);
				m.sexo = rs.getString(4);
				m.fecha_nacimiento = rs.getDate(5);
				return m;
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return null;
	}
}
