package BBDD;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import DTO.AtletaDTO;
import DTO.CarreraDTO;
import DTO.ParticipaDTO;
import logica.NumericHelper;
import util.Fecha;

public class InsertarClub {


	/**
	 * El formato del archivo de texto a leer será el siguiente:
	 * DNI-NOMBRE-APELLIDOS-SEXO-FECHANAC-CLUB
	 * La fecha de nacimiento tendrá el siguiente formato:
	 * 10/10/2018
	 */
	
	private List<AtletaDTO> atletas = new ArrayList<AtletaDTO>();
	public List<AtletaDTO> atletasConProblemas = new ArrayList<AtletaDTO>();
	public List<ParticipaDTO> participaciones = new ArrayList<ParticipaDTO>();
	private boolean out = true;
	
	/**
	 * Lee el fichero y lo pasa a formato Atleta DTO
	 * @param lugar del archivo a leer (.txt)
	 */
	@SuppressWarnings("deprecation")
	private void leerFichero(String ruta) {
		 String cadena;
		 try { 
	        FileReader f = new FileReader(ruta);
	        BufferedReader b = new BufferedReader(f);
	        while((cadena = b.readLine())!=null) {
	           AtletaDTO at = new AtletaDTO();
	           ParticipaDTO par = new ParticipaDTO();
	           try {
	           String[] cad = cadena.split("-");
	           if(cad.length == 6) {
	           at.DNI = cad[0];
	           at.nombre = cad[1];
	           at.apellidos = cad[2];
	           at.sexo = cad[3];

	           if(validoLaFecha(new String(cad[4]))) {
		       String[] fec = cad[4].split("/");
		       Calendar c = Calendar.getInstance();
		       c.set(Calendar.YEAR, Integer.parseInt(fec[0]));
		       c.set(Calendar.MONTH,Integer.parseInt(fec[1])-1);
		       c.set(Calendar.DATE, Integer.parseInt(fec[2]));
	           at.fecha_nacimiento = c.getTime();
	           par.club = cad[5];
	           par.DNI= at.DNI;
	           participaciones.add(par);
	           atletas.add(at);
	           }else {
	        	   at.DNI = cad[0];
	        	   atletasConProblemas.add(at);
	        	   out = false;
	           }
	           }
	           else {
	        	   at.DNI = cad[0];
	        	   atletasConProblemas.add(at);
	        	   out = false;
	           }
	           }catch(NullPointerException e) {
	        	   System.out.print("El formato es incorrecto");
	           }
	        }
	        b.close();
		 }catch( Exception e) {
			 System.out.print("El formato es incorrecto");
		 }
	}
	public static boolean validoLaFecha(String fecha) {
		String aux[] = fecha.split("/");
		if(aux.length !=3)
			return false;
		else{
			if(!NumericHelper.isNumeric(aux[0]) || !NumericHelper.isNumeric(aux[1]) || !NumericHelper.isNumeric(aux[2]))
				return false;
			else
			{
				if(Integer.parseInt(aux[2])<0 || Integer.parseInt(aux[2])>31)
					return false;
				if(Integer.parseInt(aux[1])<1 ||Integer.parseInt(aux[1])>12 )
					return false;
				
			}
		}
		return true;
	}
	
	public boolean anadirABase(CarreraDTO carrera) {
		InscribirAtletas i = new InscribirAtletas();
		int aux=0;
		for(AtletaDTO atleta: atletas) {
			if(!i.executeClub(atleta, carrera, participaciones.get(aux))) {
				out = false;
				atletasConProblemas.add(atleta);
			}
			aux++;
		}
		return out;
	}
	
	public boolean insertarAtletas(String ruta, CarreraDTO carrera) {
		leerFichero(ruta);
		return anadirABase(carrera);
	}
}
