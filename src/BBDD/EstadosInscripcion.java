package BBDD;

public enum EstadosInscripcion {
	/**
	 * DIFERENTES ESTADOS QUE PUEDE TENER UNA INSCRIPCION
	 */
	PENDIENTE {
		public String toString() {
			return "PENDIENTE_DE_PAGO";
		}
	},

	INSCRITO {
		public String toString() {
			return "INSCRITO";
		}
	},
	ANULADO {
		public String toString() {
			return "ANULADA";
		}
	}
}
