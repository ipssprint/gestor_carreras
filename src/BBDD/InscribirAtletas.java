package BBDD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.time.LocalDate;
import java.util.Date;

import DTO.AtletaDTO;
import DTO.CarreraDTO;
import DTO.CategoriaDTO;
import DTO.ParticipaDTO;
import JDBC.Jdbc;

public class InscribirAtletas {
	/**
	 * @author Yago Garc?a
	 *
	 */
	private static final String SQL_COMPROBAR_PARTICIPA = "select * from participa where dni = ? and idcarrera = ?";
	private static final String SQL_COMPROBAR_PLAZA = "select * from participa where idcarrera = ?";
	private static final String SQL_COMPROBAR_ATLETA = "select * from atleta where dni = ?";
	private static final String SQL_INSERTAR_PARTICIPA = "Insert into participa (dni,idCarrera,fecha_inscripcion,nombre_categoria) values(?, ?, ?, ?);";
	private static final String SQL_INSERTAR_ATLETA = "Insert into Atleta (dni,nombre,apellido,fecha_nacimiento,sexo) values (?, ?, ?, ?, ?);";
	private static final String SQL_MODIFICAR_ATLETA = "update Participa set tiempo = ? where dni = ? and idcarrera = ?";
	private static final String SQL_INSERTAR_PARTICIPA_CLUB = "Insert into participa (dni,idCarrera,fecha_inscripcion,nombre_categoria, club_perteneciente, tipo_inscripcion) values(?, ?, ?, ?, ?, ?);";

 
	public boolean execute(AtletaDTO atleta, CarreraDTO carrera) {
		boolean out = false;
		Connection c = null;
		PreparedStatement pst = null;
		PreparedStatement pst2 = null;
		PreparedStatement pst3 = null;
		PreparedStatement pst4 = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		if(!check(carrera)) {
			return false;
		}

		try {
			c = Jdbc.getConnection();

			pst3 = c.prepareStatement(SQL_COMPROBAR_ATLETA);
			pst3.setString(1, atleta.DNI);
			rs2 = pst3.executeQuery();
			if (!rs2.next()) {
				pst4 = c.prepareStatement(SQL_INSERTAR_ATLETA);
				pst4.setString(1, atleta.DNI);
				pst4.setString(2, atleta.nombre);
				pst4.setString(3, atleta.apellidos);
				pst4.setDate(4, new java.sql.Date(atleta.fecha_nacimiento.getTime()));
				pst4.setString(5, atleta.sexo);
				pst4.executeUpdate();
				System.out.println("ATLETA INSERTADO");

			}
				pst = c.prepareStatement(SQL_COMPROBAR_PARTICIPA);
				pst.setString(1, atleta.DNI);
				pst.setString(2,carrera.idCarrera);
				rs = pst.executeQuery();
			
				if (!rs.next()) {
					System.out.println("Buscando categoria");
					Categoria cat = new Categoria();
					CategoriaDTO categoria = cat.getCategoriaEdad(getEdad(atleta));
					if(categoria != null) {
						System.out.println("categoria: " + categoria.nombre_categoria);
						System.out.print("Inscribiendo");
						pst2 = c.prepareStatement(SQL_INSERTAR_PARTICIPA);
						pst2.setString(1, atleta.DNI);
						pst2.setString(2, carrera.idCarrera);
						pst2.setDate(3, new java.sql.Date(new Date().getTime()));
						pst2.setString(4, categoria.nombre_categoria);
						pst2.executeUpdate();
						System.out.println("ATLETA INSCRITO");
						out = true;
					}
				}

				

			
			c.commit();
			// hacer aserti de si esta en la tabla inscripcion y dem?s
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return out;
	}

	private boolean check(CarreraDTO carrera) {
		boolean out = true;
		PreparedStatement pst = null;
		Connection c = null;
		ResultSet rs = null;
		int count = 0;
		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(SQL_COMPROBAR_PLAZA);
			pst.setString(1, carrera.idCarrera);
			rs = pst.executeQuery();
			while(rs.next()) {
				count++;
			}
			if(count >= carrera.limite_plazas) {
				out = false;
			}
		
	}catch(Exception e) {
		
	}
		return out;
	}

	@SuppressWarnings("deprecation")
	public int getEdad(AtletaDTO atleta) {
		LocalDate fechaNacimiento = LocalDate.of(atleta.fecha_nacimiento.getYear() + 1900, atleta.fecha_nacimiento.getMonth()+1,
				atleta.fecha_nacimiento.getDay());
		LocalDate fechaHoy = LocalDate.now();
		int edad = fechaHoy.getYear() - fechaNacimiento.getYear();
		return edad;
	}

	public static void modificarTiempoFinal(Time time, String dni, String idcarrera) {
        Connection c = null;;
        PreparedStatement ps = null;
        try {
            c = Jdbc.getConnection();
            ps = c.prepareStatement(SQL_MODIFICAR_ATLETA);
            ps.setTime(1, time);
            ps.setString(2, dni);
            ps.setString(3, idcarrera);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            Jdbc.close(c);
        }
    }
	
	public boolean executeClub(AtletaDTO atleta, CarreraDTO carrera, ParticipaDTO participa) {
		boolean out = false;
		Connection c = null;
		PreparedStatement pst = null;
		PreparedStatement pst2 = null;
		PreparedStatement pst3 = null;
		PreparedStatement pst4 = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		if(!check(carrera)) {
			return false;
		}

		try {
			c = Jdbc.getConnection();

			pst3 = c.prepareStatement(SQL_COMPROBAR_ATLETA);
			pst3.setString(1, atleta.DNI);
			rs2 = pst3.executeQuery();
			if (!rs2.next()) {
				pst4 = c.prepareStatement(SQL_INSERTAR_ATLETA);
				pst4.setString(1, atleta.DNI);
				pst4.setString(2, atleta.nombre);
				pst4.setString(3, atleta.apellidos);
				pst4.setDate(4, new java.sql.Date(atleta.fecha_nacimiento.getTime()));
				pst4.setString(5, atleta.sexo);
				pst4.executeUpdate();
				System.out.println("ATLETA INSERTADO");

			}
				pst = c.prepareStatement(SQL_COMPROBAR_PARTICIPA);
				pst.setString(1, atleta.DNI);
				pst.setString(2,carrera.idCarrera);
				rs = pst.executeQuery();
				
				if (!rs.next()) {
					System.out.println("Buscando categoria");
					Categoria cat = new Categoria();
					CategoriaDTO categoria = cat.getCategoriaEdad(getEdad(atleta));
					if(categoria != null) {
						System.out.println("categoria: " + categoria.nombre_categoria);
						System.out.print("Inscribiendo");
						pst2 = c.prepareStatement(SQL_INSERTAR_PARTICIPA_CLUB);
						pst2.setString(1, atleta.DNI);
						pst2.setString(2, carrera.idCarrera);
						pst2.setDate(3, new java.sql.Date(new Date().getTime()));
						pst2.setString(4, categoria.nombre_categoria);
						pst2.setString(5,participa.club);
						pst2.setString(6, EstadosInscripcion.INSCRITO.toString());
						pst2.executeUpdate();
						System.out.println("ATLETA INSCRITO");
						out = true;
					}
				}

				

			
			c.commit();
			// hacer aserti de si esta en la tabla inscripcion y dem?s
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return out;
	}

}



