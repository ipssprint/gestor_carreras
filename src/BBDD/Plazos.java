package BBDD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import DTO.PlazoDTO;
import JDBC.Jdbc;
import util.Fecha;

public class Plazos {
	private static String insertar = "insert into Plazo values(?,?,?,?)";
	private static String dame = "select cuota_inscripcion from Plazo where idcarrera = ? and fecha_inicio_plazo <= ? and fecha_fin_plazo >= ?";
	private static String damePlazosCarrera = "select * from Plazo where idcarrera = ?";
	
	public void insertar(PlazoDTO pd)
	{
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(insertar);
			pst.setString(1, pd.idCarrera);
			pst.setDouble(2, pd.cuota_inscripcion);
			pst.setDate(3, java.sql.Date.valueOf(new Fecha(pd.fecha_inicio_plazo).printFechaSQLvalueOf()));
			pst.setDate(4, java.sql.Date.valueOf(new Fecha(pd.fecha_fin_plazo).printFechaSQLvalueOf()));
			
			
			
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		
	}
	public Double precioFecha(String printFechaSQLvalueOf, String id) {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Double precio = null;

		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(dame);
			pst.setString(1, id);
			pst.setDate(2, java.sql.Date.valueOf(printFechaSQLvalueOf));
			pst.setDate(3, java.sql.Date.valueOf(printFechaSQLvalueOf));
			
			rs = pst.executeQuery();
			while(rs.next())
			{
				precio = rs.getDouble(1);
				break;
			}
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return precio;
	}
	
	public List<PlazoDTO> plazosCarrera(String id) {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<PlazoDTO> l = new ArrayList<>();
		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(damePlazosCarrera);
			pst.setString(1, id);
			
			rs = pst.executeQuery();
			while(rs.next())
			{
				PlazoDTO p = new PlazoDTO();
				p.fecha_fin_plazo = rs.getDate(4);
				p.fecha_inicio_plazo = rs.getDate(3);
				l.add(p);
			}
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return l;
	}
}
