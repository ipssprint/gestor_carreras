package BBDD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import BBDD.Listados.ListaParticipa;
import DTO.AtletaDTO;
import DTO.PreinscripcionDTO;
import JDBC.Jdbc;
import util.Fecha;

public class InscribirAtletaSegunTipoInscripcion {

	private static final String SQL_INSERTAR_PARTICIPA = "Insert into participa values (?, ?, ?, ?, ?, ?, ? ,? ,?)";
	private static final String SQL_INSERTAR_ATLETA = "Insert into Atleta (dni,nombre,apellido,fecha_nacimiento,sexo) values (?, ?, ?, ?, ?);";
	private static final String SQL_BORRA_PARTICIPA = "DELETE FROM PARTICIPA WHERE DNI = ? AND IDCARRERA = ? AND CLUB_PERTENECIENTE = 'INDEPENDIENTE'";
	private static final String SQL_TIENE_PI = "Select * from preinscripcion where idcarrera = ?";
	private static final String SQL_PREINSCRIPCION_ACTIVA = "Select * from preinscripcion where FECHA_INICIO_PLAZO <= ? and FECHA_FIN_PLAZO >= ?";
	public static void execute(AtletaDTO atleta, String idCarrera, String club) {
		
			if(club.isEmpty() || club == null)
				club = "INDEPENDIENTE";
			if(!existeYa(atleta))
				insertaAtleta(atleta);
			if(yaEsParticipante(atleta, idCarrera))
				borraAtleta(atleta, idCarrera);
			Connection c = null;
			PreparedStatement pst = null;
			ResultSet rs = null;
			try {
				c = Jdbc.getConnection();

				pst = c.prepareStatement(SQL_INSERTAR_PARTICIPA);
				pst.setString(1, atleta.DNI);
				pst.setString(2, idCarrera);
				pst.setDate(3, new java.sql.Date(new Fecha().getFecha().getTime()));
				
				if(tienePreinscripcion(idCarrera))
					pst.setString(4, "PREINSCRITO");
				else
				{
					if(club.equals("INDEPENDIENTE"))
						pst.setString(4, EstadosInscripcion.PENDIENTE.toString());
					else
						pst.setString(4, EstadosInscripcion.INSCRITO.toString());
				}
				pst.setInt(5, -1); 
				pst.setTime(6, null);
				
				pst.setString(7, buscaSuCategoria(atleta.fecha_nacimiento, idCarrera));
				
				if(!club.equals("INDEPENDIENTE"))
					pst.setDouble(8, new Plazos().precioFecha(new Fecha().printFechaSQLvalueOf(), idCarrera));
				else
					pst.setDouble(8, 0.0);
				
				pst.setString(9, club);
				pst.executeUpdate();
				
				
			} catch (SQLException e) {
				throw new RuntimeException(e);
			} finally {
				Jdbc.close(rs, pst, c);
			}
			
				
	}
	
	private static void borraAtleta(AtletaDTO atleta, String idCa) {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(SQL_BORRA_PARTICIPA);
			pst.setString(1, atleta.DNI);
			pst.setString(2, idCa);
			pst.executeUpdate();
			
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		
	}

	private static boolean yaEsParticipante(AtletaDTO atleta, String idCarrera) {
		
		ListaParticipa l = new ListaParticipa();
		return l.getFechaInscripcionbyparticipant(atleta.DNI, idCarrera) != null;
	}

	private static String buscaSuCategoria(Date fecha_nacimiento, String idCarrera) {
		Categoria c = new Categoria();
		Fecha f = new Fecha(fecha_nacimiento);
		
		
		if(c.getCategoriaEdadAceptada(f.getEdadDeEstaFecha(), idCarrera))
			return c.getCategoriaEdad(f.getEdadDeEstaFecha()).nombre_categoria;
		else
			throw new RuntimeException("No hay categoria para este atleta");
		
	}

	private static boolean existeYa(AtletaDTO atleta)
	{
		return AtletaporDni.execute(atleta.DNI) != null;
	}
	
	public static void insertaAtleta(AtletaDTO atleta)
	{
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(SQL_INSERTAR_ATLETA);
			pst.setString(1, atleta.DNI);
			pst.setString(2, atleta.nombre);
			pst.setString(3, atleta.apellidos);
			pst.setDate(4, new java.sql.Date(atleta.fecha_nacimiento.getTime()));
			pst.setString(5, atleta.sexo);
			pst.executeUpdate();
			
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
	}
	
	public static boolean tienePreinscripcion(String id)
	{
		String tr = "";
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(SQL_TIENE_PI);
			pst.setString(1, id);
			
			rs = pst.executeQuery();
			while(rs.next())
			{
				tr = "si";
				break;
			}
			
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return !tr.isEmpty();
	}

	public static boolean tienePreinscripcionAhora(String idCarrera) {
		PreinscripcionDTO pd = damelaPreinscripcion(idCarrera);
		if(tienePreinscripcion(idCarrera) && pd != null)
		{
			
			Fecha f = new Fecha();
			return f.getFecha().getTime() >= pd.fechaInicio.getTime() 
					&& f.getFecha().getTime() <= pd.fechaFin.getTime();
		}
		return false;
	}
	
	
	public static PreinscripcionDTO damelaPreinscripcion(String idCarrera)
	{
		PreinscripcionDTO pd = null;
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(SQL_TIENE_PI);
			pst.setString(1, idCarrera);
			
			rs = pst.executeQuery();
			while(rs.next())
			{
				pd = new PreinscripcionDTO();
				pd.idCarrera = rs.getString(1);
				pd.fechaInicio = rs.getDate(2);
				pd.fechaFin = rs.getDate(3);
			}
			
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return pd;
	}
	
	public static  ArrayList<PreinscripcionDTO> listaConPreinscripcionActiva()
	{
		Fecha f = new Fecha();
		ArrayList<PreinscripcionDTO> pd = new ArrayList<>();
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(SQL_PREINSCRIPCION_ACTIVA);
			pst.setDate(1,new java.sql.Date(f.getFecha().getTime()) );

			pst.setDate(2,new java.sql.Date(f.getFecha().getTime()) );
			
			rs = pst.executeQuery();
			while(rs.next())
			{
				PreinscripcionDTO pdto = new PreinscripcionDTO();
				
				pdto.idCarrera = rs.getString(1);
				pdto.fechaInicio = rs.getDate(2);
				pdto.fechaFin = rs.getDate(3);
				pd.add(pdto);
			}
			
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return pd;
	}
}
