package BBDD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import DTO.PlazoCancelacionDTO;
import JDBC.Jdbc;
import util.Fecha;

public class PlazosCancelacion {
	private static String insertar = "insert into PlazoDevolucion values(?,?,?,?)";
	private static String dame = "select cuota_inscripcion from PlazoDevolucion where idcarrera = ? and fecha_inicio_plazo <= ? and fecha_fin_plazo >= ?";
	public void insertar(PlazoCancelacionDTO pd)
	{
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(insertar);
			pst.setString(1, pd.idCarrera);
			pst.setDouble(2, pd.porcentaje_devolucion);
			pst.setDate(3, java.sql.Date.valueOf(new Fecha(pd.fecha_inicio_plazo).printFechaSQLvalueOf()));
			pst.setDate(4, java.sql.Date.valueOf(new Fecha(pd.fecha_fin_plazo).printFechaSQLvalueOf()));
			
			
			
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		
	}
	public Double porcentajeFecha(String printFechaSQLvalueOf, String id) {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Double precio = null;

		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(dame);
			pst.setString(1, id);
			pst.setDate(2, java.sql.Date.valueOf(printFechaSQLvalueOf));
			pst.setDate(3, java.sql.Date.valueOf(printFechaSQLvalueOf));
			
			rs = pst.executeQuery();
			while(rs.next())
			{
				precio = rs.getDouble(1);
				break;
			}
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return precio;
	}
}
