package BBDD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import DTO.CategoriaDTO;
import JDBC.Jdbc;

/**
 * @author Javier Ardura
 *
 */
public class Categoria {
private static String insertar = "insert into Categoria values(?,?,?)";
private static String editar = "update categoria set edad_minima = ?, edad_maxima = ? where nombre_categoria = ?";
private static String comprueba = "select * from categoria where nombre_categoria = ?";
private static String compruebaChoque = "select * from categoria where edad_minima <= ? and edad_maxima >= ? and nombre_categoria <> ?";
private static String compruebaChoqueSinNada = "select * from categoria where edad_minima <= ? and edad_maxima >= ? ";
private static String elimina = "delete from categoria where nombre_categoria = ?";
private static String editar_participa = "update participa set nombre_categoria = ? where dni, idCarrera in (select p.dni, p.idCarrera from participa p where p.nombre_categoria = ?";
private static String listaDondeSeAcepta = "select idCarrera from aceptadas where nombre_categoria = ?";
private static String insertar_acepta = "insert into Aceptadas values(?,?)";
private static String borra_acepta = "delete from aceptadas where nombre_categoria = ?";
private static String listaSiSeAcepta = "select * from aceptadas where nombre_categoria = ? and idCarrera = ?";
private static String notiene = "Select count(*) from participa where nombre_categoria = ?";
public boolean noExiste(String nombre)
{
	Connection c = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	List<CategoriaDTO> lm = null;

	try {
		c = Jdbc.getConnection();
		pst = c.prepareStatement(comprueba);
		pst.setString(1, nombre);
		rs = pst.executeQuery();
		lm = new ArrayList<CategoriaDTO>();
		while (rs.next()) {
			CategoriaDTO m = new CategoriaDTO();
			m.nombre_categoria = rs.getString(1);
			m.edad_maxima = rs.getInt(3);
			m.edad_minima = rs.getInt(2);
			lm.add(m);
		}
	} catch (SQLException e) {
		throw new RuntimeException(e);
	} finally {
		Jdbc.close(rs, pst, c);
	}
	return lm.size()==0;
}

	public void insertar(CategoriaDTO cd)
	{
		if(noExiste(cd.nombre_categoria))
		{Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(insertar);
			pst.setString(1, cd.nombre_categoria);
			pst.setInt(2, cd.edad_minima);
			pst.setInt(3, cd.edad_maxima);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		}
	}

	public void borrar(String cd)
	{
		if(!noExiste(cd) && !tieneAsociados(cd))
		{Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(elimina);
			pst.setString(1, cd);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		}
	}
	
	public boolean tieneAsociados(String cd) {
		int r = 0;
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(notiene);
			pst.setString(1, cd);
			rs = pst.executeQuery();
			while(rs.next())
				r = rs.getInt(1);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return r > 0;
	}

	/**
	 * @param cd
	 */
	public void modificar(CategoriaDTO cd)
	{
		if(!noExiste(cd.nombre_categoria))
		{Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(editar);
			pst.setString(3, cd.nombre_categoria);
			pst.setInt(1, cd.edad_minima);
			pst.setInt(2, cd.edad_maxima);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		}
	}

	public boolean comprobarChoqueEdad(int edadCambio, String nombre) {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ArrayList<String> r = new ArrayList<>();

		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(compruebaChoque);
			pst.setString(3, nombre);
			pst.setInt(1, edadCambio);
			pst.setInt(2, edadCambio);
			rs = pst.executeQuery();
			
			while(rs.next())
			{
				r.add("");
				break;
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return r.size() == 0;
	}

	public boolean comprobarChoqueEdad(int edadCambio) {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ArrayList<String> r = new ArrayList<>();

		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(compruebaChoqueSinNada);
			pst.setInt(1, edadCambio);
			pst.setInt(2, edadCambio);
			rs = pst.executeQuery();
			
			while(rs.next())
			{
				r.add("");
				break;
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return r.size() == 0;
	}
	
	public CategoriaDTO getCategoriaEdad(int edadCambio) {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		CategoriaDTO cat = null;

		try {
			c = Jdbc.getConnection();
			cat = new CategoriaDTO();
			pst = c.prepareStatement(compruebaChoqueSinNada);
			pst.setInt(1, edadCambio);
			pst.setInt(2, edadCambio);
			rs = pst.executeQuery();
			
			while(rs.next())
			{
				cat.nombre_categoria = rs.getString(1);
				cat.edad_minima = rs.getInt(2);
				cat.edad_maxima = rs.getInt(3);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return cat;
	}
	
	public boolean getCategoriaEdadAceptada(int edadCambio, String idCarrera) {
		Connection c = null;
		String idCarreraTR = "";
		PreparedStatement pst = null;
		ResultSet rs = null;
		CategoriaDTO cat = getCategoriaEdad(edadCambio);
		if(cat != null)
		{try {
			c = Jdbc.getConnection();
			pst = c.prepareStatement(listaSiSeAcepta);
			pst.setString(1, cat.nombre_categoria);
			pst.setString(2,idCarrera);
			rs = pst.executeQuery();
			while(rs.next())
			{
				idCarreraTR = rs.getString(1);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		}
		return idCarreraTR != "";
	}
	
	
	public void editarParticipantes(String categoria_nueva, String cat_anterior)
	{
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(editar_participa);
			pst.setString(1, categoria_nueva);
			pst.setString(2, cat_anterior);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		
	}
	
	public void editarAceptadas(String categoria_nueva, String cat_anterior)
	{
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(editar_participa);
			pst.setString(1, categoria_nueva);
			pst.setString(2, cat_anterior);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		
	}

	public void acepta(String nombre_categoria, String cat_antigua) {
		List<Long> lista = listaDondeSeAcepta(cat_antigua);
		
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		for(Long l : lista)
		{
			try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(insertar_acepta);
			pst.setLong(1, l);
			pst.setString(2, nombre_categoria);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		}
		
	}

	private List<Long> listaDondeSeAcepta(String categoria)
	{
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Long> lm = null;

		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(listaDondeSeAcepta);
			pst.setString(1, categoria);
			rs = pst.executeQuery();
			lm = new ArrayList<Long>();
			while (rs.next()) {
				lm.add(rs.getLong(1));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return lm;
	}

	public void borra_acepta(String cat_antigua) {
		
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
			try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(borra_acepta);
			pst.setString(1, cat_antigua);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
	}
	
}
