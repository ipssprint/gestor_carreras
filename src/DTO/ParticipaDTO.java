package DTO;

import java.sql.Time;
import java.util.Date;

/**
 * DTO para la tabla participa
 * 
 * @author Javier Ardura
 *
 */
public class ParticipaDTO {

	public String DNI;
	public String idCarrera;
	public Date fecha_inscripcion;
	public String tipo_inscripcion;
	public Time tiempo;
	public int dorsal;
	public String club;
	public String nombre_categoria;
	public double importe_pagado;
	public int posicion;

}
