package DTO;

import java.util.Date;

public class ExtractoDto {
	public String dni;
	public String idCarrera;
	public Date fecha;
	public double cantidad;
}
