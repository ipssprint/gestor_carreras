package DTO;

import java.util.Date;

public class PlazoCancelacionDTO {

	public String idCarrera;
	public Date fecha_inicio_plazo;
	public Date fecha_fin_plazo;
	public double porcentaje_devolucion;
}
