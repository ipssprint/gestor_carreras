package DTO;

import java.util.Date;

/**
 * DTO para la tabla carrera
 * 
 * @author Javier Ardura
 *
 */
public class CarreraDTO {
	public String idCarrera;
	public String nombre;
	public String tipo;
	public double cuota_inscripcion;
	public int distancia;
	public int desnivel;
	public int limite_plazas;
	public Date Fecha_fin_inscripcion;
	public Date Fecha_competicion;
	public String iban;
	public boolean preinscripcion;
}
