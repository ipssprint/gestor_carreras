package DTO;

import java.util.Date;

public class PlazoDTO {

	public String idCarrera;
	public double cuota_inscripcion;
	public Date fecha_inicio_plazo;
	public Date fecha_fin_plazo;
}
