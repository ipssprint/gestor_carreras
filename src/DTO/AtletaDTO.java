package DTO;

import java.util.Date;


 
/**
 * DTO para la tabla atleta
 * @author Javier Ardura
 */
public class AtletaDTO {

	public String DNI;
	public String nombre;
	public String apellidos;
	public String sexo;
	public Date fecha_nacimiento;
}