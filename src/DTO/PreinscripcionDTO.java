package DTO;

import java.util.Date;

public class PreinscripcionDTO {

	public String idCarrera;
	public Date fechaInicio;
	public Date fechaFin;
}
