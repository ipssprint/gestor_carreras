package DTO;

import java.util.List;
import java.util.Map;

public class ResultadoDeAtletaEnCarrerasDTO {

	public AtletaDTO atleta;
	public Map<String, CarreraDTO> inscripciones;
	public List<ParticipaDTO> participaciones;
	public Map<String, List<ControlDTO>> controles;
	public int maximosControles;

}
