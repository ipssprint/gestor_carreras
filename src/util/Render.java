package util;

import java.awt.Component;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableCellRenderer;

public class Render extends DefaultTableCellRenderer {

	/**
	 * @author Javier Ardura
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row,
			int column) {
		if(value instanceof JButton)
		{	JButton boton = (JButton) value;
			return boton;
		}
		else if(value instanceof JTextArea)
		{
			JTextArea textArea = (JTextArea) value;
			return textArea;
		}
		else if(value instanceof JCheckBox)
		{
			JCheckBox checkBox = (JCheckBox) value;
			return checkBox;
		}
		return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
	}

}
