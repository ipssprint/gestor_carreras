package util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;

public class Fecha {

	Date fecha;
	SimpleDateFormat sdf;
	private int mes;
	private int year;
	private int dia;
	public Fecha(int day, int month, int year) {
		
		sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			fecha = sdf.parse(year+"-"+month+"-"+day);
			this.dia = day;
			this.mes = month;
			this.year = year;
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
public Fecha(String ddmmyyyy) {
		String[] aux = ddmmyyyy.split("-");
		int year = Integer.parseInt(aux[2]);
		int month = Integer.parseInt(aux[1]);
		int day = Integer.parseInt(aux[0]);
		sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			fecha = sdf.parse(year+"-"+month+"-"+day);
			this.dia = day;
			this.mes = month;
			this.year = year;
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	public Fecha()
	{
		transformaFechaHoy(new Date());
		this.fecha = new Date();
	}
	
	public Fecha(Date diad)
	{
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		String aux = df.format(diad);
		String[] aux2 = aux.split(" ");
		String[] aux3 = aux2[0].split("/");
		mes = Integer.parseInt(aux3[0]);
		dia = Integer.parseInt(aux3[1]);
		year = Integer.parseInt(aux3[2]);
		this.fecha = diad;
	}
	
	private void transformaFechaHoy(Date hoy) {
		String[] hoyTS = hoy.toString().split(" ");
		this.dia = Integer.parseInt(hoyTS[2]);
		this.year = Integer.parseInt(hoyTS[5]);
		this.mes = buscaMes(hoyTS[1]);
		
		
	}

	private int buscaMes(String string) {
		HashMap<String, Integer> dm = new HashMap<>();
		dm.put("Dec", 12);
		dm.put("Nov", 11);
		dm.put("Oct", 10);
		dm.put("Sep", 9);
		dm.put("Aug", 8);
		dm.put("Jul", 7);
		dm.put("Jun", 6);
		dm.put("May", 5);
		dm.put("Apr", 4);
		dm.put("Mar", 3);
		dm.put("Feb", 2);
		dm.put("Jan", 1);
		return dm.get(string);
	
	
	}

	public Date getFecha()
	{
		return fecha;
	}
	public String printFecha()
	{
		
		return (dia + " - "+ mes+ " - "+ year);
	}
	
	public String printFechaSQL()
	{
		String diaS = dia+"";
		String mesS = mes+"";
		if(this.dia <10)
			diaS = "0"+dia;
		if(this.mes <10)
			mesS = "0"+mes;
		return (diaS + "/"+ mesS+ "/"+ year);
	}
	public String printFechaSQLvalueOf()
	{
		
		return (year + "-"+ mes+ "-"+ dia);
	}
	
	public int ultimoDiaMes(int mes, int year)
	{
		if(mes == 1 || mes == 3 || mes == 5|| mes ==7|| mes == 8 || mes ==10|| mes ==12)
			return 31;
		else if(mes != 2)
			return 30;
		else{
			if(year %4 ==0 && ((year %100 != 0)|| year %400 ==0))
				return 29;
			else
				return 28;
		}
	}

	public int getMes() {
		return mes;
	}

	public int getYear() {
		return year;
	}

	public int getDia() {
		return dia;
	}
	
	public int getEdadDeEstaFecha()
	{
		
		DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate fechaNac = LocalDate.parse(printFechaSQL(), fmt);
		Fecha f = new Fecha();
		LocalDate ahora = LocalDate.parse(f.printFechaSQL(), fmt);
		Period p = Period.between(fechaNac, ahora);
		return p.getYears();
	}
	
}